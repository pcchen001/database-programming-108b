---
title: Cookie 瀏覽器上的暗記
date: 2020-03-19 15:59:27
tags:
- html
- cookie
categories:
- http
- Tools
---

# Cookie

WWW 上的 Cookie 是指我們在瀏覽器上的一塊可以讓伺服器存取資料的區域。

我們瀏覽的網站，每一個都會有對應的 cookie。

練習：

```
開啟瀏覽器，找出 cookie 。
```

一個網站可以在我們的瀏覽器上存放的總Cookie 數有限制：

1. 一個 Cookie 大小必須小於 4K bytes
2. 一個網站可以存放 50 個 Cookie
3. 一個瀏覽器最少要能提供 3000 個 cookie 的容量



不同瀏覽器 cookie 的設定位置

Google Chrome

1. 「自訂和控制」（瀏覽器地址欄右側的扳手圖標） > 設定 > 進階選項
2. 隱私權說明：內容設定…
3. Cookie  [所有 Cookie 和網站資料]

微軟 Internet Explorer

    1. 工具 > 網際網路選項 > 隱私權頁
       2. 調節滑塊或者點擊「進階」，進行設置。

Mozilla Firefox

	1. 工具 > 選項 > 個人隱私
 	2. 設置Cookies選項
      	1. 設定阻止／允許的各個域內Cookie
      	2. 查看Cookies管理視窗，檢查現存Cookie信息，選擇刪除或者阻止它們



Javascript 或是 php 都可以讀寫 cookie 。

php 寫入 cookie 語法：

```
setcookie(string name[, string value[, int expire[, string path[, string domain[, bool secure]]]]])

<?php
  header(“Content-type: text/html; charset=utf-8”);　//指定網頁編碼方式為UTF-8
  setcookie("UserName", "小丸子", time() + 60 * 60 * 24);
  setcookie("UserAge", 10, time() + 60 * 60 * 24);
?>

```

php 讀取 cookie 語法：

```
$_COOKIE["UserName"];

<?php
   echo $_COOKIE['UserName'];
?>
```

陣列表示的寫入讀取方式：

```
<?php
  setcookie("Words[0]", "Apple");
  setcookie("Words[1]", "Banana");
  setcookie("Words[2]", "Cherry");
?>

<?php
  if( isset($_COOKIE["Words"]){
    foreach($_COOKIE["Words"] as $key=> $value )
        echo "$key: $value <br>";
  }
?>
```

JSON 表示的寫入讀取方式：

```
<?php
  $myObj = new class{};   // anonymous class
  
  $myObj->name = "John約翰";
  $myObj->age = 30;
  $myObj->city = "New York紐約";

  $myJSON = json_encode($myObj);
  setcookie("JsonData", $myJSON, time() + 24 * 60 * 60);
?>

<?php
  if (isset($_COOKIE['JsonData']))
    $values = json_decode($_COOKIE['JsonData']);
    foreach ($values as  $key => $value)
      echo "$key : $value <br>";
?>

```

Javascript 的寫入讀取 Cookie:

```javascript
document.cookie="username=John Doe";
document.cookie="username=John Doe; expires=Thu, 18 Dec 2013 12:00:00 GMT";

var x = document.cookie; // 取得全部的 Cookie
console.log( x );         // 字串，要取各個參數，要用分號，段開，再用等號分開。
　　//例如  "username=Jonh Doe;age=12"
　　
document.cookie="username=Mary Don"; // 　更改 username 的內容
document.cookie="username=;"  // 刪除　username
```

