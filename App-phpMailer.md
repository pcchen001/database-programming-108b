---
title: '郵件寄送 SMPT - PHPMailer'
date: 2020-05-5 08:36:33
tags:
- PHPMailer
- Mail
- SMTP
categories:
- Tools
- PHP
---

# PHPMailer

https://github.com/PHPMailer/PHPMailer

PHP 寄送郵件，比較常見好用的是 PHPMailer。

目前的版本是 PHPMailer 6.0 。

使用方式：

1. 使用 composer 安裝

   1. 首先你必須已經安裝了 composer (這是PHP的套件管理軟體，你可以點擊 [Composer-Setup.exe](https://getcomposer.org/Composer-Setup.exe) 直接安裝，或是按照官網用 php 安裝。

   2.  接著在你的工作目錄下執行

      ```
      composer require phpmailer/phpmailer
      ```

      

   3. 此時，你的工作目錄下會出現 vendor 以及 composer.json, composer.lock 二個檔案。這樣就算安裝完成。

   4. 假設你的寄信 php 在工作目錄，一個範例程式如下：

      ```
      <?php
      // Import PHPMailer classes into the global namespace
      // These must be at the top of your script, not inside a function
      use PHPMailer\PHPMailer\PHPMailer;
      use PHPMailer\PHPMailer\SMTP;
      use PHPMailer\PHPMailer\Exception;
      
      // Load Composer's autoloader
      require 'vendor/autoload.php';
      
      // Instantiation and passing `true` enables exceptions
      $mail = new PHPMailer(true);
      
      try {
          //Server settings
          $mail->SMTPDebug = SMTP::DEBUG_SERVER;       // 開啟除錯模式
                                                       // 實際運作時，請關閉
          $mail->isSMTP();                                      // Send using SMTP
          $mail->Host       = 'smtp.gmail.com';                 // 設定 SMTP server to send through
          $mail->SMTPAuth   = true;                    // Enable SMTP authentication
          $mail->Username   = 'yourAccount@gmail.com';          // Gmail 帳號
          $mail->Password   = 'YourPassword';                   // Gmail 密碼
          $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;   // 開啟 TLS 加密
          $mail->Port       = 587;                              // TLS 的 port 587
      
          //Recipients 收件者
          $mail->setFrom('yourAccount@gmail.com', 'Po-chi Chen');
          $mail->addAddress('hw.pcchen@gmail.net', 'Po-chi Chen');     // Add a recipient
          // $mail->addAddress('ellen@example.com');               // Name is optional
          // 回復給
          $mail->addReplyTo('hw.pcchen@gmail.com', 'Po-chi Chen');
          // Copy To, 副本收件者
          $mail->addCC('yourccaccount@gmail.com');
          // Back Copy To, 隱密副本收件者
          // $mail->addBCC('bcc@example.com');
      
          // Attachments 附件檔案
          // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
          // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
      
          // Content 信件內容
          $mail->isHTML(true);                         // Set email format to HTML
          $mail->Subject = 'Here is the subject';
          $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
          $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
      
          $mail->send();
          echo 'Message has been sent';
      } catch (Exception $e) {
          echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
      }
      ```

      

2. 下載原始檔案，解壓，放在工作目錄下的 PHPMailerN ，我們的 php 程式碼的前端要改成：

   ```php
   <?php
   use PHPMailer\PHPMailer\PHPMailer;
   use PHPMailer\PHPMailer\Exception;
   use PHPMailer\PHPMailer\SMTP;
   
   require 'PHPMailerN/src/Exception.php';
   require 'PHPMailerN/src/PHPMailer.php';
   require 'PHPMailerN/src/SMTP.php';
   ```

為方便你自己使用，我們可以將帳號密碼寫在一個工具函式中。例如 phpmail.tools.php

```php
<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require 'PHPMailerN/src/Exception.php';
require 'PHPMailerN/src/PHPMailer.php';
require 'PHPMailerN/src/SMTP.php';

function pmail(){

    $mail = new PHPMailer(true);

    //Server settings
    // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
    $mail->isSMTP();                                            // Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'yourname@gmail.com';                     // SMTP username
    $mail->Password   = 'yourpassword';                               // SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    return $mail;
}
?>
```

程式碼就可以改成：

```php
<?php
require 'phpmail.tools.php';

// Instantiation and passing `true` enables exceptions

try {
    $mail = pmail();

    //Recipients
    $mail->setFrom('yourAccount@gmail.com', 'Your Name here');
    $mail->addAddress('hw.pcchen@gmail.com', 'Po-chi Chen');     // Add a recipient
    // $mail->addAddress('ellen@example.com');               // Name is optional
    $mail->addReplyTo('hw.pcchen@gmail.com', 'Po-chi Chen');
    $mail->addCC('yourcc@gmail.com');
    // $mail->addBCC('bcc@example.com');

    // Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Here is the subject';
    $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}
```

試試看，你是不是已經可以順利的使用 php 寄出一封電子郵件？

如果你剛才寄送的寄件人或是主旨有中文，你可能會發現收信的人看到的是亂碼。這是因為mail 傳輸以 ascii 進行，非 ascii 的資料要經過編碼，編碼使用 base64_encode()。以下範例為寄件人，收信人及主旨。

```php
    $sname = "=?utf-8?B?".base64_encode('陳大一')."?=";
    $mail->setFrom('hw.pcchen@gmail.com', $sname);
    $sname = "=?utf-8?B?".base64_encode('陳大二')."?=";
    $mail->addAddress('pcchen001@gmail.com', $sname);     // Add a recipient
    $sub = "=?utf-8?B?".base64_encode('中文的主旨')."?=";
    $mail->Subject = $sub;
    ...
```

至於信件內容，中文內容應該是 OK 的，若還是亂碼，只要設定 mail 的編碼為 UTF-8 就可以了！

$mail->Charset='UTF-8' 。

請注意到，如果 isHTML(true) 那麼 Body 內的字串會被用 HTML 渲染。

我們也可以將一個要寄送的 HTML 檔案先寫好存檔，然後使用 $mail->msgHTML('content.html') 渲染。

```php
$mail->isHTML(true);
$mail->Body= "html 的內容 ";
改成
$mail->msgHTML(file_get_contents('content.html'), __DIR__);
```

如果要附件檔案的話：

```PHP
$mail->addAttachment('images/phpmailer_mini.png');   // 放檔案路徑及檔名
```

下面有幾個範例，請一一測試： 

範例一  pemail/sendmail_01.html, send_01.php

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>線上送信系統</title>
    <script type="text/javascript">
      function send()
      {
        if (document.myForm.from_name.value.length == 0)
          alert("寄件者欄位不可以空白哦！");
        else if (document.myForm.from_email.value.length == 0)
          alert("寄件者電子郵件欄位不可以空白哦！");
        else if (document.myForm.to_name.value.length == 0)
          alert("收件者欄位不可以空白哦！");
        else if (document.myForm.to_email.value.length == 0)
          alert("收件者電子郵件欄位不可以空白哦！");
        else if (document.myForm.subject.value.length == 0)
          alert("郵件主旨欄位不可以空白哦！");
        else if (document.myForm.message.value.length == 0)
          alert("郵件內容欄位不可以空白哦！");
        else
          myForm.submit();
      }
    </script>
  </head>
  <body>
    <p align="center"><img src="title.jpg"></p>
    <form name="myForm" action="send_01.php" method="post">
      <table width="800" align="center" cellspacing="2" cellpadding="3">
        <tr bgcolor="#FFFFA1">
          <td width="12%">寄件者</td>
          <td width="25%">
            <input type="text" name="from_name" size="10">
          </td>
          <td width="63%">電子郵件地址
            <input type="text" name="from_email" size="30">
          </td>
        </tr>
        <tr bgcolor="#FFFFF0">
          <td>收件者</td>
          <td><input type="text" name="to_name" size="10"></td>
          <td>電子郵件地址
            <input name="to_email" type="text" size="30">
          </td>
        </tr>
        <tr bgcolor="#FFFFA1">
          <td>郵件主旨</td>
          <td colspan="2">
            <input type="text" name="subject" size="80">
          </td>
        </tr>
        <tr bgcolor="#FFFFF0">
          <td>郵件內容</td>
          <td colspan="2">
            <textarea name="message" cols="62" rows="5"></textarea>
          </td>
        </tr>
       </table>
      <p align="center"><input type="button" value="傳送郵件" onClick="send()"></p>
    </form>
  </body>
</html>
```

```php
<?php
  require '../phpmail.tools.php';

  //取得表單資料
  $from_name = "=?utf-8?B?" . base64_encode($_POST["from_name"]) . "?=";
  $from_email = $_POST["from_email"];
  $to_name = "=?utf-8?B?" . base64_encode($_POST["to_name"]) . "?=";
  $to_email = $_POST["to_email"];
  $subject = "=?utf-8?B?" . base64_encode($_POST["subject"]) . "?=";
  $message = $_POST["message"];

  //傳送郵件
  $mail = pmail();

  $mail->setFrom($from_email, $from_name);
  $mail->addReplyTo('pcchen@gm.nuu.edu.tw', 'First Last');
  $mail->addAddress($to_email, $to_name);
  $mail->Subject = $subject;
  $mail->Body=$message;

  if (!$mail->send()) {
      echo "Mailer Error: " . $mail->ErrorInfo;
  } else {
      echo "Message sent!";
  }
?>
```

範例二 pemail/sendmail_02.html, sent_02.php ( 附件檔案，上傳寄送 )

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>線上送信系統</title>
  </head>
  <body>
    <p align="center"><img src="title.jpg"></p>
    <form action="send_02.php" method="post" enctype="multipart/form-data" name="myForm">
      <table width="800" align="center" cellspacing="2" cellpadding="3">
        <tr bgcolor="#FFFFA1">
          <td width="12%">寄件者</td>
          <td width="25%">
            <input type="text" name="from_name" size="10">
          </td>
          <td width="63%">電子郵件地址
            <input type="text" name="from_email" size="30">
          </td>
        </tr>
        <tr bgcolor="#FFFFF0">
          <td>收件者</td>
          <td><input type="text" name="to_name" size="10"></td>
          <td>電子郵件地址
            <input name="to_email" type="text" size="30">
          </td>
        </tr>
        <tr bgcolor="#FFFFA1">
          <td>郵件主旨</td>
          <td colspan="2">
            <input type="text" name="subject" size="80">
          </td>
        </tr>
        <tr bgcolor="#FFFFF0">
          <td>郵件內容</td>
          <td colspan="2">
            <textarea name="message" cols="62" rows="5"></textarea>
          </td>
        </tr>
        <tr bgcolor="#CCCCFF">
          <td>附加檔案</td>
          <td colspan="2">
            <input type="file" name="myfile" size="80">
          </td>
        </tr>
      </table>
      <p align="center">
        <input type="submit" value="傳送郵件">
      </p>
    </form>
  </body>
</html>
```

```php
<?php
  //取得表單資料
  $from_name = "=?utf-8?B?" . base64_encode($_POST["from_name"]) . "?=";
  $from_email = $_POST["from_email"];
  $to_name = "=?utf-8?B?" . base64_encode($_POST["to_name"]) . "?=";
  $to_email = $_POST["to_email"];
  // $format = $_POST["format"];
  $subject = "=?utf-8?B?" . base64_encode($_POST["subject"]) . "?=";
  $message = $_POST["message"];

  //若檔名稱不是空白，表示上傳成功，新增附加檔案
  if ($_FILES{"myfile"}{"name"} != "")
  {
    $file = $_FILES{"myfile"}{"tmp_name"};
    $file_name = $_FILES{"myfile"}{"name"};
    $content_type = $_FILES{"myfile"}{"type"};
    $upload_dir =  "./upload files/";
    $upload_file = $upload_dir . $to = iconv("UTF-8", "Big5", $_FILES["myfile"]["name"]);

    //將上傳的檔案由暫存目錄移至指定之目錄
    move_uploaded_file($_FILES["myfile"]["tmp_name"], $upload_file);

  }

  //傳送郵件
  require '../phpmail.tools.php';
  $mail = pmail();
  $mail->setFrom($from_email, $from_name);
  $mail->addReplyTo('pcchen002@gmail.com', 'First Last');
  $mail->addAddress($to_email, $to_name);
  $mail->Subject = $subject;
  $mail->Body=$message;

  $mail->addAttachment($upload_file);

  if (!$mail->send()) {
      echo "Mailer Error: " . $mail->ErrorInfo;
  } else {
      echo "Message sent!";

  }
?>
```

範例三  pemail/send_03.php ( 內容來自一個 html 檔案)

```php
<?php
  require '../phpmail.tools.php';
  $mail = pmail();

  $mail->setFrom("hw.pcchen@gmail.com", "Pochi Chen");
  $mail->addReplyTo('pcchen@gm.nuu.edu.tw', 'First Last');
  $mail->addAddress("pcchen001@gmail.com", 'hw.pcchen');
  $mail->Subject = "=?utf-8?B?" . base64_encode('我的主題') . "?=";
  $mail->Body=" 不在網頁檔的內容";
  $mail->msgHTML(file_get_contents('content.html'), __DIR__);  // 內容檔案
  if (!$mail->send()) {
      echo "Mailer Error: " . $mail->ErrorInfo;
  } else {
      echo "Message sent!";
  }
?>
```

content.html 範例

```html
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <p>html as content</p>
    <h2>恭喜發信成功</h2>
    <p>老師 </p>
  </body>
</html>
```

三個範例都完成後，我們要跟資料庫結合。這一部分請參考課本範例：

ecard/ 是一個電子賀卡的範例，雖然有一點過時，但還是可以充分練習資料庫讀寫以及檔案的應用。

資料庫的部分，請執行下列 SQL 指令，建立表格 card_message。你也可以自己建立，改掉所有欄位的名稱。

```sql
CREATE TABLE IF NOT EXISTS `card_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_id` varchar(30) NOT NULL DEFAULT '',
  `subject` tinytext NOT NULL,
  `from_name` varchar(30) NOT NULL DEFAULT '',
  `from_email` varchar(50) NOT NULL DEFAULT '',
  `to_name` varchar(30) NOT NULL DEFAULT '',
  `to_email` varchar(50) NOT NULL DEFAULT '',
  `music` varchar(30) NOT NULL DEFAULT '',
  `style` varchar(20) NOT NULL DEFAULT '',
  `size` varchar(10) NOT NULL DEFAULT '',
  `color` varchar(20) NOT NULL DEFAULT '',
  `message` mediumtext NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
```

這裡包含了流水號、寄件、收件、message 等。其他都是將卡片參數化之後的選項。課本事先準備了幾張影像及音樂檔，我們要做的只是讓使用者可以選擇，選定之後就將一張賀卡的內容及設定存成一筆資料，從這一筆資料，我們可以產生一張賀卡。賀卡寄達的就是一個 超連結。所以我們的步驟是：

1. 填寫表單，完成一張賀卡的內容。送給 preview.php 處理，
2. preview.php 根據設定的內容，展示卡片預覽。若沒問題，則送出給 ecard.php
3. ecard.php 先存資料庫，在形成所需的超連結，寄送超連結給收信者。
4. 收信者收到信，點擊信上的超連結，便可以到 get_card.php 去看賀卡。

主要程式碼如下： index0.php

```php
<?php
  $from_name = "";
  $from_email = "";
  $to_name = "";
  $to_email = "";
  $subject = "";
	
  if (isset($_GET["from_name"]))
  {
    $from_name = $_GET["from_name"];
    $from_email = $_GET["from_email"];
    $to_name = $_GET["to_name"];
    $to_email = $_GET["to_email"];
    $subject = $_GET["subject"];
  }
?>
<!doctype html>
<html>
  <head>
    <title>電子賀卡 DIY</title>
    <meta charset="utf-8">
    <script type="text/javascript">
      function check_data()
      {
        if (document.eCard.subject.value.length == 0)
          alert("卡片通知標題欄位不可以空白哦！");
        else if (document.eCard.from_name.value.length == 0)
          alert("寄件人姓名欄位不可以空白哦！");
        else if (document.eCard.from_email.value.length == 0)
          alert("寄件人電子郵件欄位不可以空白哦！");
        else if (document.eCard.to_name.value.length == 0)
          alert("收件人姓名欄位不可以空白哦！");
        else if (document.eCard.to_email.value.length == 0)
          alert("收件人電子郵件欄位不可以空白哦！");
        else if (document.eCard.message.value.length == 0)
          alert("卡片內容欄位不可以空白哦！");		
        else
          eCard.submit();
      }
    </script>
  </head>		
  <body>
    <p align="center"><img src="images/title.jpg"></p>
    <form name="eCard" method="post" action="preview.php" >
      <p><img src="images/步驟一.jpg"></p>
      <table align="center" width="750">
        <?php
          $card_id = 0;
					
          for ($i = 1; $i <= 2; $i++)
          {
            for ($j = 1; $j <= 4; $j++)
            {
              $card_id += 1;
              echo "<td>";
              echo "<input type='radio' name='card_id' value='"; 
              echo $card_id .".jpg' checked>";
              echo "<a href='images/" . $card_id . ".jpg' target='_blank'>";
              echo "<img src='images/" . $card_id . "s.jpg' border='1'></a>";
              echo "</td>";
            }
            echo "</tr>";
          }
        ?>
      </table>
      <p><img src="images/步驟二.jpg"></p>
      <table width="750" align="center" cellspacing="0" cellpadding="4">
        <tr bgcolor="#F36B9D"> 
          <td colspan="2">1.卡片通知標題：
            <input name="subject" size="50" value="<?php echo $subject ?>"></td>
        </tr>
        <tr bgcolor="#FEA1C1"> 
          <td>2. 寄信人姓名：
            <input name="from_name" size="10" value="<?php echo $from_name ?>"></td>
          <td>電子郵件信箱：
            <input name="from_email" size="30" value="<?php echo $from_email ?>"></td>
        </tr>
        <tr bgcolor="#F36B9D"> 
          <td>3. 收信人姓名：
            <input name="to_name" size="10" value="<?php echo $to_name ?>"></td>
          <td>電子郵件信箱：
            <input name="to_email" size="30" value="<?php echo $to_email ?>"></td>
        </tr>
        <tr bgcolor="#FEA1C1"> 
          <td colspan="2">4. 選擇音樂： 
            <select name="music">
              <option value="music_01.mid" selected>如果雲知道</option>
              <option value="music_02.mid">往事隨風</option>
              <option value="music_03.mid">流浪的小孩</option>
              <option value="music_04.mid">棋子</option>
            </select>
          </td>
        </tr>
        <tr bgcolor="#F36B9D"> 
          <td colspan="2">5. 選擇卡片字體 ： 
            <select name="style">
              <option value="normal" selected>一般</option>
              <option value="italic">斜體</option>
            </select>
          　大小： 
            <select name="size">
              <option value="8 pt">8 pt</option>
              <option value="10 pt">10 pt</option>
              <option value="12 pt" selected>12 pt</option>
              <option value="14 pt">14 pt</option>
              <option value="16 pt">16 pt</option>
              <option value="20 pt">20 pt</option>
              <option value="24 pt">24 pt</option>
            </select>
          　顏色： 
            <select name="color">
              <option value="#000000" selected>黑色</option>
              <option value="#FF0000">紅色</option>
              <option value="#FF6633">橙色</option>
              <option value="#339900">綠色</option>
              <option value="#0099FF">藍色</option>
              <option value="#9933FF">紫色</option>
              <option value="#BB0000">暗紅色</option>
              <option value="#005500">深綠色</option>
              <option value="#333399">深藍色</option>
              <option value="#663399">深紫色</option>
              <option value="#770000">咖啡色</option>
            </select>
          </td>
        </tr>
        <tr bgcolor="#FEA1C1"> 
          <td colspan="2">6.卡片內容：<br>&nbsp;&nbsp;&nbsp;&nbsp;
            <textarea cols="72" name="message" rows="8"></textarea>
          </td>
        </tr>
      </table><br>
      <p align="center">
        <input type="button" value="卡片預覽" onClick="check_data()"> 
        <input type="reset" value="重新填寫">
      </p>
    </form>
  </body>
</html>
```

preview.php

```php
<?php
  //取得表單資料
  $card_id = $_POST["card_id"];
  $subject = $_POST["subject"];
  $from_name = $_POST["from_name"];
  $from_email = $_POST["from_email"]; 
  $to_name = $_POST["to_name"]; 
  $to_email = $_POST["to_email"]; 
  $music = $_POST["music"]; 
  $style = $_POST["style"];
  $size = $_POST["size"];
  $color = $_POST["color"];
  $message = $_POST["message"];
?>
<!doctype html>
<html>
  <head>
    <title>卡片預覽</title>
    <meta charset="utf-8">
    <embed src="music/<?php echo $music ?>" hidden loop="true"></embed>
  </head>
  <body>
    <p align="center"><img src="images/preview.jpg"></p>
    <table width="800" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr> 
        <td colspan="2" bgcolor="#FFFFCC"> 
          <p align="center"><br><img src="images/<?php echo $card_id ?>"><br><br></p></td>
      </tr>
      <tr> 
        <td colspan="2" height="22" bgcolor="#CCCCFF" valign="middle"> 
          <p align="center"><font color="#9900CC">標題：<?php echo $subject ?></font></p>
        </td>
      </tr>
      <tr> 
        <td width="28%" bgcolor="#FFC6E2" valign="top"> 
          <br><font color="#9900CC">
          寄件日期：<?php echo date("Y-m-d H:i:s") ?><br>
          寄件人：<a href="MAILTO:<?php echo $from_email ?>"><?php echo $from_name ?></a><br>
          收件人：<a href="MAILTO:<?php echo $to_email ?>"><?php echo $to_name ?></a></font>
        </td>
        <td bgcolor="#FFC6E2">
          <br><font color="#0000FF">Dear, <?php echo $to_name ?>：</font>
          <p style="font-style:<?php echo $style ?>; color:<?php echo $color ?>;
            font-size:<?php echo $size ?>; padding-left:3mm; padding-right:3mm">
            <?php echo $message ?>
          </p>
          <div align="right">
            <font color="#0000FF">Write By <?php echo $from_name ?></font>
          </div>
        </td>
      </tr>
    </table>
    <form action="ecard.php" method="post" >
      <input type="hidden" name="card_id" value="<?php echo $card_id ?>">
      <input type="hidden" name="subject" value="<?php echo $subject ?>">
      <input type="hidden" name="from_name" value="<?php echo $from_name ?>">
      <input type="hidden" name="from_email" value="<?php echo $from_email ?>">
      <input type="hidden" name="to_name" value="<?php echo $to_name ?>">
      <input type="hidden" name="to_email" value="<?php echo $to_email ?>">
      <input type="hidden" name="music" value="<?php echo $music ?>">
      <input type="hidden" name="style" value="<?php echo $style ?>">
      <input type="hidden" name="size" value="<?php echo $size ?>">
      <input type="hidden" name="color" value="<?php echo $color ?>">
      <input type="hidden" name="message" value="<?php echo $message ?>">
      <p align="center">
        <a href="javascript:history.back()"><img src="images/modify.gif" border="0"></a>
        <input type="image" src="images/sent.gif">
      </p>
    </form>
  </body>
</html>
```

ecard.php

```php
<?php
  require_once("dbtools.inc.php");

  //取得表單資料
  $card_id = $_POST["card_id"];
  $subject = $_POST["subject"];
  $from_name = $_POST["from_name"];
  $from_email = $_POST["from_email"];
  $to_name = $_POST["to_name"];
  $to_email = $_POST["to_email"];
  $music = $_POST["music"];
  $style = $_POST["style"];
  $size = $_POST["size"];
  $color = $_POST["color"];
  $message = $_POST["message"];
  $date = date("Y-m-d H:i:s");

  //建立資料連接
  $link = create_connection();

  //執行 INSERT INTO 陳述式來將賀卡內容寫入 card_message 資料表
  $sql = "INSERT INTO card_message (card_id, subject, from_name, from_email,
          to_name, to_email, music, style, size, color, message, date)
          VALUES ('$card_id', '$subject', '$from_name', '$from_email',
          '$to_name', '$to_email', '$music', '$style', '$size', '$color',
          '$message', '$date')";
  $result = execute_sql($link, "u0533001", $sql);

  //執行 SELECT 陳述式取出剛才加入記錄的 id 欄位
  $sql = "SELECT id FROM card_message WHERE subject='$subject' AND date='$date'";
  $result = execute_sql($link, "u0533001", $sql);

  //取得 id 欄位的值
  $id = mysqli_fetch_object($result)->id;

  //關閉資料連接
  mysqli_free_result($result);
  mysqli_close($link);

  //設定檢視電子賀卡的網址
  //$current_url = "http://" . $_SERVER["REMOTE_ADDR"] . $_SERVER["PHP_SELF"];
  $current_url = "http://localhost/database-programming-108b/examples/ch16eMail/ecard/ecard.php";
  $get_ecard_url = str_replace("ecard.php", "get_ecard.php", $current_url);
  $get_ecard_url .= "?id=$id&subject=" . urlencode($subject);

  //指定郵件內容
  $message = "<h1 align='center'>翠墨資訊電子賀卡站</h1>";
  $message .= "<p>親愛的【" . $to_name . "】：</p>";
  $message .= "<p>【" . $from_name . "】透過本站寄給您一張電子賀卡</p>";
  $message .= "<p>您可以到以下網址觀看您的卡片：</p>";
  $message .= "<p align='center'><a href='$get_ecard_url'>";
  $message .= "按此可以觀看卡片</a></p>";
  $message .= "<p align='right'>寄件日期：$date</p>";

  $subject = "=?utf-8?B?" . base64_encode($subject) . "?=";
  $from_name = "=?utf-8?B?" . base64_encode($from_name) . "?=";
  $to_name = "=?utf-8?B?" . base64_encode($to_name) . "?=";

  //若要傳送 HTML 格式的郵件，須指定 Content-type 標頭資訊
  // $headers  = "MIME-Version: 1.0\r\n";
  // $headers .= "Content-type: text/html; charset=utf-8\r\n";
  // $headers .= "From: $from_name<$from_email>\r\n";
  // $headers .= "To: $to_name<$to_email>\r\n";
  //
  // //傳送郵件
  // mail($to_email, $subject, $message, $headers);
  require '../phpmail.tools.php';
  $mail = pmail();
  $mail->setFrom($from_email, $from_name);
  //Set who the message is to be sent to
  $mail->addAddress($to_email, $to_name);
  //Set the subject line
  $mail->Subject = $subject;
  $mail->msgHTML($message);
  if (!$mail->send()) {
      echo "Mailer Error: " . $mail->ErrorInfo;
  } else {
      echo "Message sent!";
  }

?>
```

請特別留意網址的部分，剛開始自己測試時，可以使用 localhost 。若要收信者能開你的賀卡，那就要將程式放到 有固定網址的網站，例如： http://coding.im.nuu.edu.tw/users/U0533001/ecard... 。

get_card.php

```php
<?php
  require_once("dbtools.inc.php");

  //取得電子賀卡的編號及卡片標題
  $id = $_GET["id"];
  $subject = $_GET["subject"];

  //建立資料連接
  $link = create_connection();

  //執行 SQL 命令
  $sql = "SELECT * FROM card_message WHERE id = $id AND subject = '$subject'";
  $result = execute_sql($link, "u0533001", $sql);

  //取得賀卡各欄位的值
  if (mysqli_num_rows($result) != 0)
  {
    $row = mysqli_fetch_object($result);
    $id = $row->id;
    $card_id = $row->card_id;
    $subject = $row->subject;
    $from_name = $row->from_name;
    $from_email = $row->from_email;
    $to_name = $row->to_name;
    $to_email = $row->to_email;
    $music = $row->music;
    $style = $row->style;
    $size = $row->size;
    $color = $row->color;
    $message = $row->message;
    $date = $row->date;

    //釋放 $result 佔用的記憶體
    mysqli_free_result($result);
  }
  else
  {
    echo "沒有您的卡片或卡片已經刪除...";
    exit();
  }

  //關閉資料連接
  mysqli_close($link);

  //設定回覆電子賀卡的參數
  $parameter = "from_name=" . urlencode($to_name) . "&to_email=" . $from_email;
  $parameter .= "&to_name=" . urlencode($from_name) . "&from_email=" . $to_email;
  $parameter .= "&subject=Re:" . urlencode($subject);
?>
<!doctype html>
<html>
  <head>
    <title>觀看卡片</title>
    <meta charset="utf-8">
    <embed src="music/<?php echo $music ?>" hidden loop="true"></embed>
  </head>
  <body>
    <p align="center"><img src="images/view.jpg"></p>
    <table width="800" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="2" bgcolor="#FFFFCC">
          <p align="center"><br><img src="images/<?php echo $card_id ?>"><br><br></p></td>
      </tr>
      <tr>
        <td colspan="2" height="22" bgcolor="#CCCCFF" valign="middle">
          <p align="center"><font color="#9900CC">標題：<?php echo $subject ?></font></p>
        </td>
      </tr>
      <tr>
        <td width="28%" bgcolor="#FFC6E2" valign="top">
          <font color="#9900CC"><br>
          寄件日期：<?php echo $date ?><br>
          寄件人：<a href="mailto:<?php echo $from_email ?>"><?php echo $from_name ?></a><br>
          收件人：<a href="mailto:<?php echo $to_email ?>"><?php echo $to_name ?></a></font>
        </td>
        <td bgcolor="#FFC6E2">
          <font color="#0000FF"><br>Dear, <?php echo $to_name ?>：</font>
          <p style="font-style:<?php echo $style ?>; color:<?php echo $color ?>;
            font-size:<?php echo $size ?>; padding-left:3mm; padding-right:3mm">
            <?php echo $message ?>
          </p>
          <div align="right">
            <font color="#0000FF">Write By <?php echo $from_name ?></font>
          </div>
        </td>
      </tr>
    </table>
    <p align="center">
    <a href="index.php?<?php echo $parameter ?>">我也要寄卡片給他（她）</a></p>
  </body>
</html>
```

## 註冊確認信

註冊後，產生一組亂數，假設6個阿拉伯數字。將這個阿拉伯數字連同帳號和成一個超連結寄給註冊者。

註冊者點選超連結後，進入網頁，將資料表中的使用者的開啟打開。進入一個歡迎頁面，通知帳號開啟成功。

我們用會員管理的流程來學習。

假設我們的會員資料表結構如下： (跟之前的應用類似，在 phpmyadmin 執行建立資料表)

```sql
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(20) NOT NULL DEFAULT '',
  `passwd` varchar(20) NOT NULL DEFAULT '',
  `state` varchar(5) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;
```

先準備資料填寫的表單：

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <form action="adduser.php" method="post">
        帳號： <input type="text" name="username"><br>
        請以 email 帳號作為你的帳號，註冊後請收信開啟帳號！ <br>
        <input type="text" name="userpass"> <br>
        <input type="submit" value="註冊">
    </form>
</body>
</html>
```

adduser.php

```php+HTML
<?php
require_once '../dbtools.inc.php';
$uname = $_POST['username'];
$upass = $_POST['userpass'];
$state = "12345";
for($i = 0; $i < 5; $i++){
    $state[$i]= chr(random_int(ord('A'), ord('Z')));
}
$link = create_connection();
$sql = "INSERT INTO users(account, passwd, state) VALUES('$uname', '$upass', '$state');";
$result = execute_sql($link, 'u0533001', $sql);
echo $result;

require '../phpmail.tools.php';
$mail = pmail();

$to_email=$to_name=$uname;
$from_email="pcchen002@gmail.com";
$from_name="=?utf-8?B?".base64_encode('大衣公司')."?=";
$mail->setFrom($from_email, $from_name);
$mail->addAddress($to_email, $to_name);
$mail->Subject = "account confirm";
$message="帳號開通： <a href='http://localhost/database-programming-108b/examples/ch16email/mailConfirm/active.php?uname=$uname&state=$state'>開啟</a>";
$mail->isHTML(true);
$mail->Body=$message;

if( !$mail->send()){
    echo 'mail send fail!';
}else{
    echo 'mail send!';
}
?>
```

開啟帳號，將 state 設定為 OK 。之後登入時除了檢查帳密外，還需檢查 state 是否為 OK。

```php
<?php
$uname = $_GET['uname'];
$state = $_GET['state'];

require_once "../dbtools.inc.php";
$link = create_connection();
$sql = "SELECT state from users WHERE account = '$uname'";
$result = execute_sql($link, 'u0533001', $sql);

$row = mysqli_fetch_row($result);

if( $row[0] == $state){

    $sql = "UPDATE users SET state='OK' WHERE account = '$uname'";
    execute_sql($link, 'u0533001', $sql);
    echo "confirmed";
}
else{
    header("location: reg.html");
}
?>
```

到目前為止，我們建立了信件開啟帳戶的雛型程式，同學以此為例，可以讓你的系統更加完善。