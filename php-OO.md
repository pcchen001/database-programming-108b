---
title: PHP 物件導向
date: 2020-3-1 11:11:11
tags:
- PHP
- OO
categories: 
- 資料庫程式設計
---

# php 的物件導語法

結構與物件例項化

```php
class ClassName{ }
// 例項化
$obj = new ClassName(); 　　　　　　　　　　　　　　 
$obj = new ClassName($v1, $v2, ...);  
```

建構子及解構子

__construct(){ }  或是

__construct($v1, $v2, ...) { }

解構子 __destruct(){ }

範例：

```php
class MyFirstClass{
  var $b;    // 資料成員
  function __construct(){
        $this->b = 100;    // 資料成員初始化
    }
    function addabc($c){
        return $this->b + $c; 
    }
}
```

註： 父類別的建構子必須冠上 parent, 也就是 parent::__construct()。

資料成員

資料成員分為 public, protected, 以及 private。 private 不需前飾，其餘則需要。例如：

```php
public $d;
protected $v1 = 100; 
var $vp = 0;   // 私有資料成員
```



| magic methods                                                |
| ------------------------------------------------------------ |
| magic methods 是一系列以二個底線開頭的方法，當我們定義這些方法時，系統會在特定的時機呼叫這些方法。例如 \_\_construct()，\_\_destruct()等。 對於私有資料成員，我們可以定義 __get() 及 \_\_set() 函式 |

```
  __construct(),
  __destruct(),
  __get(),
  __set()
```

成員函式一樣分為public, protected, private 三種，預設為 private。關於建構子、static function, const 我們在下面的範例中說明：

```
class Employee	
{
  public $Name;
  const PI = 3.14115;
  function __construct($n){
      $this->Name = $n;
  }
  public function ShowName()
  {
    echo '這名員工的名字為'.$this->Name;
  }
  public static function Salary(){
      return 35000;
  }
}

$e = new Employee('小英');
$e->Name = '大雄';
$e->ShowName();
echo Employee::Salary();
echo Employee::PI;
```

#### 匿名類別

```php
<?php
  $Obj = new class('小紅豆')
  {
    public $Name;
    function __construct($Str)
    {
      $this->Name = $Str;
      echo '已經建立名字為'.$this->Name.'的物件！';
    }
  };
 ?>

```

#### 繼承 

extends

#### 抽象類別

```php
abstract class Payroll{
    public var $Name;
    abstract public function Payment($a, $b);
}
class BPx extends Payroll{
    public function Payment($p, $m){
       return $p * $m * 12;
    }
}
```

#### 命名空間

 「命名空間」(namespace) 是一種命名方式，用來組織各個類別、函式、常數等，它和這些元素的關係就像檔案系統中目錄與檔案的關係一樣。

   PHP 的命名空間用反斜線分隔子空間。

   舉例來說，假設MyClass隸屬於 \A\B\C命名空間，那麼若要建立一個名稱為Obj、隸屬於MyClass的物件，可以寫成如下：

```
$obj = new \A\B\C\MyClass();
```

在定義類別時，我們可以為目前的類別冠上一個命名空間：

```
namespace App\Http\Components;
```

使用已經定義好的類別，若該類別有比較長的命名空間，我們可以加上 

```
use Illuminate\Http\Request; 
```

省略接下來呼叫該命名空間內函式或類別時，必須嘉上的完整名稱。

順便提一下，引入其他檔案的類別或函式定義，我們使用

```
require 'filename.php';
```

或是

```
require_once 'filename.php';
```

