---
title: App Shopping
date: 2020-04-19 11:22:47
tags:
categories:
- Application
---

本應用的範例，利用了下列的資料表：

```sql
CREATE TABLE IF NOT EXISTS `product_list` (
  `book_no` varchar(20) NOT NULL DEFAULT '',
  `book_name` varchar(30) NOT NULL DEFAULT '',
  `price` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`book_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

因為我們主題在購物，不在電子商務，因此先填入一些資料，方便設計程式：

```sql
INSERT INTO `product_list` (`book_no`, `book_name`, `price`) VALUES
('EE0018', '最新計算機概論', 560),
('EE0069', '電腦網路概論與實務', 580),
('EL0063', 'ASP.NET 3.5 網頁程式設計', 580),
('EN0010', '網路概論', 580),
('P766', 'PHP 5 & MySQL 程式設計', 580),
('P816', 'Visual Basic 2008 程式設計', 560),
('P818', 'Visual C# .2008 程式設計', 580);
```

登入頁面其實是不需要的，因為我們並沒有要檢查帳號密碼，但是了頁面順暢，我們存了使用者名稱。index0.html:

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>購物車</title>
  </head>
  <body bgcolor="lightyellow">
    <form method="post" action="main.php">
      <p align="center">
        <img src="fig1.jpg"><br><br>請輸入您的名字：
        <input type="text" name="name" size="20">
        <input type="submit" value="登入">   
        <input type="reset" value="重新輸入">
      </P>                     
    </form>
  </body> 
</html>
```

點選登入，進入 mail.php，原則上這是一個 html 檔案，使用 iframe 來分隔頁面。雖然近年來已經愈來愈少用 frame 來處理頁面，但這還是一個好用的小工具。

```php+HTML
<?php
  setcookie("name", $_POST["name"]);
?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>購物車</title>
  </head>
  <frameset rows="60, *" border="0">
    <frame name="top" noresize scrolling="no" src="show_link.html">
    <frame name="bottom" noresize src="catalog.php">
  </frameset>
</html>
```

show_link.html 僅僅只是放置選項連結，下方的 frame, 我們準備放置 catalog.php, shopping_car.php 以及 print_order.php 。

catalog.php

```php+HTML
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body bgcolor="lightyellow">
    <table border="0" align="center" width="800" cellspacing="2">
      <tr bgcolor="#BABA76" height="30" align="center">
        <td>書號</td>
        <td>書名</td>
        <td>定價</td>
        <td>輸入數量</td>
        <td>進行訂購</td>
      </tr>
        <?php
          require_once("dbtools.inc.php");

          //建立資料連接
          $link = create_connection();

          //篩選出所有產品資料
          $sql = "SELECT * FROM product_list";
          $result = execute_sql($link, "u0533001", $sql);

          //計算總記錄數
          $total_records = mysqli_num_rows($result);

          //列出所有產品資料
          for ($i = 0; $i < $total_records; $i++)
          {
            //取得產品資料
            $row = mysqli_fetch_assoc($result);

            //顯示產品各欄位的資料
            echo "<form method='post' action='add_to_car.php?book_no=" .
              $row["book_no"] . "&book_name=" . urlencode($row["book_name"]) .
              "&price=" . $row["price"] . "'>";
            echo "<tr align='center' bgcolor='#EDEAB1'>";
            echo "<td>" . $row["book_no"] . "</td>";
            echo "<td>" . $row["book_name"] . "</td>";
            echo "<td>$" . $row["price"] . "</td>";
            echo "<td><input type='text' name='quantity' size='5' value='1'></td>";
            echo "<td><input type='submit' value='放入購物車'></td>";
            echo "</tr>";
            echo "</form>";
          }

          //釋放資源及關閉資料連接
          mysqli_free_result($result);
          mysqli_close($link);
        ?>
    </table>
  </body>
</html>
```

shopping_car.php，我們的重點在如何在 cookie 中存放複雜的購物資訊：

```php
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
  </head>	
  <body bgcolor="LightYellow">
    <p align="center"><img src='fig1.jpg'></p>
    <table border="0" align="center" width="800">
      <tr bgcolor="#ACACFF" height="30" align="center">
        <td>書號</td>
        <td>書名</td>
        <td>定價</td>
        <td>數量</td>		
        <td>小計</td>
        <td>變更數量</td>		
      </tr>				
        <?php
          //若購物車是空的，就顯示 "目前購物車內沒有任何產品及數量" 訊息
          if (empty($_COOKIE["book_no_list"]))
          {
            echo "<tr align='center'>";
            echo "<td colspan='6'>目前購物車內沒有任何產品及數量！</td>";	
            echo "</tr>";
          }
          else
          {
            //取得購物車資料
            $book_no_array = explode(",", $_COOKIE["book_no_list"]);
            $book_name_array = explode(",", $_COOKIE["book_name_list"]);
            $price_array = explode(",", $_COOKIE["price_list"]);		
            $quantity_array = explode(",", $_COOKIE["quantity_list"]);		
					
            //顯示購物車內容
            $total = 0;			
            for ($i = 0; $i < count($book_no_array); $i++)
            {
              //計算小計
              $sub_total = $price_array[$i] * $quantity_array[$i];
						
              //計算總計
              $total += $sub_total;
						
              //顯示各欄位資料
              echo "<form method='post' action='change.php?book_no=" . 
                $book_no_array[$i] . "'>";						
              echo "<tr bgcolor='#EDEAB1'>";
              echo "<td align='center'>" . $book_no_array[$i] . "</td>";		
              echo "<td align='center'>" . $book_name_array[$i] . "</td>";			
              echo "<td align='center'>$" . $price_array[$i] . "</td>";
              echo "<td align='center'><input type='text' name='quantity' value='" . 
                $quantity_array[$i] . "' size='5'></td>";			
              echo "<td align='center'>$" . $sub_total . "</td>";
              echo "<td align='center'><input type='submit' value='修改'></td>";					
              echo "</tr>";
              echo "</form>";						
            }
					
            echo "<tr align='right' bgcolor='#EDEAB1'>";
            echo "<td colspan='6'>總金額 = " . $total . "</td>";	
            echo "</tr>";	
            echo "<tr align='center'>";
            echo "<td colspan='6'>" . "<br><input type='button' value='退回所有產品'
              onClick=\"javascript:window.open('delete_order.php','_self')\">";
            echo "</td>";	
            echo "</tr>";	
          }
      ?>
    </table>
  </body>                                                                                 
</html>
```

最後是列印訂單 print_order.php，電子商務近年盛行，劃撥單可能已經不多人使用了！

值得留意的是如何從 cookie 中取出資訊進行訂單列印。

```php+HTML
<?php
  //若購物車是空的，就顯示尚未選購產品
  if (empty($_COOKIE["book_no_list"]))
  {
    echo "<script type='text/javascript'>";
    echo "alert('您尚未選購任何產品');";
    echo "history.back();";		
    echo "</script>";
  }
?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body background="bg1.jpg">
    <h3>注意事項</h3>
    <ol type="1">
      <li>
        訂購方法一：請填妥信用卡專用訂購單後裝訂，免貼郵票，
        直接投郵即可，亦可放大傳真至 02-23695588。
      </li>
      <li>
        訂購方法二：請利用郵局劃撥單，填妥姓名、戶名、書名、數量、
        電話，直接至郵局劃撥付款。帳號：12345678戶名：快樂書城
      </li>
      <li>
        寄書與補書：您將於付款之後的3-5天收到書籍，若沒有收到，
        請來電洽詢 02-23695599。
      </li>
    </ol>
    <hr>
    <table border="1" bgcolor="white" rules="cols" align="center" cellpadding="5">
    <tr height="25">
				<td colspan="4" align="Center" bgcolor="#CCCC00">個人資料</td>
    </tr>
    <tr height="25">
      <td colspan="4">姓名：<u><?php echo $_COOKIE["name"] ?>
        <?php for ($i = 0; $i <= 100 - 2* strlen($_COOKIE["name"]); $i++) echo "&nbsp;"; ?></u>
      </td>
    </tr>
    <tr height="25">
      <td colspan="4">電話：
        <u><?php for ($i = 0; $i <= 100; $i++) echo "&nbsp;"; ?></u>
      </td>
    </tr>
    <tr height="25">
      <td colspan="4">地址：
        <u><?php for ($i = 0; $i <= 100; $i++) echo "&nbsp;"; ?></u>
      </td>
    </tr>
    <tr height="25">
      <td colspan="4">
        郵寄方式：□國內限時&nbsp;&nbsp;&nbsp;&nbsp;□國內掛號 (另加20元郵資)
      </td>
    </tr>
    <tr height="25">
      <td colspan="4">
        付款方式：□JCB CARD&nbsp;&nbsp;&nbsp;□VISA CARD&nbsp;&nbsp;&nbsp;□MASTER CARD
      </td>
    </tr>
    <tr height="25">
      <td colspan="4">
        信用卡卡號：<u><?php for ($i = 0; $i <= 89; $i++) echo "&nbsp;"; ?></u>
      </td>
    </tr>
    <tr height="25">
      <td colspan="4">
        有效日期：<u>西元<?php for ($i = 0; $i <= 85; $i++) echo "&nbsp;"; ?></u>
      </td>
    </tr>
    <tr height="25">
      <td colspan="4">
        簽名(與信用卡簽名相同)：<u><?php for ($i = 0; $i <= 66; $i++) echo "&nbsp;"; ?></u>
      </td>
    </tr>
    <tr height="25">
      <td colspan="4">
        支付總金額：<u><?php for ($i = 0; $i <= 89; $i++) echo "&nbsp;"; ?></u>
      </td>
    </tr>
    <tr height="25">
      <td colspan="4">
        開立發票：□二聯式&nbsp;&nbsp;&nbsp;&nbsp;□三聯式
      </td>
    </tr>
    <tr height="25">
      <td colspan="4">
        發票地址：<u><?php for ($i = 0; $i <= 93; $i++) echo "&nbsp;"; ?></u>
      </td>
    </tr>
    <tr height="25">
      <td colspan="4">
        統一編號：<u><?php for ($i = 0; $i <= 93; $i++) echo "&nbsp;"; ?></u>
      </td>
    </tr>
    <tr height="25">
      <td colspan="4" align="center" bgcolor="#CCCC00">訂單細目</td>
    </tr>
    <tr height="25" align="center" bgcolor="FFFF99">
      <td>書名</td>
      <td>定價</td>
      <td>數量</td>
      <td>小計</td>																
    </tr>			
      <?php
        //取得購物車資料
        $book_name_array = explode(",", $_COOKIE["book_name_list"]);
        $price_array = explode(",", $_COOKIE["price_list"]);		
        $quantity_array = explode(",", $_COOKIE["quantity_list"]);		
					
        //顯示購物車內容
        $total = 0;		
        for ($i = 0; $i < count($book_name_array); $i++)
        {
          //計算小計
          $sub_total = $price_array[$i] * $quantity_array[$i];
					
          //計算總計
          $total += $sub_total;
					
          //顯示各欄位資料
          echo "<tr>";	
          echo "<td align='center'>" . $book_name_array[$i] . "</td>";			
          echo "<td align='center'>$" . $price_array[$i] . "</td>";
          echo "<td align='center'>" . $quantity_array[$i] . "</td>";
          echo "<td align='center'>$" . $sub_total . "</td>";
          echo "</tr>";
        }
        echo "<tr align='right' bgcolor='#CCCC00'>";
        echo "<td colspan='4'>總金額 = " . $total . "</td>";	
        echo "</tr>";	
      ?>
    </table>
  </body>
</html>
```

我們距離電子商務愈來愈近了，期末專題就是要打造一個電子商務網站，從註冊，登入，貨物上架，下架，購物訂單處理等，形成一個網站服務！

最後，若要取消訂單：

delete_order.php

```
<?php
  //清除購物車資料
  setcookie("book_no_list", "");
  setcookie("book_name_list", "");
  setcookie("price_list", "");
  setcookie("quantity_list", "");	
	
  //導向到shopping_car.php網頁
  header("location:shopping_car.php");	
?>
```

