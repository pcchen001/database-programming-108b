---
title: 資料庫程式設計 課程簡介
date: 2020-2-20 21:28:43
tags: 
- 
categories: 
- README
---



# Database Programming 108b

資料庫程式設計 108下學期
PHP + MariaDB + Apache (Wamp)

1. 課程介紹
2. 網站基本架構
3. PHP基本語法(Ch.2, 3, 4)
4. PHP本地檔案存取(Ch.5)
5. PHP繪圖(Ch.6)
6. PHP進階語法
   1. Error(Exception)處理(Ch.7)
   2. 物件導向(Ch.8)
7. 網頁表單(Ch.9)
   1. Form
   2. Http Request/Response
   3. Cookie
   4. Session
9. 資料庫存取　MariaDB ch13
9. 範例：留言板、討論區 ch15
10. 檔案上傳：ch14
11. 範例：投票統計圖 ch18
12. 寄信、範例；電子賀卡 ch16
13. 範例：會員管理　ch17
14. 範例：購物車 ch19 、購物流程管理
15. 範例：相簿系統 ch20
16. Ajax
    1. fetch 指令、chart.js 結合。

作業：

1. 繪圖與統計圖表
2. 註冊與登入 session
3. 新增、刪除、修改、查詢
   1. 期限：期中考周
4. 登入後的討論區應用（選項）
   1. 期限：5/5
5. 