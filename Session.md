---
title: Session
date: 2020-03-19 16:38:38
tags:
- session
- html
- server
categories:
- http
- php
---

# Session

session 是一個能夠暫時紀錄與瀏覽器互動的機制。

一個客戶端開啟一個伺服器　php 程式時，可以開始記錄。例如：

```
<?php
    $_SESSION['name']="dearUser";
?>
```

下一個從相同瀏覽器過來開啟同一個或另一個 php 程式時，我們可以檢查這個 session 是否開始記錄了，根據紀錄的內容以及是否有紀錄可以判斷這個使用者剛才是否合法。

Session 用來確保一段時間內的認證是有效的。

由於 HPPT 協定沒有記憶性，因此登入後，若換一個網頁，新的網頁將無法辨識剛才是否已經登入了。利用Session, 我們可以保證在進行一個工作(會談)之間，我們的登入是有效的。

Session 的工作原理是：送出一個SID，，當同一瀏覽器下次進入時，攜回這個SID，依此伺服器才能夠辨識這是延續的request。

Session的用途是記錄用戶端的資訊，而且每個用戶端擁有各自的Session，下圖：

![image-20200319164700034](E:\wamp64\www\dbP\source\_posts\image-20200319164700034.png)

存取範例：

二個網頁的範例：　session1.php

```php
<HTML>
<HEAD>
</HEAD>	
<BODY>
    <h1> Hello </h1>
        <a href="session2.php">session2</a>
 <?php
      session_start();    //  使用 session
      if (!isset($_SESSION['Count']))
        $_SESSION['Count'] = 1;
      else
        $_SESSION['Count']++;
      $_SESSION['uname']="pcchen";
      echo "這是您在同一個瀏覽器第{$_SESSION['Count']}次載入本網頁！";
	  // session_destroy();
    ?>

  </BODY>
</HTML>
```

session2.php

```php
<HTML>
<HEAD>
</HEAD>	
<BODY>
 <?php
      session_start();    //  使用 session，每一個要用 session 的php網頁都要有
      if (!isset($_SESSION['Count']))
        $_SESSION['Count'] = 1;
      else
        $_SESSION['Count']++;

      echo $_SESSION['uname']."這是您在同一個瀏覽器第{$_SESSION['Count']}次載入本網頁！";
    ?>  
</BODY>
</HTML>
```

再來看第2個範例：同樣是二個網頁　page1.php 及 page2.php

```php
<?php
// page1.php
$s = session_id('s1');
if( $s == "")
  session_start();
echo 'Welcome to page #1';

$_SESSION['favcolor'] = 'green';
$_SESSION['animal']   = 'cat';
$_SESSION['time']     = time();

// Works if session cookie was accepted
echo '<br /><a href="page2.php">page 2</a>';

// Or maybe pass along the session id, if needed
echo '<br /><a href="page2.php?' . SID . '">page 2</a>';
//session_unset();      // 移除註解行，結束 session !
//session_destroy();
?>

<?php
// page2.php
session_start();   // 這一行必須有
echo 'Welcome to page #2<br />';

echo $_SESSION['favcolor']; // green
echo $_SESSION['animal'];   // cat
echo date('Y m d H:i:s', $_SESSION['time']);
echo session_id();
// You may want to use SID here, like we did in page1.php
echo '<br /><a href="page1.php">page 1</a>';
?>
```

登入處理範例：　本範例包含4個檔案　b01.html, b01.php, b02.php, b03.php

```html
<!DOCTYPE html>
<html>
  <head>
     <script>
	     function check(){
		     var x = document.forms['myForm']['username'].value;
		     var y = document.forms['myForm']['passwd'].value;
			 if( x == null || x == "" ){
			     alert(" Username cannot be empty!" );
				 return false;
			 }
			 if( y == null || y == "" ){
			     alert(" Password cannot be empty!" );
				 return false;
			 }
			 else
			    document.forms['myForm'].submit();
		 }
	 </script>
  </head>
  <body>
    <h2> 行動商場 登入頁面 </h2>
    <form name="myForm" method="post" action="b01.php" onsubmit="return check()">
	  帳號： <input name = "username" type="text" size="15" /> <br>
	  密碼： <input name = "passwd" type="password" size="15" />  <br>
      <input type="submit"> <br>
    </form>
  </body>
</html>
```

b01.php, b02.php

```php
<?php
      // 
      session_start();
	  
	  if( isset($_SESSION['log']) ){	      
		  echo "isset!";
		  header("location: b03.php");
		  exit();                   // 跟在 header() 之後，這是需要的
	  }
      $uname = $_POST['username'];
	  $pwd = $_POST['passwd'];
	  if( $uname != "kk" ){
	      header("Location: b01.html" );
		  exit();                   // 跟在 header() 之後，這是需要的
		  }
	  if( $pwd != "yy" ){
	      header("location: b01.html");
		  exit();                   // 跟在 header() 之後，這是需要的
	  }
	  $_SESSION['log'] = 'ok';
	  echo "OK";
	  header("location: b03.php");
	  exit();                   // 跟在 header() 之後，這是需要的
?>

// b02.php
<?php
      // 登出處理，將 Session 變數做 unset
      session_start();
      unset($_SESSION['log']);	  
	  header("location: b01.html");
	  exit();
?>
```

b03.php

```php
<HTML>
  <HEAD>
  </HEAD>
  <BODY>
    <h1> 行動商城 歡迎購物 </h1>
  <?php
      // 檢查是否登入，若否，踢回登入，若有，繼續！  可點選登出。
      session_start();
	  
	  if( !isset($_SESSION['log']) ){
	      echo "尚未登入";
		  header("location: b01.html");
		  exit();
	  }
  ?>
  <div><h3> 購物資訊 及表單</h3> 
  <form name="purchase" >
     <input type="checkbox" name="items" value="1">頻果</input><br/>
     <input type="checkbox" name="items" value="2">鳳梨</input><br/>
     <input type="checkbox" name="items" value="3">香蕉</input><br/>
     <input type="checkbox" name="items" value="4">橘子</input><br/>
  </form>
  </div>
  
  <a href="b02.php">登出</a>
  </BODY>
</HTML>
```

