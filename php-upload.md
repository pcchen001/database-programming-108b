---
title: 檔案上傳與下載
date: 2020-05-5 8:05:35
tags:
- php
- file upload
- file download
- file
categories:
- php
- Tools
---

## 檔案上傳

檔案上傳通常跟著 form 傳送。

有檔案的表單不可以使用 GET, 只能使用 POST 或是 PUT。

我們只介紹 POST 方法。

首先，我們可以看一下 php.ini 中，對檔案上傳的幾個設定：

建議下列設定，若不是的話請更改一下！

file_uploads = on
upload_tmp_dir = "c:\wamp64\tmp"
upload_max_filesize = 20M
post_max_size = 28M
max_input_time = 30

更改後，請重新啟動 Apache Server！



本單元介紹檔案的上傳與下載會用到下列二個 PHP 的指令：

```
iconv( A, B, String); // 將 String 的編碼由 A 轉換成 B, 例如：
iconv("UTF-8", "Big5", $_FILES["myfile"]["name"]);

move_uploaded_file( A, B ); // 將檔案 A 搬移到 B, A, B 均須完整檔名，例如
move_uploaded_file($_FILES["myfile"]["tmp_name"], $upload_file)

```

範例一 單一檔案上傳：(index_01.html)

​    準備一個 html 檔案， body 內放入

```html
<form method="post" action="upload_01.php" enctype="multipart/form-data">
  <input type="file" name="myfile" size="50"><br><br>
  <input type="submit" value="上傳">
  <input type="reset" value="重新設定">
</form>
```

送出時由 upload_01.php 處理，為了存放上傳的檔案，我們在目前的子目錄下建立一個子目錄 uploads 。

upload_01.php 

```php
<!doctype html>
<html>
  <head>
    <title>檔案上傳</title>
    <meta charset="utf-8">
  </head>
  <body>
    <p align="center">檔案上傳回應</p>
    <?php
      //指定檔案儲存目錄及檔名
      $upload_dir =  "./uploads/";
      $to = iconv("UTF-8", "Big5", $_FILES["myfile"]["name"]);
      $upload_file = $upload_dir.$to;
		
      //將上傳的檔案由暫存目錄移至指定之目錄
      if (move_uploaded_file($_FILES["myfile"]["tmp_name"], $upload_file))
      {
        echo "<strong>檔案上傳成功</strong><hr>";	
				
        //顯示檔案資訊		
        echo "檔案名稱：" . $_FILES["myfile"]["name"] . "<br>";	
        echo "暫存檔名：" . $_FILES["myfile"]["tmp_name"] . "<br>";
        echo "檔案大小：" . $_FILES["myfile"]["size"] . "<br>";
        echo "檔案種類：" . $_FILES["myfile"]["type"] . "<br>";						
        echo "<p><a href='JavaScript:history.back()'>繼續上傳</a></p>";
      }
      else
      {
        echo "檔案上傳失敗 (" . $_FILES["myfile"]["error"] . ")<br><br>";
        echo "<a href='javascript:history.back()'>重新上傳</a>";
      }
    ?>
  </body>
</html>
```



若要一次上傳多個檔案，可以在同一個 form 中增加多個 file 型態的 input，我們可以將這些 input 的 name 設成陣列，php 處理時就會能用陣列處理：

```html
    <form method="post" action="upload_02.php" enctype="multipart/form-data">
      <input type="file" name="myfile[]" size="50"><br>
      <input type="file" name="myfile[]" size="50"><br>
      <input type="file" name="myfile[]" size="50"><br>
      <input type="file" name="myfile[]" size="50"><br><br>
      <input type="submit" value="上傳">
      <input type="reset" value="重新設定">
    </form>
```

處理多個檔案的上傳，需要使用陣列：(upload_02.php)

```php
      //指定儲存檔案的目錄
      $upload_dir =  "./uploads/";
			
      for ($i = 0; $i < 4; $i++)
      {		
        //若檔案名稱不是空字串，表示上傳成功，將暫存檔案移至指定之目錄。
        if ($_FILES["myfile"]["name"][$i] != "")
        {
          //搬移檔案
          $upload_file = $upload_dir . iconv("UTF-8", "Big5", $_FILES["myfile"]["name"][$i]);
          move_uploaded_file($_FILES["myfile"]["tmp_name"][$i], $upload_file);
					
          //顯示檔案資訊		
          echo "檔案名稱：" . $_FILES["myfile"]["name"][$i] . "<br>";	
          echo "暫存檔名：" . $_FILES["myfile"]["tmp_name"][$i] . "<br>";
          echo "檔案大小：" . $_FILES["myfile"]["size"][$i] . "<br>";
          echo "檔案種類：" . $_FILES["myfile"]["type"][$i] . "<br>";						
          echo "<hr>";
        }
      } 
```

對於不同格式的檔案，我們可以找檔案格式相關的程式庫來協助處理。

例如影像檔案，照相時通常數位相機或手機會記錄照相時的設定，有時還會包含GPS定位資料。我們可以在上傳時檢視一下，有時可以幫忙後續的處理。下面的範例是使用 EXIF 程式庫，讀取影像檔案的 exit 資料。

index_03.html

upload_03.php

```php
    echo $_FILES["myfile"]["tmp_name"];
	  $exif = @exif_read_data($_FILES["myfile"]["tmp_name"]);
      if (!$exif)
      {
       	echo("這個檔案沒有 exif 標頭資訊");
      }
      else
      {
        foreach ($exif as $key => $value)
        {
          if(!is_array($value))
              echo "$key: $value<br>";
        }
		$t = $exif['MimeType'];
		if($t != 'image/jpeg' && $t != 'image/png' && $t != 'image/bmp' && $t != 'image/gif')
			echo "Not available image file!<br>";
      }

      //指定檔案儲存目錄及檔名
      $upload_dir =  "./uploads/";
      $upload_file = $upload_dir . $to = iconv("UTF-8", "Big5", $_FILES["myfile"]["name"]);

      //將上傳的檔案由暫存目錄移至指定之目錄
      if (move_uploaded_file($_FILES["myfile"]["tmp_name"], $upload_file))
      {
        echo "<strong>檔案上傳成功, POST 的檔案資訊</strong><hr>";

        //顯示檔案資訊
        echo "檔案名稱：" . $_FILES["myfile"]["name"] . "<br>";
        echo "暫存檔名：" . $_FILES["myfile"]["tmp_name"] . "<br>";
        echo "檔案大小：" . $_FILES["myfile"]["size"] . "<br>";
        echo "檔案種類：" . $_FILES["myfile"]["type"] . "<br>";

        echo "<p><a href='JavaScript:history.back()'>繼續上傳</a></p>";
      }
      else
      {
        echo "檔案上傳失敗 (" . $_FILES["myfile"]["error"] . ")<br><br>";
        echo "<a href='javascript:history.back()'>重新上傳</a>";
      }
```

詳細說明在課堂。  

```
E:\wamp64\tmp\phpDDFE.tmpFileName: phpDDFE.tmp
FileDateTime: 1588685888
FileSize: 1216790
FileType: 2
MimeType: image/jpeg
SectionsFound: ANY_TAG, IFD0, EXIF, MAKERNOTE
ImageDescription:
Make: SONY
Model: DSC-RX10M2
Orientation: 1
XResolution: 350/1
YResolution: 350/1
ResolutionUnit: 2
Software: DSC-RX10M2 v1.20
DateTime: 2018:04:14 13:08:05
Artist: pochi chen
YCbCrPositioning: 2
Copyright: CHEN
ExposureTime: 1/80
FNumber: 35/10
ExposureProgram: 3
ISOSpeedRatings: 1600
UndefinedTag:0x8830: 2
UndefinedTag:0x8832: 1600
ExifVersion: 0230
DateTimeOriginal: 2018:04:14 13:08:05
DateTimeDigitized: 2018:04:14 13:08:05
ComponentsConfiguration: 
CompressedBitsPerPixel: 4/1
BrightnessValue: 3974/2560
ExposureBiasValue: 0/10
MaxApertureValue: 760/256
MeteringMode: 5
LightSource: 0
Flash: 16
FocalLength: 2618/100
```

## 檔案下載：

download01.php

```php
<?php
$filename = $_GET['filename'];
if(!file_exists($filename)) {
    echo "file not exists!";
    exit();
}

// 讓大部分的瀏覽器都能適用
header("Pragma: public");
header("Expires: 0");   // 設定超時限制 0 表示無限制
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

// force download dialog 強制下載對話盒
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");

header("Content-Disposition: attachment; filename=".basename($filename).";");

header("Content-Transfer-Encoding: binary");
header("Content-Length: ".filesize($filename));

@readfile($filename);
exit(0);
?>
```

我們用 get 送來下載檔案檔名：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Download file</title>
</head>
<body>
    <form action="download01.php" method="get">
    <input type="text" name="filename">
    <input type="submit" value="下載">
    </form>
</body>
</html>
```

