---
title: PHP 在地檔案的存取
date: 2020-02-28 21:38:23
tags:
- php
- file
categories:
- 資料庫程式設計
---

## PHP 檔案的存取

PHP 在伺服器端執行，對於本機的資源的權限由執行 php 的使用者的權限來決定。(請留意，不是遠端呼叫 php 的使用者，是執行 apache 服務的那個使用者，通常就是本機的管理員)　通常我們會賦與很大的權限取存取本機的檔案。

PHP 檔案存取的目的是讓我們能夠以程式撰寫的方式來存取本機的檔案資料。

我們先來複習路徑概念：檔案系統在windows下，以磁碟機為根節點，可以視為一個樹狀結構，根節點是磁碟機代碼。例如： e:\ 。節點可以是檔案或是目錄(又稱為資料夾)。路徑就是將一個節點在這個樹狀結構中，從根節點到該節點所有的目錄名稱用反斜線\\ 接起來。例如：　e:\wamp64\www\dbp\myfirst.php ，檔案名稱是 myfirst.php, 所在的資料夾是 dbp ，dbp 上面是 www , www 上面是 wamp64。 wamp64 就在 e:\ 裡面。

為了方便，有時我們不寫完整的路徑，而只寫相對於目前位置的路徑，稱之為相對路徑。而從根節點一路寫下來的路徑稱為絕對路徑。

所謂的"目前位置"，在 console 模式下就是我們的工作目錄，而在 Apache Server 下，就是我們執行 php 時所在的資料夾。

由於工作的需要，有時我們需要絕對路徑，有時僅需要檔案名稱，因此需要及個指令來處理：

basename("路徑字串")　　取得檔案名稱

pathinfo("路徑字串")['dirname']  資料夾路徑

pathinfo("路徑字串")['basename']  檔案名稱

pathinfo("路徑字串")['extension']  檔案副檔名

realpath("路徑字串")　取得絕對路徑字串

在進一步介紹PHP檔案存取的指令之前，我們先介紹從網站執行PHP檔案時，對於系統可取得的資料以及相關知識。

請先執行 wamp ，將 apache server 跑起來！

在工作目錄下撰寫 myphps.php，內容如下：

```php
<?php
	  echo '<table>';
	  foreach( $_SERVER as $ind => $val )
	     echo '<tr><td>'.$ind . '</td><td>'. $val .'</td></tr>';
	  echo '</table>';
?>
```

接著，開瀏覽器：　http://localhost/U0533001/myphps.php 查看結果。

此處有一個變數 `$_SERVER` ，這是php 提供的關於 server 端系統的資訊集合。`$_SERVER`是一個associative 陣列，瀏覽器所見是　key-value 的集合。

我們留意下列幾個：

```
DOCUMENT_ROOT
PHP_SELF
...
```

我們可以試著用 basename(), pathinfo() 及　realpath() 來針對 `$_SERVER['PHP_SELF']` 應用看看。

接著，我們看幾個處理子目錄的指令：

```
mkdir("子目錄名稱");  建立子目錄
chdir("子目錄名稱");  更動工作目錄（也就是目前目錄)
rmdir("子目錄名稱");  移除子目錄
getcwd();           取得目前工作子目錄　current working directory
is_dir("子目錄名稱")　回傳所給子目錄名稱是否存在，若有同名檔案，也會回傳false。
file_exists("檔名或是子目錄名稱")　回傳檔案或是子目錄是否存在（檔案子目錄都算)  
    當我們要建立一個新的子目錄時，可以先檢查這個子目錄是否存在，若沒有才去新建。
    檔案也是一樣，以免建立新的檔案，將現有的檔案蓋掉。
chmod("子目錄名稱", mode)  設定子目錄的存取權限。 mode 通常用八進位表示，例如 0642。
    一般權限由 9 個位元表示，（擁有者，群組，一般）X(讀取、寫入、更改)。
dirname("檔案或子目錄名稱")　回傳該檔案或子目錄所在的子目錄名稱。
scandir("子目錄名稱")　　　回傳一個陣列，陣列包含該目錄內的子目錄及檔案名稱
```

接著，我們看處理檔案的指令：

```
is_file("檔案名稱")  回傳檔案是否存在，同名子目錄回傳 false。
copy("源檔案名稱","新檔案名稱")
unlink("檔案名稱")　　刪除檔案
rename("舊檔名", "新檔名")
fileatime("檔案名稱")　取得檔案上一次被存取的時間
filectime("檔案名稱")　取得檔案上一次被建立的時間
filemtime("檔案名稱")　取得檔案上一次被修改的時間
is_readable("檔案名稱")　回傳是否可讀取
is_writable("檔案名稱")　回傳是否可寫入
```

最後我們來看看如何讀取或寫入一個檔案：

```
fread($openedFile, n)　對一個已開啟的檔案，讀取指定的字數。　
fgets($openedFile)　將一個已開啟的檔案，一次讀取一行或是指定的字數。
    通常用 feof($openedFile) 判斷是否檔案已結束
    $openedFile = fopen("filename");
    while( !feof($openedFile)) {
        $line = nl2br( fgets($openedFile));
    }
    fclose($openedFile);
file_get_contents("檔案名稱")　　將檔案內容以一個字串傳回，這個比較好用！
　　檔案名稱也可以是url。
fwrite($resource, $stringToWrite) 將字串寫入到 $resource
fputs($resource, $stringToWrite) 　與 fwrite() 相同。
file_put_contents("檔案名稱", $stringtoWrite) 將字串寫入到
fopen("檔案名稱", mode)  mode 是一個字串 "ab" append binary. "r" read, "w" write。
```

### CSV

資料文字檔　.csv 的字串剖析：

```php
<?php
      $data = nl2br(file_get_contents("data.csv"));
	  echo $data;
	  $lines = explode('<br />', $data);
	  $i = 0;
	  echo '<table>';
	  foreach($lines as $v){
	      echo '<tr>';
          $d = explode(',', $v);
		  foreach($d as $item)
		     echo '<td>'.$item.'</td>';
		  echo '</tr>';
	  }
	  echo '</table>';
    ?>

```

data.csv

```
項次,商品名稱,價格,數量
1,【QUALY】雀兒便條夾,$250,81
2,【urban prefer】MEET+ 名片盒上下蓋組合 11色任選,$300,43
3,【 賽先生科學工廠】喝水鳥Drinking Bird,$350,91
4,【Propaganda】小精靈訂書機4色,$450,21
5,【Indulgence】TAiWAN 護照套-澄黃,$480,100
6,【urban prefer】PIN 無針釘書機 桃紅色,$480,69
7,【HeadphoneDog】錄音帶造型卡夾/名片盒/煙盒酷黑特別版,$549,47
8,【Indulgence】TAiWAN 摺疊旅行袋(中)-澄黃,$780,86
9,【 賽先生科學工廠】仿生蝴蝶罐Butterfly jar4色,$850,81
10,【Luckies】我的地圖刮刮樂 旅遊雜誌 紅色版,$1080,27
11,【Bomber & Co】傘繩求生手環及鑰匙圈,$1450,32
12,【ZENLET】行動錢包＋RFID屏蔽卡,$1500,0
13,【賽先生科學工廠】復古圓柱風暴球Storm glass,$1500,79
14,【 Flightline】City Classic 城市系列 潮流簡約大容量多功能包 _7色,$3250,96
15,【25TOGO】灰姑娘腕錶優雅藍,$3980,95
```

再多一個範例： confirmeds.csv

```
Province/State,Country/Region,Lat,Long,3/8/20,3/9/20,3/10/20,3/11/20,3/12/20,3/13/20,3/14/20,3/15/20
,Thailand,15,101,50,50,53,59,70,75,82,114
,Japan,36,138,502,511,581,639,639,701,773,839
,Singapore,1.2833,103.8333,150,150,160,178,178,200,212,226
,Nepal,28.1667,84.25,1,1,1,1,1,1,1,1
,Malaysia,2.5,112.5,99,117,129,149,149,197,238,428
British Columbia,Canada,49.2827,-123.1207,27,32,32,39,46,64,64,73
New South Wales,Australia,-33.8688,151.2093,38,48,55,65,65,92,112,134
Victoria,Australia,-37.8136,144.9631,11,15,18,21,21,36,49,57
Queensland,Australia,-28.0167,153.4,15,15,18,20,20,35,46,61
,Cambodia,11.55,104.9167,2,2,2,3,3,5,7,7
,Sri Lanka,7,81,1,1,1,2,2,6,10,18
,Germany,51,9,1040,1176,1457,1908,2078,3675,4585,5795
,Finland,64,26,23,30,40,59,59,155,225,244
,United Arab Emirates,24,54,45,45,74,74,85,85,85,98
,Philippines,13,122,10,20,33,49,52,64,111,140
,India,21,78,39,43,56,62,73,82,102,113
,Italy,43,12,7375,9172,10149,12462,12462,17660,21157,24747
,Sweden,63,16,203,248,355,500,599,814,961,1022
,Spain,40,-4,673,1073,1695,2277,2277,5232,6391,7798
South Australia,Australia,-34.9285,138.6007,7,7,7,9,9,16,19,20
,Belgium,50.8333,4,200,239,267,314,314,559,689,886
,Egypt,26,30,49,55,59,60,67,80,109,110
From Diamond Princess,Australia,35.4437,139.638,0,0,0,0,0,0,0,0
```

cov19.php 找出最近一天確診數最多的國家/區域

```php
    <?php
	  $data = nl2br(file_get_contents("confirmeds.csv"));
	  echo $data."<br />";
	  $lines = explode('<br />', $data);
	  $i = 0;
	  foreach($lines as $v){
		  $d = explode(',', $v);
		  $j = 0;
		  foreach($d as $item){
			$values[$i][$j]=$item;
			$j++;
		  }
	      $i++;
	  }
	  $row = $i; $col = $j;
	  $max = 0; $imax = 0;
	  for($i = 1; $i < $row; $i++){
		  if( $max < $values[$i][$col-1]){
			  $max = $values[$i][$col-1];
			  $imax = $i;
		  }
	  }
      echo $values[$imax][0]." ".$values[$imax][1]." $max";
    ?>
```

資料來源： [Johns Hopkins CSSE](https://github.com/CSSEGISandData/COVID-19)

另，[確診每日](https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv) [死亡每日](https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv) [康復每日](https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv)  .csv 檔案連結 [連結出問題時，可由上面連結進入在找到檔案連結]

#### JSON

針對JSON文字檔， json 的資料剖析：

JSON (JavaScript Simple Object Notation) 是 JavaScript 的一個簡單物件表示，因為好用已經成為網路用來表示資料的最普及的一個表示方式。格式如 下：

```
{ key1 : value1,

  key2 : value2, 

  … }
```

也就是json 是1 個或多個 key-value 所組成的集合，用大括號{} 括起來。其中，value 的部分可以是一個基本型別資料，也可以是一個陣列或是另一個 JSON。例如：

```json
{
 "one" : [1,2,4,7,8],
  "2": 123,
  "three" : { "1" : "first", "2": "second", "3": 300, "four": 444},
  "4": ["one", "second"]
}
```

使用 json_decode($jsonString, true) 以及 json_encode($phparray), 範例

```php
$json = '{"1": 45,"2": 780.9,"m":{"x": 300, "y": 400},"n": 88}';
$j = json_decode( $json, true );
foreach( $j as $ji => $jv ) echo $ji,"=>",$jv,'<br>';
$ej = json_encode( $j );
echo $ej;
```

練習：

1. 準備一檔文字檔，利用上述指令讀入，然後加上 `<br>`　之後，顯示出來。
2. 準備一個 .csv 檔，讀入、剖析、加上加總後，用 table 的方式顯示。
3. 練習寫出一個僅含 ascii 字元的文字檔案以及一個含有中文的文字檔。



> 重要補充：（BIG-5 or UTF-8)
>
> 由於中文編碼有時會有BIG-5 跟 UTF-8 出現，必要時必須要進行編碼轉換。mbstring 模組提供了一個可以偵測文字編碼的指令
>
> $encode = mb_detect_encoding( $content, array("ASCII", BIG-5", "UTF-8"));
>
> 取得編碼型態之後，可以將字串轉換為 UTF-8 ，方便我們處理：
>
> $content = iconv($encode, 'UTF-8', $content);

另外，在檔名的處理時，我們要特別小心，因為windows 的中文編碼預設都是BIG-5 因此中文檔名的處理要記得轉成 BIG-5 ，否則會是亂碼檔名。