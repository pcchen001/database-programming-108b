---
title: 應用 - 討論區
date: 2020-04-28 11:20:15
tags:
- database
- php
categories:
- Application
---

這個範例我們想來看看如何對一個留言回應。

畫面上除了我們在訪客留言板所見的留言外，每一個留言都多了一個回應留言的選紐。點選回應留言，出現一個只有留言及其所有回應的網頁。

先來修改訪客留言的網頁，在每一筆留言之後增加一個 form, 送出這筆留言的留言編號給 reply.php。

reply.php 中先 select 留言編號，接著到回應的資料表中，select 所有對此留言的回應。

建立二個資料表：(請各位按照下面 sql, 建立二個資料表)

```sql
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(10) NOT NULL DEFAULT '',
  `subject` tinytext NOT NULL,
  `content` mediumtext NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;
```

以及

```sql
CREATE TABLE IF NOT EXISTS `reply_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(10) NOT NULL DEFAULT '',
  `subject` tinytext NOT NULL,
  `content` mediumtext NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reply_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;
```

撰寫瀏覽留言並且能夠填寫留言的程式：(index0.php

```php
      require_once("dbtools.inc.php");

      //指定每頁顯示幾筆記錄
      $records_per_page = 5;

      //取得要顯示第幾頁的記錄
      if (isset($_GET["page"]))
        $page = $_GET["page"];
      else
        $page = 1;

      //建立資料連接
      $link = create_connection();

      //執行SQL查詢
      $sql = "SELECT id, author, subject, date FROM message ORDER BY date DESC";
      $result = execute_sql($link, "u0533001", $sql);

      //取得記錄數
      $total_records = mysqli_num_rows($result);

      //計算總頁數
      $total_pages = ceil($total_records / $records_per_page);

      //計算本頁第一筆記錄的序號
      $started_record = $records_per_page * ($page - 1);

      //將記錄指標移至本頁第一筆記錄的序號
      mysqli_data_seek($result, $started_record);

      echo "<table width='800' align='center' cellspacing='3'>";

      //使用 $bg 陣列來儲存表格背景色彩
      $bg[0] = "#D9D9FF";
      $bg[1] = "#FFCAEE";
      $bg[2] = "#FFFFCC";
      $bg[3] = "#B9EEB9";
      $bg[4] = "#B9E9FF";

      //顯示記錄
      $j = 1;
      while ($row = mysqli_fetch_assoc($result) and $j <= $records_per_page)
      {
        echo "<tr>";
        echo "<td width='120' align='center'><img src='" . mt_rand(0, 9) . ".gif'></td>";
        echo "<td bgcolor='" . $bg[$j - 1] . "'>作者：" . $row["author"] . "<br>";
        echo "主題：" . $row["subject"] . "<br>";
        echo "時間：" . $row["date"] . "<hr>";
        echo "<a href='show_news.php?id=";
        echo $row["id"] . "'>閱讀與加入討論</a></td></tr>";
        $j++;
      }
      echo "</table>" ;

      //產生導覽列
      echo "<p align='center'>";

      if ($page > 1)
        echo "<a href='index0.php?page=". ($page - 1) . "'>上一頁</a> ";

      for ($i = 1; $i <= $total_pages; $i++)
      {
        if ($i == $page)
          echo "$i ";
        else
          echo "<a href='index0.php?page=$i'>$i</a> ";
      }

      if ($page < $total_pages)
        echo "<a href='index0.php?page=". ($page + 1) . "'>下一頁</a> ";

      echo "</p>";

      //釋放記憶體空間
      mysqli_free_result($result);
      mysqli_close($link);
```

同一個檔案下面，應該有一填寫留言的 form ：(index0.php)

```html
<form name="myForm" method="post" action="post.php">
      <table border="0" width="800" align="center" cellspacing="0">
        <tr bgcolor="#0084CA" align="center">
          <td colspan="2"><font color="white">請在此輸入新的討論</font></td>
        </tr>
        <tr bgcolor="#D9F2FF">
          <td width="15%">作者</td>
          <td width="85%"><input name="author" type="text" size="50"></td>
        </tr>
        <tr bgcolor="#84D7FF">
          <td width="15%">主題</td>
          <td width="85%"><input name="subject" type="text" size="50"></td>
        </tr>
        <tr bgcolor="#D9F2FF">
          <td width="15%">內容</td>
          <td width="85%"><textarea name="content" cols="50" rows="5"></textarea></td>
        </tr>
        <tr>
          <td colspan="2" height="40" align="center">
            <input type="submit" value="張貼討論">　
            <input type="reset" value="重新輸入">
          </td>
        </tr>
      </table>
    </form>
```

這個表單要送去給 post.php 處理：

```php
  require_once("dbtools.inc.php");
  date_default_timezone_set("asia/Taipei");
  $author = $_POST["author"];
  $subject = $_POST["subject"];
  $content = $_POST["content"];
  $current_time = date("Y-m-d H:i:s");

  //建立資料連接
  $link = create_connection();

  //執行SQL查詢
  $sql = "INSERT INTO message(author, subject, content, date)
          VALUES ('$author', '$subject', '$content', '$current_time')";
  $result = execute_sql($link, "u0533001", $sql);

  //關閉資料連接
  mysqli_close($link);

  //將網頁重新導向
  header("location:index0.php");
  exit();
```

我們可以打開 index0.php 看看頁面。目前是空的，試著留言一筆。

留言後 post.php 處理之後回到 index0.php, 因此會看到新的留言。留意到我們多了一個超連結

```php+HTML
        echo "<a href='show_news.php?id=";
        echo $row["id"] . "'>閱讀與加入討論</a></td></tr>";
```

這是一個 get 的送出需求，$_GET['id'] 獲得這個留言的 id 。

show_news.php 抓取本留言以及所有針對此留言的所有回應：

```php
      require_once("dbtools.inc.php");

      //取得要顯示之記錄
      $id = $_GET["id"];

      //建立資料連接
      $link = create_connection();

      //執行SQL查詢
      $sql = "SELECT * FROM message WHERE id = $id";
      $result = execute_sql($link, "u0533001", $sql);

      echo "<table width='800' align='center' cellpadding='3'>";
      echo "<tr height='40'><td colspan='2' align='center'
            bgcolor='#663333'><font color='white'>
            <b>討論主題</b></font></td></tr>";

      //顯示原討論主題的內容
      while ($row = mysqli_fetch_assoc($result))
      {
        echo "<tr>";
        echo "<td bgcolor='#CCFFCC'>主題：" . $row["subject"] . "　";
        echo "作者：" . $row["author"] . "　";
        echo "時間：" . $row["date"] . "</td></tr>";
        echo "<tr height='40'><td bgcolor='CCFFFF'>";
        echo $row["content"] . "</td></tr>";
      }

      echo "</table>";

      //釋放 $result 佔用的記憶體空間
      mysqli_free_result($result);

      //執行 SQL 命令
      $sql = "SELECT * FROM reply_message WHERE reply_id = $id";
      $result = execute_sql($link, "u0533001", $sql);

      if (mysqli_num_rows($result) <> 0)
      {
        echo "<hr>";
        echo "<table width='800' align='center' cellpadding='3'>";
        echo "<tr height='40'><td colspan='2' align='center'
              bgcolor='#99CCFF'><font color='#FF3366'>
              <b>回覆主題</b></font></td></tr>";

        //顯示回覆主題的內容
        while ($row = mysqli_fetch_assoc($result))
        {
          echo "<tr>";
          echo "<td bgcolor='#FFFF99'>主題：" . $row["subject"] . "　";
          echo "作者：" . $row["author"] . "　";
          echo "時間：" . $row["date"] . "</td></tr>";
          echo "<tr><td bgcolor='CCFFFF'>";
          echo $row["content"] . "</td></tr>";
        }

        echo "</table>";
      }

      //釋放記憶體空間
      mysqli_free_result($result);
      mysqli_close($link);
```

另外，在同一頁面也要有一個表單，送出回應的內容：(show_news.php)

```html
    <form name="myForm" method="post" action="post_reply.php">
      <input type="hidden" name="reply_id" value="<?php echo $id ?>">
      <table border="0" width="800" align="center" cellspacing="0">
        <tr bgcolor="#0084CA" align="center">
          <td colspan="2"><font color="white">請在此輸入您的回覆</font></td>
        </tr>
        <tr bgcolor="#D9F2FF">
          <td width="15%">作者</td>
          <td width="85%"><input name="author" type="text" size="50"></td>
        </tr>
        <tr bgcolor="#84D7FF">
          <td width="15%">主題</td>
          <td width="85%"><input name="subject" type="text" size="50"></td>
        </tr>
        <tr bgcolor="#D9F2FF">
          <td width="15%">內容</td>
          <td width="85%"><textarea name="content" cols="50" rows="5"></textarea></td>
        </tr>
        <tr>
          <td colspan="2" height="40" align="center">
            <input type="submit" value="回覆討論">
            <input type="reset" value="重新輸入">
          </td>
        </tr>
      </table>
    </form>
```

同樣，我們也需要 insert into 這個回應留言，處理的程式會是 post_reply.php

```php
  require_once("dbtools.inc.php");

  $author = $_POST["author"];
  $subject = $_POST["subject"];
  $content = $_POST["content"];
  $current_time = date("Y-m-d H:i:s");
  $reply_id = $_POST["reply_id"];

  //建立資料連接
  $link = create_connection();

  //執行SQL查詢
  $sql = "INSERT INTO reply_message(author, subject, content, date, reply_id)
          VALUES ('$author', '$subject', '$content', '$current_time', '$reply_id')";
  $result = execute_sql($link, "u0533001", $sql);

  //關閉資料連接
  mysqli_close($link);

  //將網頁重新導向
  header("location:show_news.php?id=" . $reply_id);
  exit();
```

現在，我們可以好好看一下整個應用。美中不足的是在回應區沒有可以回到首頁的連結，麻煩你加上去！

貼文及回應在 FB 中十分常見，我們其實還蠻希望能在同一頁看到留言及回應，要不要試試看？

