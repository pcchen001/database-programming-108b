<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>資料庫程式設計 課程說明</title>

</head>

<body>
  <h1> PHP Hello World! </h1>


  <h3>by PHP</h3>

  <?php
  date_default_timezone_set('UTC');
  $t=time();
  echo($t . "<br />");
  echo(date("D F d Y",$t));
  ?>
  <h3>by JavaScript</h3>
  <script>
  var d = Date();
  document.write("<p>"+Date() + "</p>");
  </script>
  <?php for($c = 0; $c < 100; $c+=5){
    echo("C: ".$c."--> F: ".($c*9/5+32)."<br>" );
  } ?>
</body>
</html>
