<!doctype html>
<html>
  <head>
    <title>投票結果</title>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
  </head>
  <body>
    <p align='center'><img src='title_3.jpg'></p>
      <?php
        require_once("dbtools.inc.php");
        $link = create_connection();
        $sql = "SELECT * FROM candidate";
        $result = execute_sql($link, "u0533001", $sql);

        $total_records = mysqli_num_rows($result);

        for ($j = 0; $j < $total_records; $j++)
        {
          $row = mysqli_fetch_assoc($result);
          $percent[$j] = round($row["score"] / $total_score, 4) * 100;
          $labels[$j] = $row["name"];
          $scores[$j] = $row["score"];
        }
        function randColor($a){
          $r = random_int(0,255);
          $g = random_int(0,255);
          $b = random_int(0,255);
          $s = "rgba($r,$g,$b,$a)";
          return $s;
        }
        ?>
        <canvas id="myChart" width="400" height="200" aria-label="投票結果統計圖" role="img"></canvas>
        <script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',    // polarArea, bar, line, radar, Pie
    data: {
        labels: [
              <?php 
                for($i=0;$i<$total_records-1;$i++) 
                  echo "'{$labels[$i]}',";
                echo "'{$labels[$total_records-1]}'";
              ?>],
        datasets: [{
            label: '# of Votes',
            data: [
              <?php 
                for($i=0; $i<$total_records-1; $i++) 
                  echo "{$scores[$i]},";
                echo "{$scores[$total_records-1]}";
              ?> ],
            backgroundColor: [ 
                <?php 
                  for($i=0; $i < $total_records-1; $i++) 
                    echo "'".randColor(0.2)."',";
                  echo "'".randColor(0.2)."'"; 
                ?> 
            ],
            borderColor: [
                <?php 
                  for($i=0; $i < $total_records-1; $i++)
                    echo "'".randColor(1)."',";
                    echo "'".randColor(1)."'"; 
                ?> 
            ],
            borderWidth: 1
        }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          }
      }
    });
</script>
<?php
        //釋放資源及關閉資料連接
        mysqli_free_result($result);
        mysqli_close($link);
      ?>
    <p align='center'><a href='index0.php'>回首頁</a></p>
  </body>
</html>
