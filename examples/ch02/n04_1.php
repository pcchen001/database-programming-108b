<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>範例練習 三</title>
<style type="text/css" media="all">
#head {
	width: 800px;
	height: 142px;
	background: url(sample03_r1_c1.gif);

}

#left {
	float: left;
    width: 196px;
	height: 558px;
	background: url(sample03_r2_c1.gif);
}

#center{
   float: left;
   width: 491px;
   height: 558px;
   background: url(sample03_r2_c2.gif) no-repeat;
}

#right {
	float: left;
	width: 113px;
	height: 558px;
	 background: url(sample03_r2_c3.gif);
}

#footnote{
	clear: both;
	height: 20px;
	text-align: center;
	font-family: Verdana, Geneva, sans-serif;
	background: #CCC;
}
</style>
</head>

<body>
<div id="head" class="head">



</div>
<div id="left" class="select">


</div>

<div id="center" class="content">
<?php
  echo '<p>&nbsp;</p>';
  echo '<p>&nbsp;</p>';
  $dec = 100;
  $bin = decbin( $dec );
  echo $bin.'<br />';
  echo decoct(100).'<br />';
  echo dechex(0x33ff).'<br />';

  echo '目前時間: <br />';
  echo getdate().'<br />';
  $date = getdate();
  echo  "year: ".$date["year"].'<br />';
  echo  "month: ".$date["month"].'<br />';
  echo  "mday: ".$date["mday"].'<br />';
  echo  "yday: ".$date["yday"].'<br />';
  echo  "wday: ".$date["wday"].'<br />';
  echo  "weekday: ".$date["weekday"].'<br />';
  echo  "hours: ".$date["hours"].'<br />';
  echo  "minutes: ".$date["minutes"].'<br />';
  echo  "seconds: ".$date["seconds"].'<br />';
  echo '某年月日: ';
  $n = mktime(13, 20, 30, 11, 30, 2009);
  echo $n.'<br />';

  $file = fopen("sample03.txt", "r");
  $str = fread($file, filesize("sample03.txt"));
  echo $str.'<br />';
  echo strpos( $str, "foot");
  ?>

</div>

<div id="right" class="subnav">
<?php
echo 'Hello world';
echo '<ol><li>HTML</li>';
echo '<li>PHP></li>';
echo '<li>JavaScript</li></ol>';
$arr['hx'] = 1000;
echo $arr['hx'].'<br />';
$arr[0]=300;
echo $arr[0].'<br \>';
foreach ( $arr as $i=>$v )
  echo $v.' '.$i.'<br>';
?>
</div>
<div id="footnote" class="foot">
<?php
$str = "資料庫程式設計 copyleft cc 2018 Po-chi Chen";
echo $str."<br />";
?>
</div>
</body>
</html>
