<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    <title>第五周 練習範例 w05</title>
<style type="text/css" media="all">
#head {
	width: 800px;
	height: 142px;
	background: url(sample03_r1_c1.gif);

}

#left {
	float: left;
    width: 196px;
	height: 558px;
	background: url(sample03_r2_c1.gif);
}

#lred {
	margin-top: 400px;
	top: 300px;
	left: 0px;
}

#center{
   float: left;
   width: 491px;
   height: 558px;
}

#cup{
   width: 491px;
   height: 55px;
   background: url(sample03_r2_c2_r1_c1.gif);
}

#cdown {
	width: 491px;
	height: 323px;
	background: url(sample03_r2_c2_r2_c1.gif)
}

#right {
	float: left;
	width: 113px;
	height: 558px;
	background: url(sample03_r2_c3.gif);
}

#footnote{
	clear: both;
	height: 20px;
	text-align: center;
	font-family: Verdana, Geneva, sans-serif;
	background: #CCC;
}
</style>
</head>

<body>
<div id="head" class="head">



</div>
<div id="left" class="select">

<img id="lred" src="littleRed.png" width="196"  />

</div>

<div id="center" class="content">
<div id="cup" class="centent">
&nbsp;
</div>
<div id="cdown" class="centent">
<?php
/* w04
  $dec = 100;
  $bin = decbin( $dec );
  echo $bin.'<br />';
  echo decoct(100).'<br />';
  echo dechex(0x33ff).'<br />';

  echo '目前時間: <br />';
  echo getdate().'<br />';
  $date = getdate();
  echo  "year: ".$date["year"].'<br />';
  echo  "month: ".$date["month"].'<br />';
  echo  "mday: ".$date["mday"].'<br />';
  echo  "yday: ".$date["yday"].'<br />';
  echo  "wday: ".$date["wday"].'<br />';
  echo  "weekday: ".$date["weekday"].'<br />';
  echo  "hours: ".$date["hours"].'<br />';
  echo  "minutes: ".$date["minutes"].'<br />';
  echo  "seconds: ".$date["seconds"].'<br />';
  echo '某年月日: ';
  $n = mktime(13, 20, 30, 11, 30, 2009);
  echo $n.'<br />';

  $file = fopen("sample03.txt", "r");
  $str = fread($file, filesize("sample03.txt"));
  echo $str.'<br />';
  echo strpos( $str, "foot");
  */

  // w05  陣列

  // 一維陣列

  $arr[0] = 36;           // 鍵值可以是 整數
  $arr1["first"] = 23.7;  // 鍵值也可以是 字串
  $arr[0.4] = 457;        // 鍵值也可以是 浮點數, 也可以是大部分的型別

  $arrx = array("one", "two", "three");  // 自動給鍵值 0, 1, 2
  echo $arrx[0].'<br />';

  $arry = array(2=>"first", 4=>"second");
  echo $arry[2].'<br />';

  $arrz = array("first"=>"RED", "second"=>"ORANGE", "third"=>"黃", "4"=>"綠色");
  echo $arrz["third"].'<br />';

  $arrz["new"] = "普魯士藍";

  foreach ( $arrz as $value )
    echo $value.'<br />';

  foreach ( $arrz as $ind=>$value )
    echo $ind.' : => : '.$value.'<br />';

  echo current($arrz).'<br>';
  echo pos($arrz).'<br>';


  // 二維陣列
  /*
  $twodarr[2][4]="紅色的花";
  $twodarr[5]["red"]="紅";
  echo $twodarr[2][4].'<br />';

  $twoarr = array( array(4, 5, 7, 9), array(4, 6, 88, 9),
                   array(6, 2, 4, 5), array(1, 2, 3, 4));

  foreach( $twoarr as $oned)
  {
    foreach ( $oned as $value)
	  echo $value.' ';
	echo '<br />';
  }

  for( $i = 0; $i < 4; $i++)
    for( $j = 0; $j < 4; $j++)
	  echo $twoarr[$i][$j].'  ';
*/

  // arr4.php 凾式返回陣列
/*      function ExpValue($X)
      {
        $Result[0] = $X;
        $Result[1] = $X * $X;
        $Result[2] = $X * $X * $X;
        return $Result;
      }
      $ReturnArray = ExpValue(10);
      foreach($ReturnArray as $Value)
        echo $Value.'<BR>';
*/

  // arr5.php 傳入凾式陣列
/*
      //宣告包含一個陣列參數的函式
      function ArrAdd($Array1)
      {
        foreach($Array1 as $Value)
          $Result += $Value;
        return $Result;
      }

      $A = array(1, 2, 3, 4, 5);	//將陣列A的值設定為1、2、3、4、5
      echo ArrAdd($A);       		  //將陣列A當做參數傳遞給函式並將結果顯示
*/
  // arr6.php  聯集
/*
      $a = Array(0 => '台北', 1 => '苗栗' );
      $b = Array(0 => '巴黎', 1 => '羅馬', 2 => '東京' );
      $c = $a + $b;
      foreach($c as $Value)
        echo $Value.'<BR>';
*/
  // arr7.php  array_walk()
/*
      function add_prefix(&$value, $key, $prefix)
      {
        $value = "$prefix: $value";
      }

      function print_result($value, $key)
      {
        echo "$key. $value<BR>";
      }

      $colors = array('red', 'green', 'blue', 'yellow');
      echo '加上前置文字之前<BR>';
      array_walk($colors, 'print_result');

      array_walk($colors, 'add_prefix', 'color');
      echo '加上前置文字之後<BR>';
      array_walk($colors, 'print_result');
*/
  // arr8.php usort() 排序
  /*
      function compare($a, $b)
      {
        return strcmp($a['color'], $b['color']);
      }

      $colors[0]['color'] = 'red';
      $colors[1]['color'] = 'green';
      $colors[2]['color'] = 'blue';

      usort($colors, 'compare');

      while (list($key, $value) = each($colors))
        echo "\$colors[$key]: ".$value['color'].'<BR>';
*/
/* 陣列凾式 示範 */
/*
$arr = array();
if( is_array( $arr ) )
  echo 'This is an Array <br />';

$arr[1] = "yes";
$arr[2] = "no";
echo 'Array Size: '.sizeof( $arr ).'<br />';

if( in_array( "yes", $arr ) )
  echo 'yes is in the Array $arr<br />';

$arr[3] = "yes&no";

foreach ($arr as $v )
  echo $v.' ';
echo '<br />';

unset( $arr[2] );
foreach ($arr as $v )
  echo $v.' ';
echo '<br />';
$arr[2] = "new no";
$arr[4] = "new One";
reset( $arr ); // 游標移到第一個
echo current( $arr ).'<br />';
next( $arr);    // 游標後移一個
echo current( $arr ).'<br />';
prev( $arr );    // 游標前移一個
echo current( $arr ).'<br />';
end( $arr );     // 游標移到最後一個
echo current( $arr ).'<br />';

foreach ($arr as $k=>$v )
  echo $k.'=>'.$v.'&nbsp;';
echo '<br />';

list($arg1, $arg2, $arg3) = $arr;   // 依照 0, 1, 2, 3, 順序獨入陣列內容
echo 'list: 0'.$arg1.'1'. $arg2.'2: '. $arg3.'<br />';

$narr["new"] = "yes new";
list($arg1, $arg2, $arg3) = $narr;   // 依照 0, 1, 2, 3, 順序獨入陣列內容
echo 'list: 0'.$arg1.'1'. $arg2.'2: '. $arg3.'<br />';

reset( $arr );
while( list($key, $v) = each( $arr ) )  // each  返回目前游標之key 與 value, 然後後移一個
  echo $key.'xxx'.$v.'&nbsp;';
echo '<br />';

$karr = array(0, 2, 4, 6, 8);
$varr = array('new', 'ntwo', 'nuu', 'im', 'new');
$combine = array_combine( $karr, $varr );
foreach ($combine as $k=>$v )
  echo $k.'=>'.$v.'&nbsp;';
echo '<br />';

// array_diff 及 array_fill
$arr1 = array(1, 2, 3, 4, 5, 6, 7);
$arr2 = array(2, 4, 6, 8);
$arr3 = array(3, 6, 9, 10);
$darr = array_diff( $arr1, $arr2, $arr3 );  // $arr1 沒有在 $arr2 及 $arr3 出現的 值
foreach ($darr as $k=>$v )
  echo $k.'=>'.$v.'&nbsp;';
echo '<br />';


// array_keys 及 array_values
$karr = array_keys( $combine );
foreach ($karr as $k=>$v )
  echo $k.'=>'.$v.'&nbsp;';
echo '<br />';

$karr = array_keys( $combine, 'new' );
foreach ($karr as $k=>$v )
  echo $k.'=>'.$v.'&nbsp;';
echo '<br />';

$varr = array_values( $combine );
foreach ($varr as $k=>$v )
  echo $k.'=>'.$v.'&nbsp;';
echo '<br />';

*/
  ?>
</div>
</div>

<div id="right" class="subnav">
<br />
<?php
echo 'subNavigator';
echo '<ol><li>多媒體程式設計</li>';
echo '<li>網頁程式設計</li>';
echo '<li>數位內容應用</li></ol>';
?>
</div>
<div id="footnote" class="foot">
<?php
$str = "資料庫程式設計 copyleft cc 2018 Po-chi Chen";
echo $str."<br />";
?>
</div>
</body>
</html>
