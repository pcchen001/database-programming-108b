<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>範例說明 六 Cookie 及 Header 函式</title>
<style type="text/css" media="all">
#head {
	width: 900px;
	height: 120px; 
	background: url(ducks.jpg);
	
}

#left {
	float: left;
    width: 196px;
	height: 558px;
	background: url();
}

#lred {
	margin-top: 400px;
	top: 300px;
	left: 0px;
}

#center{
   float: left;
   width: 491px;
   height: 558px;  
}

#cup{
   width: 491px;
   height: 55px;
   background: url();
}

#cdown {
	width: 491px;
	height: 323px;
	background: url()
}

#right {
	float: left;
	width: 113px; 
	height: 558px;
	background: url();
}

#footnote{
	clear: both;
	height: 20px;
	text-align: center;
	font-family: Verdana, Geneva, sans-serif;
	background: #CCC;
}
</style>
</head>

<body>
<?php
ob_start();
?>
<div id="head" class="head">

</div>
<div id="left" class="select">
<?php
echo 'Cookie 示範<br /><br />';
if( !isset( $_COOKIE['UserName1006'] )) // 若 Cookie 還沒有設定
{					 
  if( $_GET['UserName'] == NULL )   // 若不是提送資料過來 產生表單
  {
    echo '<form name="myForm" method="get" action="w06.php">';
    echo '姓&nbsp;&nbsp;名：<INPUT TYPE="TEXT" NAME="UserName" SIZE="40"><BR>
      E-Mail：<INPUT TYPE="TEXT" NAME="UserMail" SIZE="40" VALUE="username@mailserver"><BR><BR>
 
      <INPUT TYPE="SUBMIT" VALUE="提交">
      <INPUT TYPE="RESET" VALUE="重新輸入"><br />';
    echo '</form>';
  }
  else   // 取得姓名及 e-mail 資料 同時設定 COOKIE 
  {
	  $uname = $_GET['UserName'];
	  $umail = $_GET['UserMail'];
	  echo $uname.'已經登錄<br /> e-mail: '.$umail.'<br />';
	  setcookie("UserName1006", $uname, time()+60*60*24);
  }
}
else  // 如果以經設定了 COOKIE 則出現使用者姓名
	echo $_COOKIE['UserName1006'].'&nbsp;<br />歡迎光臨 <br />';
	
ob_end_flush();
?>
<img id="lred" src="littleRed.png" width="196"  />

</div>

<div id="center" class="content">
<div id="cup" class="centent">
&nbsp;

</div>
<div id="cdown" class="centent">
<!--  這一段用來說明 header 指令，請搭配 redirect.php 學習
-->
<h3>網頁重新導向</h3>
<h4>header(Location:"http://xxx.xxx.xxx.xxx/xxx.html")</h4>
    <FORM METHOD="POST" ACTION="w06-redirect.php">
      <SELECT NAME="mySelect" SIZE="1"> 
        <OPTION VALUE="http://godel.im.nuu.edu.tw">博智老師</OPTION> 
        <OPTION VALUE="http://www.im.nuu.edu.tw">聯大資管</OPTION> 
        <OPTION VALUE="http://www.nuu.edu.tw">聯合大學</OPTION>
        <OPTION VALUE="http://en.wikipedia.org/wiki/List_of_HTTP_headers">HTTP Headers 表列</OPTION>
      </SELECT>
      &nbsp;<INPUT TYPE="SUBMIT" VALUE="GO!">
    </FORM>

<br />

</div>
</div>

<div id="right" class="subnav">
<br />
<?php
echo '開心點菜站';
echo '<ol><li>蔬果</li>';
echo '<li>穀物</li>';
echo '<li>蛋奶</li>';
echo '<li>畜肉</li></ol>';
?>
</div>
<div id="footnote" class="foot">
<?php
$str = "資料庫程式設計 copyleft cc 2009 Po-chi Chen";
echo $str."<br />";
?>
</div>
</body>
</html>
