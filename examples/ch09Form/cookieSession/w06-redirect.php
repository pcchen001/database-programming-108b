<?php
  // header(""); 有關 HTTP Header 的語法，請參考 http://en.wikipedia.org/wiki/List_of_HTTP_headers
   
  // 使用 header() 函式傳送自訂的 HTTP Header  
  // 必須放在任何輸出之前，否則會導致 header() 函式執行錯誤。

  // echo 'before header';
  $URL = $_POST['mySelect'];  // 取得所選擇的網址
  header("Location: $URL");   // 重新導向
?>