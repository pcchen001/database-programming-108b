<?php
require 'phpmail.tools.php';

// Instantiation and passing `true` enables exceptions

try {
    $mail = pmail();
    //Recipients
    $sname = "=?utf-8?B?".base64_encode('陳大一')."?=";
    $mail->setFrom('yourMail@gmail.com', $sname);
    $mail->addAddress('hw.pcchen@gmail.com', $sname);     // Add a recipient
    // $mail->addAddress('ellen@example.com');               // Name is optional
    $mail->addReplyTo('hw.pcchen@gmail.com', $sname);
    $mail->addCC('yourcc@gmail.com');
    // $mail->addBCC('bcc@example.com');

    // Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Here is the subject';
    $mail->Body    = '有中文This is the HTML message body <b>in bold!</b>';
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}