<?php
  require '../phpmail.tools.php';

  //取得表單資料
  $from_name = "=?utf-8?B?" . base64_encode($_POST["from_name"]) . "?=";
  $from_email = $_POST["from_email"];
  $to_name = "=?utf-8?B?" . base64_encode($_POST["to_name"]) . "?=";
  $to_email = $_POST["to_email"];
  // $format = $_POST["format"];
  $subject = "=?utf-8?B?" . base64_encode($_POST["subject"]) . "?=";
  $message = $_POST["message"];

  //設定郵件標頭資訊
  // $headers  = "MIME-Version: 1.0\r\n";
  // $headers .= "Content-type: text/$format; charset=utf-8\r\n";
  // $headers .= "To: $to_name<$to_email>\r\n";
  // $headers .= "From: $from_name<$from_email>\r\n";

  //傳送郵件
  $mail = pmail();

  $mail->setFrom($from_email, $from_name);
  //Set an alternative reply-to address
  $mail->addReplyTo('pcchen@gm.nuu.edu.tw', 'First Last');
  //Set who the message is to be sent to
  $mail->addAddress($to_email, $to_name);
  // $mail->addAddress('id2', 'John Doe');
  //Set the subject line
  $mail->Subject = $subject;
  //Read an HTML message body from an external file, convert referenced images to embedded,
  //convert HTML into a basic plain-text alternative body
  $mail->Body=$message;
  // $mail->msgHTML(file_get_contents('sendmail_01.html'), __DIR__);
  // $mail->msgHTML("<h1>Congratulations</h1>
  // <img src='title.jpg'>".$message);
  //Replace the plain text body with one created manually
  // $mail->AltBody = 'This is a plain-text message body,<h1>Congratulations</h1>';
  //Attach an image file
  // $mail->addAttachment('images/phpmailer_mini.png');
  //send the message, check for errors
  // $mail->Body = $message;
  if (!$mail->send()) {
      echo "Mailer Error: " . $mail->ErrorInfo;
  } else {
      echo "Message sent!";
      //Section 2: IMAP
      //Uncomment these to save your message in the 'Sent Mail' folder.
      #if (save_mail($mail)) {
      #    echo "Message saved!";
      #}
  }
?>
