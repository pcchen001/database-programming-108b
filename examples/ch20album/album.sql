﻿-- phpMyAdmin SQL Dump
-- version 3.1.3.1
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 建立日期: Apr 09, 2009, 11:47 AM
-- 伺服器版本: 5.1.31
-- PHP 版本: 5.2.9-1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫: `u9933000`
--
use u9933000;
-- --------------------------------------------------------

--
-- 資料表格式： `album`
--

CREATE TABLE IF NOT EXISTS `album` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `owner` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 列出以下資料庫的數據： `album`
--

INSERT INTO `album` (`id`, `name`, `owner`) VALUES
(4, '愛情鳥撒野篇', 'U0533000'),
(5, '愛情鳥洗澡篇', 'U0533000'),
(6, '小綿羊成熟篇', 'U0533000'),
(7, '小綿羊天鵝篇', 'U0533000'),
(9, '伊斯坦堡', 'U0533000'),
(10, '吳哥', 'U0533000'),
(11, 'SunMoon', 'U0533000'),
(12, 'Moscow', 'U0533000');

-- --------------------------------------------------------

--
-- 資料表格式： `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `filename` varchar(64) NOT NULL,
  `comment` varchar(512) DEFAULT NULL,
  `album_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `album_id` (`album_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=124 ;

--
-- 列出以下資料庫的數據： `photo`
--

INSERT INTO `photo` (`id`, `name`, `filename`, `comment`, `album_id`) VALUES
(58, 'IMG_0324.jpg', '49db3d094bd08.jpg', NULL, 4),
(61, 'IMG_0318.jpg', '49db3e217abd1.jpg', NULL, 4),
(62, 'IMG_0321.jpg', '49db3e21c6dc3.jpg', NULL, 4),
(63, 'IMG_0322.jpg', '49db3e21e94d0.jpg', NULL, 4),
(64, 'IMG_0315.jpg', '49db3e2218185.jpg', NULL, 4),
(65, 'IMG_0316.jpg', '49db3e2644648.jpg', NULL, 4),
(66, 'IMG_0333.jpg', '49db40054a0e9.jpg', NULL, 5),
(67, 'IMG_0331.jpg', '49db40056b188.jpg', NULL, 5),
(68, 'IMG_0338.jpg', '49db40058b684.jpg', NULL, 5),
(69, 'IMG_0343.jpg', '49db4005b517d.jpg', NULL, 5),
(70, 'IMG_0344.jpg', '49db400ec7021.jpg', NULL, 5),
(71, 'IMG_0345.jpg', '49db400ef0661.jpg', NULL, 5),
(72, 'IMG_0346.jpg', '49db400f22eea.jpg', NULL, 5),
(73, 'IMG_0467.jpg', '49db419f58ff9.jpg', NULL, 6),
(74, 'IMG_0471.jpg', '49db419f7b0c6.jpg', NULL, 6),
(75, 'IMG_0465.jpg', '49db419f9c858.jpg', NULL, 6),
(76, 'IMG_0462.jpg', '49db419fbb709.jpg', NULL, 6),
(77, 'IMG_0459.jpg', '49db41a90fb50.jpg', NULL, 6),
(78, 'IMG_0453.jpg', '49db41a931f9b.jpg', NULL, 6),
(79, 'IMG_0430.jpg', '49db41a9514c2.jpg', NULL, 6),
(80, 'IMG_0192.jpg', '49db42a261ef0.jpg', NULL, 7),
(81, 'IMG_0193.jpg', '49db42a2870e1.jpg', NULL, 7),
(82, 'IMG_0191.jpg', '49db42a2db83e.jpg', '裝無辜', 7),
(83, 'IMG_0190.jpg', '49db42a306864.jpg', '人家長大變漂漂了', 7),
(84, 'IMG_0189.jpg', '49db42ac60140.jpg', NULL, 7),
(85, 'IMG_0188.jpg', '49db42ac80fcb.jpg', NULL, 7),
(86, 'DSC00052.JPG', '49db446185238.jpg', NULL, 8),
(87, 'DSC00051.JPG', '49db4461c4702.jpg', NULL, 8),
(88, 'DSC00050.JPG', '49db44620c0df.jpg', NULL, 8),
(89, 'DSC00049.JPG', '49db4462403da.jpg', NULL, 8),
(90, 'DSC00048.JPG', '49db446c41da9.jpg', NULL, 8),
(91, 'DSC00047.JPG', '49db446c800ce.jpg', NULL, 8),
(92, 'DSC00046.JPG', '49db446cbbc44.jpg', NULL, 8),
(93, 'DSC00045.JPG', '49db446d04f63.jpg', NULL, 8),
(94, 'DSC00044.JPG', '49db447809049.jpg', NULL, 8),
(95, 'DSC00043.JPG', '49db447847984.jpg', NULL, 8),
(96, 'DSC00042.JPG', '49db447885d04.jpg', NULL, 8),
(97, 'DSC00041.JPG', '49db4478c353e.jpg', NULL, 8),
(98, 'DSC00040.JPG', '49db448a07b30.jpg', NULL, 8),
(99, 'DSC00039.JPG', '49db448a44ef4.jpg', NULL, 8),
(100, 'DSC00038.JPG', '49db448a7d7f1.jpg', NULL, 8),
(101, 'DSC00037.JPG', '49db448ab4c4e.jpg', NULL, 8),
(102, 'DSC00036.JPG', '49db44945cf40.jpg', NULL, 8),
(103, 'DSC00035.JPG', '49db44949c31c.jpg', NULL, 8),
(104, 'DSC00034.JPG', '49db4494d405d.jpg', NULL, 8),
(105, 'DSC00033.JPG', '49db44951617c.jpg', NULL, 8),
(106, 'DSC00032.JPG', '49db44a30fce1.jpg', NULL, 8),
(107, 'DSC00031.JPG', '49db44a345a77.jpg', NULL, 8),
(108, 'DSC00030.JPG', '49db44a3823c6.jpg', NULL, 8),
(109, 'DSC00029.JPG', '49db44a3bee7b.jpg', NULL, 8),
(110, 'DSC00028.JPG', '49db44acf0c18.jpg', NULL, 8),
(111, 'DSC00027.JPG', '49db44ad3b115.jpg', NULL, 8),
(112, 'DSC00026.JPG', '49db44ad6fd8d.jpg', NULL, 8),
(125, 'IMG_0684.JPG', '5376bc34d18d1.jpg', NULL, 9),
(126, 'IMG_2999.JPG', '5376be07157b4.jpg', NULL, 9),
(127, 'IMG_2480.JPG', '5376be085bcfe.jpg', NULL, 9),
(129, 'IMG_2219.JPG', '5376be8e1a4bf.jpg', NULL, 9),
(130, 'DSC02098.JPG', '5376bf2f9fa7f.jpg', NULL, 10),
(131, 'DSC02105.JPG', '5376bf3188fa4.jpg', NULL, 10),
(132, 'DSC02013.JPG', '5376bf333b9bc.jpg', NULL, 10),
(133, 'IMG_3326.JPG', '5376c0b5ee594.jpg', NULL, 11),
(134, 'IMG_3413.JPG', '5376c0b641034.jpg', NULL, 11),
(135, 'IMG_3430.JPG', '5376c0b677759.jpg', NULL, 11),
(136, '001.JPG', '5376c16107d3c.jpg', NULL, 12),
(137, '002.JPG', '5376c1620815d.jpg', NULL, 12),
(138, '003.JPG', '5376c162f23ac.jpg', NULL, 12),
(139, 'DSCN9749.JPG', '5376c1bceaf0e.jpg', NULL, 12);

-- --------------------------------------------------------

--
-- 資料表格式： `auser`
--

CREATE TABLE IF NOT EXISTS `auser` (
  `account` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 列出以下資料庫的數據： `user`
--

INSERT INTO `auser` (`account`, `password`, `name`) VALUES
('U0533999', 'u0533999', '二甲'),
('U0533000', 'u0533000', '二乙'),
('user','user','相簿主人');
