<?php
/*
 * use this script to enable your visitors to download
 * your files.
 */

$fileserver_path = dirname(__FILE__);	// change this to the directory your files reside
$whoami	 = basename(__FILE__);	// you are free to rename this file

if( isset($_GET['file'])){
  $req_file 	 = basename($_GET['file']);
}
else {
	echo "Usage: $whoami?file=&lt;file_to_download&gt;";
	exit;
}

/* no web spamming 避免不恰當檔名*/
if (!preg_match("/^[a-zA-Z0-9._-]+$/", $req_file, $matches)) {
	echo "Error: 不恰當檔名 ...";
	exit;
}

/* download any file, but not this one 不下載本身檔案 */
if ($req_file == $whoami) {
	echo "Error: 不下載本身檔案 ...";
	exit;
}
/* check if file exists 確認檔案存在*/


if (!file_exists("$fileserver_path/$req_file")) {
	echo "File <strong>$req_file</strong> doesn't exist.";
	exit;
}

if (empty($_GET['send_file'])) {
    // 延遲一些
	header("Refresh: 5; url=$whoami?file=$req_file&send_file=yes");
}
else {
	header('Content-Description: File Transfer');
	header('Content-Type: application/force-download');
	header('Content-Length: ' . filesize("$fileserver_path/$req_file"));
	header('Content-Disposition: attachment; filename=' . $req_file);
	readfile("$fileserver_path/$req_file");
	exit;
}
?>

<!-- Change the HTML page below for your convenient -->
<html>
<head>
<title>Simple File Server and Downloader Demo Page</title>
</head>
<body>
<h2>下載中 <?=$req_file?>...</h2>
<p>Your download should begin shortly. 若還沒開始下載，可點擊<a href="<?=$req_file?>">連結</a>.</p>
</body>
</html>
