<!doctype html>
<html>
  <head>
    <title>檔案上傳</title>
    <meta charset="utf-8">
  </head>
  <body>
    <p align="center"><img src="title.jpg"></p>
    <?php
      //指定檔案儲存目錄及檔名
      $upload_dir =  "./uploads/";
      $upload_file = $upload_dir . $to = iconv("UTF-8", "Big5", $_FILES["myfile"]["name"]);

      //將上傳的檔案由暫存目錄移至指定之目錄
      if (move_uploaded_file($_FILES["myfile"]["tmp_name"], $upload_file))
      {
        echo "<strong>檔案上傳成功</strong><hr>";

        //顯示檔案資訊
        echo "檔案名稱：" . $_FILES["myfile"]["name"] . "<br>";
        echo "暫存檔名：" . $_FILES["myfile"]["tmp_name"] . "<br>";
        echo "檔案大小：" . $_FILES["myfile"]["size"] . "<br>";
        echo "檔案種類：" . $_FILES["myfile"]["type"] . "<br>";
        echo "<p><a href='JavaScript:history.back()'>繼續上傳</a></p>";
      }
      else
      {
        echo "檔案上傳失敗 (" . $_FILES["myfile"]["error"] . ")<br><br>";
        echo "<a href='javascript:history.back()'>重新上傳</a>";
      }

$imageType = @exif_imagetype($upload_file);
if (!$imageType)
  echo "這個檔案不是圖片";
elseif ($imageType == IMAGETYPE_JPEG)
  echo "這張圖片的格式為 JPG";
elseif ($imageType == IMAGETYPE_GIF)
  echo "這張圖片的格式為 GIF";
elseif ($imageType == IMAGETYPE_BMPG)
  echo "這張圖片的格式為 BMP";
elseif ($imageType == IMAGETYPE_PNG)
  echo "這張圖片的格式為 PNG";

  $size = getimagesize($upload_file);
  echo "圖片寬度：$size[0]<BR>";
  echo "圖片高度：$size[1]<BR>";
  echo "圖片格式：$size[2]<BR>";
  echo "圖片長寬字串：$size[3]";

  $exif = exif_read_data($upload_file);
        if (!$exif)
        {
         	echo("這個檔案沒有 exif 標頭資訊");
        }
        else
        {
          foreach ($exif as $key => $value)
          {
            echo "$key: $value<br>";
          }
        }

    ?>
  </body>
</html>
