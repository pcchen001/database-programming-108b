<?php
// downloading a file
$filename = $_GET['filename'];
if(!file_exists($filename)) {
    echo "file not exists!";
    exit();
}
// $filename = iconv("BIG5", "UTF-8", $filename);
// 讓大部分的瀏覽器都能適用
header("Pragma: public");
header("Expires: 0");   // 設定超時限制 0 表示無限制
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
// browser must download file from server instead of cache

// force download dialog 強制下載對話盒
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");

// use the Content-Disposition header to supply a recommended filename and
// force the browser to display the save dialog. 存檔對話盒
header("Content-Disposition: attachment; filename=".basename($filename).";");

/*
The Content-transfer-encoding header should be binary, since the file will be read
directly from the disk and the raw bytes passed to the downloading computer.
The Content-length 送出檔案大小，利用 filesize()指令取得後送出。.
*/
header("Content-Transfer-Encoding: binary");
header("Content-Length: ".filesize($filename));

@readfile($filename);
exit(0);
?>
