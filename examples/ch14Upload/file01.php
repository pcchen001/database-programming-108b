<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title> 檔案系統 - </title>
</head>
<body>
<h1>檔案系統 與 PHP 指令</h1>
<h2>檔案系統介紹</h2>
<h3>UNIX Linux</h3>
<h3>Macintosh OS X </h3>
<h3>Windows</h3>
<p></p>
<?php
echo "目前的工作目錄：".getcwd()."<br />";  // 目前工作目錄(資料夾)

$path="..\\ch14Upload\\file01.php";
echo '內部路徑名稱：'.$path.'<br />';
echo "絕對路徑名稱：".realpath($path)."<br />";

echo "使用斜線符號：<br />";
$path="../ch14Upload/file01.php";        // 斜線或反斜線都可以
echo '內部路徑名稱：'.$path.'<br />';
echo realpath($path)."<br />";   // 絕對路徑

echo '介紹 $_SERVER[] <br />';
$path=$_SERVER['PHP_SELF'];  // 這一個檔案的路徑
echo $path."<br />";
$path = basename($path);
echo realpath($path)."<br />";   // 絕對路徑

// $_SERVER 訊息
foreach( $_SERVER as $index => $value)
  echo $index."&nbsp;==>&nbsp;".$value."<br />";


// 建立資料夾
$folder_name = ".\\pictures";
if (!file_exists($folder_name))
  mkdir($folder_name, NULL, TRUE);
else
  echo "指定的資料夾已經存在";

// 讀取目錄
if( $handle = @opendir(".\\") ){
        while (($file = readdir($handle)) != FALSE)
            echo $file."<br>";
        closedir($handle);
}

// fopen
// echo "===== 使用 fread()讀檔 ===== <br />";
// 讀取一定字數內容， filesize("filename") 全部！
// $handle = fopen("myPoem.txt", "rb");
// if( $handle )
// {
// 	$str = fread($handle, filesize("myPoem.txt"));
//     echo $str;
//   fclose($handle);
// }
// else
//   echo "檔案開啟失敗<br />";

// echo "===== 使用 fgets()讀檔 ===== <br />";
// 一次讀一行
// $handle = fopen("poetry.txt", "rb");
// while (!feof($handle))
//       {
//         $line = nl2br(fgets($handle));
//         echo $line;
//       }
// fclose($handle);

// echo "===== 使用 fwrite()寫檔 ===== <br />";
// 寫檔案
// $handle=fopen("myPoem.txt","wb");
// if( $handle )
// {
// 	$content="竊取睡眠者\n\n";
// 	$content .= "自樹梢溜下來的賊啊\n";
// 	$content .= "從甜美的笑容旁\n";
// 	$content .= "偷走了兒童的睡眠\n";
//     fwrite($handle, $content);          // 使用fputs($handle, $content); 也可以
// 	fclose($handle);
// }
// else
//   echo "檔案開啟失敗<br />";

// 讀全部檔案
// echo nl2br(file_get_contents("myPoem.txt"));

// 寫全部檔案 fiel_put_contents(filename, content)
// 
// $content="竊取睡眠者\n\n";
// $content .= "自樹梢溜下來的賊啊\n";
// $content .= "從甜美的笑容旁\n";
// $content .= "偷走了兒童的睡眠\n";
// $content .="但是\n";
// $content .="輕巧的夕陽卻又正氣凜然的將竊眠者抓回來\n";
// file_put_contents("myPoem2.txt", $content);
// echo nl2br(file_get_contents("myPoem2.txt"));

?>


</body>
</html>
