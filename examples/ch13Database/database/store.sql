-- phpMyAdmin SQL Dump
-- version 3.1.2
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 建立日期: Feb 23, 2009, 12:25 PM
-- 伺服器版本: 5.1.31
-- PHP 版本: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫: `store`
--
CREATE DATABASE `store` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `store`;

-- --------------------------------------------------------

--
-- 資料表格式： `product_list`
--

CREATE TABLE IF NOT EXISTS `product_list` (
  `book_no` varchar(20) NOT NULL DEFAULT '',
  `book_name` varchar(30) NOT NULL DEFAULT '',
  `price` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`book_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 列出以下資料庫的數據： `product_list`
--

INSERT INTO `product_list` (`book_no`, `book_name`, `price`) VALUES
('EE0018', '最新計算機概論', 560),
('EE0069', '電腦網路概論與實務', 580),
('EL0063', 'ASP.NET 3.5 網頁程式設計', 580),
('EN0010', '網路概論', 580),
('P766', 'PHP 5 & MySQL 程式設計', 580),
('P816', 'Visual Basic 2008 程式設計', 560),
('P818', 'Visual C# .2008 程式設計', 580);
