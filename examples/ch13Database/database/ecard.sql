-- phpMyAdmin SQL Dump
-- version 3.1.2
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 建立日期: Feb 15, 2009, 04:41 AM
-- 伺服器版本: 5.1.31
-- PHP 版本: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫: `ecard`
--
CREATE DATABASE `ecard` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `ecard`;

-- --------------------------------------------------------

--
-- 資料表格式： `card_message`
--

CREATE TABLE IF NOT EXISTS `card_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_id` varchar(30) NOT NULL DEFAULT '',
  `subject` tinytext NOT NULL,
  `from_name` varchar(30) NOT NULL DEFAULT '',
  `from_email` varchar(50) NOT NULL DEFAULT '',
  `to_name` varchar(30) NOT NULL DEFAULT '',
  `to_email` varchar(50) NOT NULL DEFAULT '',
  `music` varchar(30) NOT NULL DEFAULT '',
  `style` varchar(20) NOT NULL DEFAULT '',
  `size` varchar(10) NOT NULL DEFAULT '',
  `color` varchar(20) NOT NULL DEFAULT '',
  `message` mediumtext NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- 列出以下資料庫的數據： `card_message`
--

