-- phpMyAdmin SQL Dump
-- version 3.1.2
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 建立日期: Feb 23, 2009, 12:31 PM
-- 伺服器版本: 5.1.31
-- PHP 版本: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫: `vote`
--
CREATE DATABASE `vote` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `vote`;

-- --------------------------------------------------------

--
-- 資料表格式： `candidate`
--

CREATE TABLE IF NOT EXISTS `candidate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `introduction` text NOT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 列出以下資料庫的數據： `candidate`
--

INSERT INTO `candidate` (`id`, `name`, `introduction`, `score`) VALUES
(1, '催智友', '「天國的階梯」的女主角\r\n', 8051),
(2, '楊恭如', '香港人氣偶象', 7597),
(3, '深田恭子', '日本超人氣的女演員', 6895),
(4, '蔡依琳', '過時的少男殺手', 4857),
(5, '許慧欣', '新一代的少男殺手', 10875);

-- --------------------------------------------------------

--
-- 資料表格式： `id_number`
--

CREATE TABLE IF NOT EXISTS `id_number` (
  `id` varchar(10) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 列出以下資料庫的數據： `id_number`
--

