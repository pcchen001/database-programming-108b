-- phpMyAdmin SQL Dump
-- version 3.1.2
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 建立日期: Feb 08, 2009, 02:49 PM
-- 伺服器版本: 5.1.31
-- PHP 版本: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫: `product`
--
-- CREATE DATABASE `product` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `U0533000`;

-- --------------------------------------------------------

--
-- 資料表格式： `price`
--

CREATE TABLE IF NOT EXISTS `price` (
  `no` int(10) unsigned NOT NULL DEFAULT '0',
  `category` varchar(20) NOT NULL DEFAULT '',
  `brand` varchar(20) NOT NULL DEFAULT '',
  `specification` varchar(100) NOT NULL DEFAULT '',
  `price` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `url` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 列出以下資料庫的數據： `price`
--

INSERT INTO `price` (`no`, `category`, `brand`, `specification`, `price`, `date`, `url`) VALUES
(301, '主機板', '華碩', 'P5Q Premium', 8500, '2009-02-01', 'tw.asus.com'),
(302, '主機板', '微星', 'P45 Platinum', 6900, '2009-02-02', 'www.msi.com.tw'),
(303, '主機板', '技嘉', 'EX48-DQ6 ', 9400, '2009-02-05', 'www.gigabyte.tw'),
(304, '主機板', '微星', 'P43 Neo', 2700, '2009-02-06', 'www.msi.com.tw'),
(305, '主機板', '華碩', 'M3N72-D', 4800, '2009-02-07', 'tw.asus.com'),
(306, '主機板', '微星', 'K9A2 Platinum', 6600, '2009-02-08', 'www.msi.com.tw'),
(307, '主機板', '技嘉', 'GigaByte GA-8KNXP', 7150, '2009-02-09', 'www.gigabyte.com.tw'),
(308, '主機板', '微星', 'X48C Platinum', 8750, '2009-02-04', 'www.msi.com.tw'),
(309, '主機板', '華碩', 'P5K-SE', 3050, '2009-02-03', 'tw.asus.com'),
(310, '主機板', '技嘉', 'EP45-DQ6', 9600, '2009-02-02', 'www.gigabyte.tw'),
(311, 'CPU', 'Intel', 'Core i7-965 3.2G', 34500, '2009-02-08', 'www.intel.com.tw'),
(312, 'CPU', 'Intel', 'Core2 Duo E2180 2.0G', 2000, '2009-02-02', 'www.intel.com.tw'),
(313, 'CPU', 'Intel', 'Core2 Duo E8400 3G', 5450, '2009-02-10', 'www.intel.com.tw'),
(314, 'CPU', 'AMD', 'AM2 Athlon64x2 5200', 1950, '2009-02-11', 'www.amdtaiwan.com.tw'),
(315, 'CPU', 'AMD', 'AM2 Phenom II X4 940', 8300, '2009-02-02', 'www.amdtaiwan.com.tw'),
(316, '記憶體', '創見', 'Transcend 512MB DDR-400', 3100, '2004-08-10', 'www.transcend.com.tw'),
(317, '記憶體', '勝創', 'Kingmax 512MB DDR-466', 3250, '2004-08-10', 'www.kingmax.com.tw'),
(318, '記憶體', '金士頓', 'Kingston 512MB DDR-400', 2950, '2004-08-15', 'www.kingston.com'),
(319, '記憶體', '創見', 'Transcend 512MB DDR-333', 3230, '2004-08-15', 'www.transcend.com.tw'),
(320, '記憶體', '勝創', 'Kingmax 512MB DDR-433', 2950, '2004-08-15', 'www.kingmax.com.tw'),
(321, '顯示卡', '華碩', 'A9800XT', 20275, '2004-08-01', 'www.asus.com.tw'),
(322, '顯示卡', '麗台', 'Quadro 4 NVS400/ 64M', 19445, '2004-08-01', 'www.leadtek.com.tw'),
(323, '顯示卡', '麗台', 'Leadtek WinFast A350 Ultra TDH', 17645, '2004-08-01', 'www.leadtek.com.tw'),
(324, '顯示卡', '華碩', 'ASUS AGP-V9520 Magic', 2030, '2004-08-01', 'www.asus.com.tw'),
(325, '硬碟', 'HITACHI', '500G/7200 轉/SATAII 300/16MB', 1700, '2009-02-13', 'www.hitachi.com.tw'),
(326, '硬碟', 'Seagate', '750G/7200 轉/SATAII 300/32MB', 3050, '2009-02-12', 'www.seagate.com'),
(327, '硬碟', 'Western Digital', '500G/7200 轉/SATAII 300/16MB', 1750, '2009-02-12', 'www.wdc.com'),
(328, '硬碟', 'Western Digital', '750G/7200 轉/SATAII 300/16MB', 2750, '2009-02-11', 'www.wdc.com'),
(329, '硬碟', 'Seagate', '1TB/7200 轉/SATAII 300/32MB', 3650, '2009-02-10', 'www.seagate.com'),
(330, '螢幕', 'ViewSonic', 'VG1921WM', 13300, '2009-02-17', 'www.viewsonic.com.tw'),
(331, '螢幕', 'EIZO', 'FlexScan SX3031', 92500, '2009-02-15', 'www.eizo.com.tw'),
(332, '螢幕', 'ViewSonic', 'VA1913wm', 3950, '2009-02-16', 'www.viewsonic.com.tw'),
(333, '螢幕', 'EIZO', 'FlexScan SX2761W', 57700, '2009-02-18', 'www.eizo.com.tw'),
(334, '印表機', '惠普', 'Photosmart C8180', 11100, '2009-02-12', 'www.hp.com.tw'),
(335, '印表機', 'Canon', 'IP4680', 3800, '2009-02-11', 'www.abico.com.tw'),
(336, '印表機', 'EPSON', 'C1100SE', 11500, '2009-02-10', 'www.epson.com.tw'),
(337, '掃描器', 'HP', 'Scanjet G3110', 3800, '2009-02-02', 'www.hp.com.tw'),
(338, '掃描器', 'Microtek', 'ScanMaker S430', 3900, '2009-02-01', 'www.microtek.com.tw'),
(339, '掃描器', 'EPSON', 'V500 PHOTO', 9300, '2009-02-03', 'www.epson.com.tw');
