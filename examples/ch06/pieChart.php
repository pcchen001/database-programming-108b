<?php 
  $im = imagecreatetruecolor(300, 300);
  $background = imagecolorallocate($im, 255, 255, 204);
//  $red = imagecolorallocate($im, 255, 0, 0);
//  $green = imagecolorallocate($im, 0, 255, 0);
  $black = imagecolorallocate($im, 0, 0, 0);

  $pdata = array(200, 300, 100);
  $pcolor[0] = imagecolorallocate($im, 255, 0, 0);
  $pcolor[1] = imagecolorallocate($im, 0, 255, 0);
  $pcolor[2] = imagecolorallocate($im, 0, 0, 255);
  //填滿背景色彩
  imagefill($im, 0, 0, $background);
   
   $ratio[0] = $pdata[0] / 600;
   $ratio[1] = $pdata[1] / 600;
   $ratio[2] = $pdata[2] / 600;
   $start = 50;
   $area = 360 * $ratio[0];
   $end = $start+$area;
  //繪製弧線
  imagefilledarc($im, 150, 150, 200, 200, $start, $end, $pcolor[0], IMG_ARC_PIE);
   $start += $area;
   $area = 360 * $ratio[1];
   $end = $start + $area;
  imagefilledarc($im, 150, 150, 200, 200, $start, $end,  $pcolor[1], IMG_ARC_PIE);
   $start += $area;
   $area = 360 * $ratio[2];
   $end = $start + $area;
  imagefilledarc($im, 150, 150, 200, 200, $start, $end, $pcolor[2], IMG_ARC_PIE);
    
   
    
  //繪製文字
  $imInfo = "投票結果： (1) " . $pdata[0] ." (2) " . $pdata[1] . " (3) ". $pdata[2];  
  imagettftext($im, 10, 0, 20, 290, $black, "simhei.ttf", $imInfo);  

  //輸出圖片
  header("Content-type: image/png");
  imagepng($im);
   
  //釋放影像佔用的記憶體
  imagedestroy($im);
?>