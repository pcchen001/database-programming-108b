<?php 
  $im = imagecreatetruecolor(500, 300);
  
  
  $background = imagecolorallocate($im, 255, 255, 204);
  
  $red = imagecolorallocate($im, 255, 0, 0);
  $green = imagecolorallocate($im, 0, 255, 0);
  $greenx = imagecolorallocate($im, 200, 255, 200);
  $black = imagecolorallocate($im, 0, 0, 0);
  
  //填滿背景色彩
  imagefill($im, 100, 100, $background);
  imagefill($im, 200, 200, $greenx);
   
     //繪製橢圓形
  imageellipse($im, 430, 30, 50, 50, $red);
  imageellipse($im, 430, 100, 40, 60, $green); 
  
  imageline($im, 0, 0, 100, 100, $black);
  imagestrinup($im, 5, 2, 395, "Hello world! This is my first image text.", $black);

  $data = array(300, 200, 700, 50, 125);
  for( $i = 0; $i < 5; $i ++){
     $h[$i] = $data[$i]*2.0/5.0; 
	 imagefilledrectangle($im, 45+90*$i, 280-$h[$i]+10, 90+90*$i, 290, $red);
  }
   
  //輸出圖片
  header("Content-type: image/png");
  imagepng($im);
   
  //釋放影像佔用的記憶體
  //imagedestroy($im);
?>