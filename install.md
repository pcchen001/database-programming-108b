---
title:  軟體需求與安裝
date: 2020-02-21 20:06:03
tags:
- 安裝
categories:
- 資料庫程式設計
---

#　軟體需求與安裝

網站系統至少需要一個網站伺服器才能提供網路需求的服務；網站後台程式的開發最常用的是 c# 以及php，其中PHP 是開放的且支援的社群完整；最後大部分的服務可能都需要一個資料庫。

課程使用的分別是 Apache, PHP 以及 MariaDB。同學可以自行個別下載安裝最新或較穩定版本，為了讓大家方便，許多團體將這些打包成一個套件，最有名的有 appserv, xmpserver 以及 wamp。

我們使用 wamp, 但不反對同學做其他選擇。

google wamp 應該可以找到官網，下載最新的套件下來，點擊之後就開始進行安裝。

安裝過程大部分都使用預設，也就是下一步。只有二個要選擇：瀏覽器是否要用 ie, 若習慣 chrome 或 firefox 可以點擊　是　，找到chrome 或是 firefox 的執行檔。我們建議使用 chrome。編輯器使否要用 notepad 也就是筆記本軟體，若習慣 atom 或是 vscode 則點擊　是，找到安裝編輯器的執行檔。(atom 或是 vscode 可能都是安裝在 user/文件 下)。

注意：若是安裝過程有問題找不到 vcruntime140.dll 或是 vctool11.dll 之類的問題，通常是因為 visual studio C 的支援檔沒有，可以到 microsoft 官網去下載安裝，程式都很小。請參考網頁 http://shaurong.blogspot.com/2019/02/wampserver-3.html 說明。不必每一個都下載，通常 2019 及 2012 就夠了，但我不確定。

安裝沒有問題，桌面會有一個 Wampserver64 的圖示，點擊執行就會執行　apache 及 mariadb 二個 service ，右下的相同較小圖示會從紅色到橘色最後是綠色。若不是綠色就是執行有問題。

服務起來之後，開一個瀏覽器，輸入 localhost 就會出現 wamp manager 的網頁。

### apache 設定

安裝完成後，這個網站只能在本機執行，資料庫也只能使用匿名帳號，而資料庫的管理員的密碼預設是沒有的。若要讓外部機器可以存取，必須留意下列幾件事：

1. 防火牆要打開，可以開給 apache 或是開 80 埠。

2. 找到 C:\wamp64\bin\apache\apache2.4.41\conf\extra\httpd-vhost.conf 修改

   Allowoverride none

   Require all granted

   httpd-vhost.conf 是用來設定 virtual host 的，這是 wamp 採取的策略，讓 www\ 變成網站的根節點，方便我們打網址。我們若需要更多的虛擬網站時，在這個組態檔中設定。詳細就不在這裡說了。

3. 找到 alias\phpmyadmin.conf 修改　二個地方

   ```
   Alias /phpmyadmin "c:/wamp64/apps/phpmyadmin4.9.2/"
   
   <Directory "c:/wamp64/apps/phpmyadmin4.9.2/">
   	Options +Indexes +FollowSymLinks +MultiViews
     # AllowOverride all 這一行修改如下
   	AllowOverride none
     <ifDefine APACHE24>
   		# Require local　這一行修改如下
   		Require all granted
   	</ifDefine>
   	<ifDefine !APACHE24>
   		Order Deny,Allow
       Deny from all
       Allow from localhost ::1 127.0.0.1
   	</ifDefine>
   
   # To import big file you can increase values
     php_admin_value upload_max_filesize 128M
     php_admin_value post_max_size 128M
     php_admin_value max_execution_time 360
     php_admin_value max_input_time 360
   </Directory>
   
   ```

   說明： AllowOverride all 表示要使用 .htaccess 檔的權限設定， AllowOverride none 表示不理會。Require local 表示只有 local 可以存取， Require all granted 表示全部放行。　若我們有使用權限的疑慮時，要慎重設定這些東西。

### mariaDB設定

mariadb 預設有匿名使用者，方便檢測。但是實際運用時不能有匿名使用者，否則正常的使用者就無法分辨了，為此我們必須刪除匿名使用者，系統才能比較正常。

> 如何刪除匿名使用者
>
> 1. 以 root 帳號進入
> 2. 點選使用者帳號
> 3. 勾選**使用者名稱**為**任何** 的三個使用者，然後在下方**刪除所選的使用者帳號**右下點擊　**執行**

另外，root 是預設的資料庫管理員，密碼沒有預設，這也很危險，因此需要更換。(教室內的不建議設定，方便大家寫程式)。

使用者帳號的建立需要使用 root 權限，用 root 進入後，新增使用者並且建立一個同名的資料庫，我們統一希望大家使用學號當作帳號，方便識別。例如 U07333311 。

### atom 或是 vscode

程式編輯器建議使用 atom 或是 microsoft 的 vscode。這二個都有龐大的支持，我二個都有請系上幫忙安裝，同學擇偏好使用。原則上我使用 vscode 。基本用法我舉範例時介紹，不特別說明。當然你也可以使用 notepad++ 或是 notepad ，都是OK的！

## coding.im.nuu.edu.tw

作業繳交需要使用 ftp 上傳到coding.im.nuu.edu.tw 上，帳號密碼在上課時說明。對應的網址也在上課時說明。







