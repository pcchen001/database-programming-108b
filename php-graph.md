---
title: PHP繪圖與影像檔管理
date: 2020-02-29 21:38:23
tags:
- php
- graph
- image
categories:
- PHP
- Graph
---

## PHP 繪圖

php 的執行在後台，使用 PHP 進行網頁的動態繪圖其原理是讓 php 根據設計好的指令產生一個瀏覽器能夠渲染的影像檔，在將這個影像檔夾在　html 檔案中，送到瀏覽器上。

我們介紹使用 gd 延伸程式庫來進行，我們可以在 phpinfo () 檢查到  gd 是否已經安裝了！

在網頁中出現繪圖或圖片，除了以 background 的方式置入圖片外，我們必須針對 `<img>` 或是 `<svg> `或是 `<canvas>` 來運作。 canvas 是 HTML5 進行繪圖與動畫的核心元件，我們會在多媒體程式設計課程中學習。SVG 是向量繪圖檔，是一種以 xml 為基礎的繪圖渲染格式，我在web簡介的投影片中有介紹，請自行參考，深入的學習也不在這一門課中。本章介紹的 GD 繪圖是針對 `<img>` 的方式進行的。

 PHP的GD (gif draw) 擴充功能 (extension) 可進行繪圖，再將所產生的內容，以某種影像格式(如 gif, png, jpeg) 輸出。依下列步驟執行的php程式相當於一個影像檔：

1. 建立空圖片
2. 指派色彩
3. 繪製線條、圖形或文字
4. 輸出圖片
5. 釋放記憶體

### 建立空圖片

```php
語法
imagecreate(int x_size, int y_size) 	    //x_size為圖片寬度，y_size為圖片高度
imagecreatetruecolor(int x_size, int y_size  //x_size為圖片寬度，y_size為圖片高度
例如
$im1 = imagecreate(800, 600);                  //單位為pixel，背景色彩預設為黑色
$im2 = imagecreatetruecolor(1024, 768);   //單位為pixel，背景色彩預設為黑色

```

空圖片建立後，我們可在上面繪圖（點、線、圓、矩、多邊）形狀有線條，我們可以指定粗細及色彩。形狀有填入色彩時，我們也可指定色彩。色彩的使用比較麻煩，需要藉助定義色彩的指令定義可能會用到的色彩：

```php
$white = imagecolorallocate($im1, 255, 255, 255);
$red = imagecolorallocate($im1, 255, 0, 0);
$green = imagecolorallocate($im1, 0, 255, 0);
$blue = imagecolorallocate($im1, 0, 0, 255);
$black = imagecolorallocate($im1, 0, 0, 0);
```

定義了色彩之後，我們可以用形狀指令繪圖：

```php
// 橢圓
imageellipse($im1, 100, 100, 199, 199, $red);　　// 圓心座標，寬　高
imageellipse($im1, 50, 100, 200, 100, $green);
// 矩形
imagerectangle($im, 200, 200, 250, 280, $blue);  //　左上角座標，右下角作標
// 直線
imageline($im, 20, 30, 200, 300, $black);
// 弧形
imagearc($im, 100, 100, 200, 100, 90, 270, $green);  // 順時針，90度 到 270度
// 多邊形
imagepolygon($im1, $points, 4, $red);  // $points 是一個　4x2=8 的陣列
    $points=[210, 50, 140, 80, 110, 20, 155, 50];
// 形狀内填色彩
imagefill($im1, 0, 0, $white);  // 背景
imagefilledellipse(...); // 填橢圓
imagefilledrectangle(...); // 填矩形
imagefilledpolygon(...); // 填多邊形
```

![image-20200229215519375](E:\wamp64\www\dbP\source\_posts\image-20200229215519375.png)

文字，在繪圖上需要被視為一個形狀，文字的形狀由字型檔來定義。我們的策略是從字型檔取得形狀的敘述，放到我們的繪圖上。

```php
imagestring($im, 5, 0, 0, “Hello world! This is my first image text.”,$black);
                size, x, y
imagestringup($im, 5, 2, 395, "Hello world! This is my first image text.", $textcolor);
            // 多了 up ，表示直向書寫
imagettftext($im, 16, 0, 0, 20, $textcolor, "simhei.ttf", $string);
　　　　　　　// 指定字形檔　使用的字形檔只能是 .ttf 檔，要從控制台複製過來！

```

### 輸出圖片

GD 提供 gif, png, jpeg 三種格式，將圖片傳送至用戶端瀏覽器或儲存在伺服器。

我

們可以先將圖片存成本機的一個圖檔，然後在網頁上 src 這個影像。也可以直接選染這個影像。

```php
// 直接送出渲染
header("content-type: image/gif");
imagegif($im1);
// 本機端儲存
imagegif($im1, "images/simple.gif");

// 直接送出渲染
header("content-type: image/png");
imagepng($im1);
// 本機端儲存
imagepng($im1, "images/simple.png");

// 直接送出渲染
header("content-type: image/jpeg");
imagejpeg($im1);
// 本機端儲存
imagejpeg($im1, "images/simple.jpg");
```

### 釋放記憶體

```php
imagedestroy($im1);
```

範例1

```php
<?php 
  $points = array(410, 50, 340, 80, 310, 20, 355, 50);

  $im = imagecreatetruecolor(500, 300);
  $background = imagecolorallocate($im, 255, 255, 180);
  $red = imagecolorallocate($im, 255, 0, 0);
  $green = imagecolorallocate($im, 0, 255, 0);
  $black = imagecolorallocate($im, 0, 0, 0);

  //填滿背景色彩
  imagefill($im, 0, 0, $background);

  //繪製弧線
  imagearc($im, 30, 200, 80, 70, 270, 90, $black);

  //繪製橢圓形
  imageellipse($im, 30, 30, 50, 50, $red);
  imageellipse($im, 30, 100, 40, 60, $green);

  //填滿矩形
  imagefilledrectangle($im, 320, 150, 370, 200, $green);

  //填滿多邊形色彩
  imagefilledpolygon($im, $points, 4, $red);

  //繪製文字 imagettftext 有時不正常，
  // $imInfo = "圖片寬度 " . imagesx($im) ." 像素，高度 " . imagesy($im) . " 像素";
  // imagettftext($im, 12, 0, 60, 37, $black, "./kaiu.ttf", "<---圓形");
  // imagettftext($im, 12, 0, 60, 107, $black, "./simhei.ttf", "<---橢圓形");
  // imagettftext($im, 10, 0, 280, 290, $red, "simhei.ttf", $imInfo);
  imagestring($im, 5, 60, 37,  "<---Circle", $black);
  imagestring($im, 5, 60, 107,  "<---Ellipse", $black);
  imagestring($im, 5, 280, 270,  "width: ".imagesx($im)." height: ".imagesy($im), $red);

  //輸出圖片
  header("Content-type: image/png");
  imagepng($im);
  //釋放影像佔用的記憶體
  imagedestroy($im);
?>
```

範例2

```php
<?php 
  $im = imagecreatetruecolor(500, 300);
  
  
  $background = imagecolorallocate($im, 255, 255, 204);
  
  $red = imagecolorallocate($im, 255, 0, 0);
  $green = imagecolorallocate($im, 0, 255, 0);
  $greenx = imagecolorallocate($im, 200, 255, 200);
  $black = imagecolorallocate($im, 0, 0, 0);
  
  //填滿背景色彩
  imagefill($im, 100, 100, $background);
  imagefill($im, 200, 200, $greenx);
   
     //繪製橢圓形
  imageellipse($im, 430, 30, 50, 50, $red);
  imageellipse($im, 430, 100, 40, 60, $green); 
  
  imageline($im, 0, 0, 100, 100, $black);
  imagestringup($im, 5, 2, 395, "Hello world! This is my first image text.", $black);

  $data = array(300, 200, 700, 50, 125);
  for( $i = 0; $i < 5; $i ++){
     $h[$i] = $data[$i]*2.0/5.0; 
	 imagefilledrectangle($im, 45+90*$i, 280-$h[$i]+10, 90+90*$i, 290, $red);
  }
   
  //輸出圖片
  header("Content-type: image/png");
  imagepng($im);
   
  //釋放影像佔用的記憶體
  //imagedestroy($im);
?>
```

練習：

1. 統計上直條圖的繪製， 圖片 寬 500 高 300，資料有 5 筆 300, 200, 700, 50, 125。

5+6 = 11, 500 / 11 約 45，也就是直條的建議寬度。

300 上下各留 10 , 280 / 700 = 4/10，　700 是最大值對應到高度是280。

資料的高分別為 `300*2/5`, `200*2/5`, `700*2/5`, `50*2/5`, `125*2/5`

長條的位置 (45 + (90*$i), 280 – 高 + 10), 右下角( X+45, 290)

```php
$data = array(300, 200, 700, 50, 125);
 for( $i = 0; $i < 5; $i ++){
   $h[$i] = $data[$i]*2.0/5.0; 
   imagefilledrectangle($im, 45+90*$i, 280-$h[$i]+10, 90+90*$i, 290, $red);
 }
```

2. 依據 120, 30, 50, 87, 99 的數值，繪出直條圖。(橫式或直式都可以)
3. 依據 120, 30, 50, 87, 99 的數值，繪出圓餅圖。



# 影像檔的管理

影像檔案的管理提供我們對於上傳來的影像作一個判讀及處理。我們可以想像當客戶上傳一個影像檔案，我們不是應該先確認這是否真的是一張影像，也就是要判斷是否符合影像檔格式，接著要判斷影像的寬高，這會讓你/妳可以決定要如何呈現這張影像。這些資訊都需要讀取影像檔案的後設資料，也就是 exif 資料。

我們使用 exif 程式庫來處理影像檔後設資料的管理。這些後設資料包含影像寬高、日期、影像格式、拍照方式、…。

我們利用 exif 功能來，判斷讀取的圖檔格式為何？是否可用？ 以及讀取影像的寬高，藉以做後續處理。

```php
exif_imagetype("影像檔案名稱")　回傳影像　IMAGETYPE_JPEG, IMAGETYPE_GIF, IMAGETYPE_PNG
getimagesize("影像檔案名稱")[0]  寬
getimagesize("影像檔案名稱")[1]  高
getimagesize("影像檔案名稱")[2]  格式
getimagesize("影像檔案名稱")[3]  字串: 'width="512" height="400"'
exif_read_data("影像檔案名稱");　　回傳一個　php associative 陣列，完整的 exif 資訊
```

程式範例，請準備一張手機或相機拍攝的影像。

```php
<?php
      $exif = exif_read_data("images/pic1.jpg");
      if (!$exif)
      {
        echo("這個檔案沒有 exif 標頭資訊");
      }
      else
      {
        foreach ($exif as $key => $value) 
        {
          echo "$key: $value<br>";
        }        
      }
    ?>
```

#### 取得縮圖

有時我們可以從現有的影像取出去產生縮小圖影像，指令如下：

```php
$img = exif_thumbnail("影像檔案名稱", $width, $height, $imagetype) ;
```

 $width, $height 以及 $imagetype 是函式回傳的寬高型態的資料。都是整數。

範例： exif_thum.php

```php
<?php
   $image = exif_thumbnail($_GET["imgName"], $width, $height, $imagetype);
   if($image){
       header("content-type:".image_type_to_mimi_type($imagetype));
       echo $image;
       exit();
   }
?>
```

```html
<html>
  <body>
    <img src="exif_thum.php?imgName=images/pic1.jpg" />
  </body>
</html>
```

### 插入影像到繪圖中

現在我們結合取得影像跟繪圖，我們可以用一張影像創建一個圖片，在接著進行繪圖：

```
語法
imagecreate(int x_size, int y_size) 	    //x_size為圖片寬度，y_size為圖片高度
imagecreatetruecolor(int x_size, int y_size  //x_size為圖片寬度，y_size為圖片高度
imagecreatefromgif("影像檔案名稱")  // 取 gif 影像作背景，相同大小
imagecreatefromjpeg("影像檔案名稱")　　// 取 jpeg 影像作背景，相同大小
imagecreatefrompng("影像檔案名稱")　　　// 取 png 影像作背景，相同大小
```

下面是一個範例：

```php
//讀取外部圖片
  $im = imagecreatefromjpeg("images/pic1.jpg");
  $textcolor = imagecolorallocate($im, , 0, 255);  
  //在圖片中加入拍攝者資訊
  imagestring($im, 20, 380, 350, "Photo By Jean", $textcolor);
  //輸出圖片
  header("content-type: image/png");
  imagepng($im);
  //釋放圖片佔用的記憶體
  imagedestroy($im); 
```

另一個範例，我們可以輸出圖片到新的檔案，網頁並不輸出

```php
<?php
    // images 子目錄下需要準備 pic01.jpg 及 pic02.jpg
  $src1 = imagecreatefromjpeg("images/pic01.jpg");
  $src2 = imagecreatefromjpeg("images/pic02.jpg");
    
  //建立新的空圖片  
  $newImg_w = (imagesx($src1) + imagesx($src2)) * 0.5;
  $newImg_h = imagesy($src1) * 0.5; 
  $newImg = imagecreatetruecolor($newImg_w, $newImg_h);  

  //進行複製並縮圖
  imagecopyresized($newImg, $src1, 0, 0, 0, 0, $newImg_w * 0.5, $newImg_h, imagesx($src1), imagesy($src1));
  imagecopyresized($newImg, $src2, imagesx($src1) * 0.5, 0, 0, 0, $newImg_w * 0.5, $newImg_h, imagesx($src2), imagesy($src2));  
   
  //輸出圖片
  imagepng($newImg, "images/pic3.png");
   
  //釋放影像佔用的記憶體
  imagedestroy($src1);
  imagedestroy($src2);  
  imagedestroy($newImg); 
?>
```



#### 影像複製

```
語法
imagecopyresized(resource dst_image, resource src_image, int dst_x, int dst_y, int src_x,  int src_y, int dst_w, int dst_h, int src_w, int src_h)
```

```php
imagecopyresized($img2, $img1, 0, 0, 0, 0, 200, 150, 1000, 750 );
imagepng($img2, "newimage.png"); // 另存新檔
```

練習：

1. 列出一個影像檔(從相機取得的)所有的 exif 資訊，以一個 `<table>` 將這些 exif 資訊輸出。
2. 將一個影像檔，複製出一個小圖另外存檔，e.g. n0010.jpg 是 1024 x 768 我們需要另外一個 40x30 的小圖，檔名為 n0010s.jpg。
3. 根據一個陣列，繪出長條圖及圓餅圖。

