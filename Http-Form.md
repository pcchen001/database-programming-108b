---
title: Http 表單處理
date: 2020-03-18 13:12:49
tags:
- form
- session
- cookie
- request
- response
categories:
- http
---

伺服器提供來自網路上的各種需求的回應。

需求包括分為

1. 將伺服器的某個檔案回傳。
2. 根據需求，進行運算後，回傳答覆。

World Wide Web 以 HTTP 協定來進行交談互動，從客戶端送來 Request 從伺服器端送回 Response 。

請撰寫下列程式碼： a1.html

```html
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <form name="myForm" method="GET" action="a1.php">
      Name: <input name="uname" type="text" value="Me"></input></br>
      <input type="submit">登入</input>
    </form>
  </body>
</html>
```

以及 a1.php

```php
<?php
  $user_name = $_GET['uname'];
  date_default_timezone_set("Asia/Taipei");
  echo "<h1> Welcome $user_name </h1>";
  echo 'Now:       '. date('Y-m-d') ."<br>";
?>
```

a1.html 是一個簡潔表單 form。在表單內，一個 submit 的輸入會啟動 http request.

method 是 GET. (另一種是 POST) 送出的目的程式是同一個網站的 a1.php 。

這個 form 裡面只有一個輸入欄位，送出後，接收端 a1.php 會以 $_GET['uname'] 取得欄位值。

HTML 的 form 可以包含 HTML 所提供的gui 介面輸入，包括：

select

textarea

input

​	type= text, radio, checkbox, password 等

範例一： 姓名 密碼

範例二：加上性別選擇，年齡群選擇 (radio)

範例三：加上喜好運動( checkbox )

範例四：註冊表單( 基本資料填寫 ) 回應完成註冊。

練習：(表單設計) 

```
姓名
性別
出生年月日
使用行動裝置
E-mail 信箱
電話
職業
密碼
密碼確認
```

HTML Form 用 Tags:  [W3Schools連結](http://www.w3schools.com/html/html_forms.asp 
)

HTML Form/Input Type 說明： [W3Schools1](http://www.w3schools.com/tags/tag_input.asp 
) [W3Schools2](http://www.w3schools.com/html/html5_form_input_types.asp)

