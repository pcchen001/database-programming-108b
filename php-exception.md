---
phtitle: PHP Exception Handling
date: 2020-3-1 11:11:11
tags:
- PHP
- Exception Handling
- Error & Error Handling
categories: 
- 資料庫程式設計
---

# php 的異常處理及錯誤

php 與 C++ 對於異常及錯誤的處理語法有點相似。

先看一個範例：

```php
<?php
  try
  {
    open_folder("C:\\book");			
  }
  catch(Exception $ex)			
  {
    echo '錯誤訊息：'.$ex->getMessage().'<br>';	
    echo '錯誤代碼：'.$ex->getCode().'<br>'; 	
    echo '檔案路徑：'.$ex->getFile().'<br>'; 	
    echo '錯誤行數：'.$ex->getLine().'<br>';	
  }
 
  function open_folder($folder)
  {
    if (file_exists($folder)) 		
      opendir($folder)
    else
      throw new Exception('欲開啟的資料夾不存在');
  } 
?>
```

Exception 是 php 內建的類別。

php 處理 Error 的函式有下列：

```php
error_reporting([int error_type]) // 有錯誤要處理的型態設定 0
set_error_handler(callback error_handler) // 處理函式宣告
restore_error_handler()
trigger_error( string error_msg [, int error_type]) // 發出錯誤, 相對於拋出異常
error_log(string message [, int message_type [, string destination [, string extra_headers]]])
```

範例：

```php
 <?php
   error_reporting(0);	
   set_error_handler('error_handler'); 
   open_folder("C:\\book");	
   
   function open_folder($folder)
   {
     if (file_exists($folder))
       opendir($folder);
     else
       trigger_error('欲開啟的資料夾不存在', E_USER_ERROR);
   } 
 
   function error_handler($errno, $errmsg, $filename, $linenum)
   {
     echo '錯誤代碼：'.$errno.'<br>';
     echo '錯誤訊息：'.$errmsg.'<br>';
     echo '檔案路徑：'.$filename.'<br>';	
     echo '錯誤行數：'.$linenum.'<br>';
   }
 ?>
```

`error_handler` 是一個 callback function, 意思是當錯誤發生時，這個函式會被內部的程式回呼，並且傳給他四個變數。我們可以針對這四個變數進行處理，例如列印出來。

