---
title: 應用 會員管理
date: 2020-05-6 11:22:16
tags:
- email
- session
categories:
- Application
---

# 會員管理

首先我們要設一會員資料表

```
CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(10) NOT NULL DEFAULT '',
  `password` varchar(10) NOT NULL DEFAULT '',
  `name` varchar(10) NOT NULL DEFAULT '',
  `sex` char(2) NOT NULL DEFAULT '',
  `cellphone` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `enable` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;
```

join.php

```html
<!doctype html>
<html>
  <head>
    <title>加入會員</title>
    <meta charset="utf-8">
  </head>
  <body>
    <form action="addmember.php" method="post" name="myForm">
      <table border="2" align="center" bordercolor="#6666FF">
        <tr> 
          <td colspan="2" bgcolor="#6666FF" align="center"> 
            <font color="#FFFFFF">請填入下列資料 (標示「*」欄位請務必填寫)</font>
          </td>
        </tr>
        <tr bgcolor="#99FF99"> 
          <td align="right">*使用者帳號：</td>
          <td><input name="account" type="text" size="15">
          (請使用英文或數字鍵)</td>
        </tr>
        <tr bgcolor="#99FF99"> 
          <td align="right">*使用者密碼：</td>
          <td><input name="password" type="password" size="15">
          (請使用英文或數字鍵)</td>
        </tr>
        <tr bgcolor="#99FF99"> 
          <td align="right">*密碼確認：</td>
          <td><input name="re_password" type="password" size="15">
          (再輸入一次密碼)</td>
        </tr>
        <tr bgcolor="#99FF99">
          <td align="right">*姓名：</td>
          <td><input name="name" type="text" size="8"></td>
        </tr>
        <tr bgcolor="#99FF99"> 
          <td align="right">*性別：</td>
          <td> 
            <input type="radio" name="sex" value="男" checked>男 
            <input type="radio" name="sex" value="女">女
          </td>
        </tr>
        <tr bgcolor="#99FF99"> 
          <td align="right">*生日：</td>
          <td>民國
            <input name="year" type="TEXT" size="2">年 
            <input name="month" type="TEXT" size="2">月 
            <input name="day" type="TEXT" size="2">日
          </td>
        </tr>
        <tr bgcolor="#99FF99"> 
          <td align="right">電話：</td>
          <td><input name="telephone" type="text" size="20"></td>
        </tr>
        <tr bgcolor="#99FF99"> 
          <td align="right">行動電話：</td>
          <td><input name="cellphone" type="text" size="20"></td>
        </tr>
        <tr bgcolor="#99FF99"> 
          <td align="right">地址：</td>
          <td><input name="address" type="text" size="45"></td>
        </tr>
        <tr bgcolor="#99FF99">
          <td align="right">E-mail 帳號：</td>
          <td><input name="email" type="text" size="30"></td>
        </tr>
        <tr bgcolor="#99FF99"> 
          <td align="right">個人網站：</td>
          <td><input name="url" type="text" value="http://" size="40"></td>
        </tr>
        <tr bgcolor="#99FF99"> 
          <td align="right">備註：</td>
          <td><textarea name="comment" cols="45" rows="4" ></textarea></td>
        </tr>
        <tr bgcolor="#99FF99"> 
          <td align="center" colspan="2"> 
            <input type="submit" value="加入會員">　
            <input type="reset" value="重新填寫">
          </td>
        </tr>
      </table>
    </form>
  </body>
</html>
```

addmember.php

```php+HTML
<?php
  require_once("dbtools.inc.php");

  //取得表單資料
  $account = $_POST["account"];
  $password = $_POST["password"];
  $name = $_POST["name"];
  $sex = $_POST["sex"];
  $year = $_POST["year"];
  $month = $_POST["month"];
  $day = $_POST["day"];
  $telephone = $_POST["telephone"];
  $cellphone = $_POST["cellphone"];
  $address = $_POST["address"];
  $email = $_POST["email"];
  $url = $_POST["url"];
  $comment = $_POST["comment"];

  //建立資料連接
  $link = create_connection();

  //檢查帳號是否有人申請
  $sql = "SELECT * FROM members Where account = '$account'";
  $result = execute_sql($link, "u0533001", $sql);

  //如果帳號已經有人使用
  if (mysqli_num_rows($result) != 0)
  {
    //釋放 $result 佔用的記憶體
    mysqli_free_result($result);

    //顯示訊息要求使用者更換帳號名稱 -- 直接產生網頁的程式碼！
    echo "<script type='text/javascript'>";
    echo "alert('您所指定的帳號已經有人使用，請使用其它帳號');";
    echo "history.back();";
    echo "</script>";
  }

  //如果帳號沒人使用
  else
  {
    //釋放 $result 佔用的記憶體
    mysqli_free_result($result);

    //執行 SQL 命令，新增此帳號
    $sql = "INSERT INTO `members` (`account`, `password`, `name`, `sex`,
            `year`, `month`, `day`, `telephone`, `cellphone`, `address`,
            `email`, `url`, `comment`) VALUES ('$account', '$password',
            '$name', '$sex', $year, $month, $day, '$telephone',
            '$cellphone', '$address', '$email', '$url', '$comment')";

    $result = execute_sql($link, "u0533001", $sql);
  }

  //關閉資料連接
  mysqli_close($link);
?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>新增帳號成功</title>
  </head>
  <body bgcolor="#FFFFFF">
    <p align="center"><img src="Success.jpg">
    <p align="center">恭喜您已經註冊成功了，您的資料如下：（請勿按重新整理鈕）<br>
      帳號：<font color="#FF0000"><?php echo $account ?></font><br>
      密碼：<font color="#FF0000"><?php echo $password ?></font><br>
      請記下您的帳號及密碼，然後<a href="index0.html">登入網站</a>。
    </p>
  </body>
</html>
```

登入頁面相對簡單， index0.html

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>會員管理系統</title>
  </head>
  <body>
    <p align="center"><img src="member.jpg"></p>  
    <p>歡迎來到本站，您必須加入成為本站的會員，才有權限使用本站的功能。
       若您已經擁有帳號，請輸入您的帳號及密碼，然後按 [登入] 鈕；
       若尚未成為本站會員，請點按 [加入會員] 超連結；若您忘記自己的帳
       號及密碼，請點按 [查詢密碼] 超連結。</p>
    <form action="checkpwd.php" method="post" name="myForm">
      <table width="40%" align="center">
        <tr> 
          <td align="center"> 
            <font color="#3333FF">帳號：</font> 
            <input name="account" type="text" size="15">
          </td>
        </tr>
        <tr> 
          <td align="center"> 
            <font color="#3333FF">密碼：</font> 
            <input name="password" type="password" size="15">
          </td>
        </tr>
        <tr>
          <td align="center"> 
            <input type="submit" value="登入">      　 
            <input type="reset" value="重填">
          </td>
        </tr>
      </table>
    </form>
    <p align="center">
      <a href="join.html">加入會員</a>　
      <a href="search_pwd.html">查詢密碼</a></p>
  </body>
</html>
```

查詢密碼我們使用了 mail 及顯示 search_pwd.html, search.php

```html
<!doctype html> 
<html>
  <head>
    <meta charset="utf-8">
    <title>查詢密碼</title>
  </head>
  <body>
    <p align="center"><img src="inquire.jpg"></p>
    <p>請輸入您的姓名及 E-mail 帳號，並選擇一種顯示方式，然後按 [查詢] 鈕。</p>
    <form method="post" action="search.php" >
      <table align="center">
        <tr><td>帳號：</td>
          <td><input type="text" name="account" size="10"></td>
        </tr>
        <tr><td>電子郵件帳號：</td>
          <td><input type="text" name="email" size="30"></td>
        </tr>
        <tr><td>顯示方式：</td>
          <td>
            <select name="show_method">
              <option value="網頁顯示">網頁顯示</option>
              <option value="E-mail 傳送">E-mail 傳送</option>
            </select>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="center"> 
            <input type="submit" value="查詢">　
            <input type="reset" value="重填">
          </td>
        </tr>
      </table>
    </form>
  </body>
</html>
```

search.php

```php
<?php
  require_once("dbtools.inc.php");
  require_once("../ch16eMail/phpmail.tools.php");  // 這要看你哦 phpmail.tools.php 放在哪裡
  header("Content-type: text/html; charset=utf-8");

  //取得表單資料
  $account = $_POST["account"];
  $email = $_POST["email"];
  $show_method = $_POST["show_method"];

  //建立資料連接
  $link = create_connection();

  //檢查查詢的帳號是否存在
  $sql = "SELECT name, password FROM members WHERE
          account = '$account' AND email = '$email'";
  $result = execute_sql($link, "u0533001", $sql);

  //如果帳號不存在
  if (mysqli_num_rows($result) == 0)
  {
    //顯示訊息告知使用者，查詢的帳號並不存在
    echo "<script type='text/javascript'>
            alert('您所查詢的資料不存在，請檢查是否輸入錯誤。');
            history.back();
          </script>";
  }
  else  //如果帳號存在
  {
    $row = mysqli_fetch_object($result);
    $name = $row->name;
    $password = $row->password;
    $message = "
      <!doctype html>
      <html>
        <head>
          <title></title>
          <meta charset='utf-8'>
        </head>
        <body>
          $name 您好，您的帳號資料如下：<br><br>
          　　帳號：$account<br>
          　　密碼：$password<br><br>
            <a href='index0.html'>按此登入本站</a>
          </body>
      </html>
    ";

    if ($show_method == "網頁顯示")
    {
      echo $message;   //顯示訊息告知使用者帳號密碼
    }
    else
    {
      $mail = pmail();
      $subject = "=?utf-8?B?" . base64_encode("帳號通知") . "?=";
      $headers  = "MIME-Version: 1.0\r\nContent-type: text/html; charset=utf-8\r\n";
      // mail($email, $subject, $message, $headers);
      $sendName = "=?utf-8?B?" . base64_encode("管理員") . "?=";
      $mail->setFrom("Admin@mystore.com", $sendName);
      $nameu = "=?utf-8?B?" . base64_encode("$name") . "?=";
      $mail->addAddress($email, $nameu);
      $mail->Subject = $subject;
      $mail->isHTML(true);
      $mail->Body = $message;
      if( !$mail->send() )
         echo "Mail not Sended. Error: ".$mail->ErrorInfo;
      else {
        //顯示訊息告知帳號密碼已寄至其電子郵件了
        echo "$name 您好，您的帳號資料已經寄至 $email<br><br>
              <a href='index0.html'>按此登入本站</a>";
      }
    }
  }

  mysqli_free_result($result);
  mysqli_close($link);
?>
```

main.php 提供了會員修改或是刪除自己帳號的介面：依照常規這應該使用 session 實作，但是 cookie 有時也可以簡易的扮演查驗結果暫存的腳色：登入時將 cookie 寫入，那麼我們也就可以藉由 cookie 來確認登入還是有效的！

```php
<?php
  //檢查 cookie 中的 passed 變數是否等於 TRUE
  $passed = $_COOKIE["passed"];

  /*  如果 cookie 中的 passed 變數不等於 TRUE
      表示尚未登入網站，將使用者導向首頁 index.html	*/
  if ($passed != "TRUE")
  {
    header("location:index0.html");
    exit();
  }
?>
<!doctype html>
<html>
  <head>
    <title>會員管理</title>
    <meta charset="utf-8">
  </head>
  <body>
    <p align="center"><img src="management.jpg"></p>
    <p align="center">
      <a href="modify.php">修改會員資料</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="delete.php">刪除會員資料</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="logout.php">登出網站</a>
    </p>
  </body>
</html>
```

修改資料動用了二個程式：  modify.php 以及 update.php 一個協助進行修改的介面，一個正式進行更改：

```php
<?php
  //檢查 cookie 中的 passed 變數是否等於 TRUE
  $passed = $_COOKIE{"passed"};

  //如果 cookie 中的 passed 變數不等於 TRUE
  //表示尚未登入網站，將使用者導向首頁 index.html
  if ($passed != "TRUE")
  {
    header("location:index0.html");
    exit();
  }

  //如果 cookie 中的 passed 變數等於 TRUE
  //表示已經登入網站，取得使用者資料
  else
  {
    require_once("dbtools.inc.php");

    $id = $_COOKIE{"id"};

    //建立資料連接
    $link = create_connection();

    //執行 SELECT 陳述式取得使用者資料
    $sql = "SELECT * FROM members Where id = $id";
    $result = execute_sql($link, "u0533001", $sql);

    $row = mysqli_fetch_assoc($result);
?>
<!doctype html>
<html>
  <head>
    <title>修改會員資料</title>
    <meta charset="utf-8">
  </head>
  <body>
    <p align="center"><img src="modify.jpg"></p>
    <form name="myForm" method="post" action="update.php" >
      <table border="2" align="center" bordercolor="#6666FF">
        <tr>
          <td colspan="2" bgcolor="#6666FF" align="center">
            <font color="#FFFFFF">請填入下列資料 (標示「*」欄位請務必填寫)</font>
          </td>
        </tr>
        <tr bgcolor="#99FF99">
          <td align="right">*使用者帳號：</td>
          <td><?php echo $row{"account"} ?></td>
        </tr>
        <tr bgcolor="#99FF99">
          <td align="right">*使用者密碼：</td>
          <td>
            <input type="password" name="password" size="15" value="<?php echo $row{"password"} ?>">
            (請使用英文或數字鍵，勿使用特殊字元)
          </td>
        </tr>
        <tr bgcolor="#99FF99">
          <td align="right">*密碼確認：</td>
          <td>
            <input type="password" name="re_password" size="15" value="<?php echo $row{"password"} ?>">
            (再輸入一次密碼，並記下您的使用者名稱與密碼)
          </td>
        </tr>
        <tr bgcolor="#99FF99">
          <td align="right">*姓名：</td>
          <td><input type="text" name="name" size="8" value="<?php echo $row{"name"} ?>"></td>
        </tr>
        <tr bgcolor="#99FF99">
          <td align="right">*性別：</td>
          <td>
            <input type="radio" name="sex" value="男" checked>男
            <input type="radio" name="sex" value="女">女
          </td>
        </tr>
        <tr bgcolor="#99FF99">
          <td align="right">*生日：</td>
          <td>民國
            <input type="text" name="year" size="2" value="<?php echo $row{"year"} ?>">年
            <input type="text" name="month" size="2" value="<?php echo $row{"month"} ?>">月
            <input type="text" name="day" size="2" value="<?php echo $row{"day"} ?>">日
          </td>
        </tr>
        <tr bgcolor="#99FF99">
          <td align="right">電話：</td>
          <td>
            <input type="text" name="telephone" size="20" value="<?php echo $row{"telephone"} ?>">
            (依照 (02) 2311-3836 格式 or (04) 657-4587)
          </td>
        </tr>
        <tr bgcolor="#99FF99">
          <td align="right">行動電話：</td>
          <td>
            <input type="text" name="cellphone" size="20" value="<?php echo $row{"cellphone"} ?>">
            (依照 (0922) 302-228 格式)
          </td>
        </tr>
        <tr bgcolor="#99FF99">
          <td align="right">地址：</td>
          <td><input type="text" name="address" size="45" value="<?php echo $row{"address"} ?>"></td>
        </tr>
        <tr bgcolor="#99FF99">
          <td align="right">E-mail 帳號：</td>
          <td><input type="text" name="email" size="30" value="<?php echo $row{"email"} ?>"></td>
        </tr>
        <tr bgcolor="#99FF99">
          <td align="right">個人網站：</td>
          <td><input type="text" name="url" size="40" value="<?php echo $row{"url"} ?>"></td>
        </tr>
        <tr bgcolor="#99FF99">
          <td align="right">備註：</td>
          <td><textarea name="comment" rows="4" cols="45"><?php echo $row{"comment"} ?></textarea></td>
        </tr>
        <tr bgcolor="#99FF99">
          <td colspan="2" align="CENTER">
            <input type="submit" value="修改資料">
            <input type="reset" value="重新填寫">
          </td>
        </tr>
      </table>
    </form>
  </body>
</html>
<?php
    //釋放資源及關閉資料連接
    mysqli_free_result($result);
    mysqli_close($link);
  }
?>
```

update.php

```php
<?php
  //檢查 cookie 中的 passed 變數是否等於 TRUE
  $passed = $_COOKIE["passed"];

  /* 如果 cookie 中的 passed 變數不等於 TRUE，
     表示尚未登入網站，將使用者導向首頁 index.html */
  if ($passed != "TRUE")
  {
    header("location:index.html");
    exit();
  }

  /* 如果 cookie 中的 passed 變數等於 TRUE，
     表示已經登入網站，則取得使用者資料 */
  else
  {
    require_once("dbtools.inc.php");

    //取得 modify.php 網頁的表單資料
    $id = $_COOKIE["id"];
    $password = $_POST["password"];
    $name = $_POST["name"];
    $sex = $_POST["sex"];
    $year = $_POST["year"];
    $month = $_POST["month"];
    $day = $_POST["day"];
    $telephone = $_POST["telephone"];
    $cellphone = $_POST["cellphone"];
    $address = $_POST["address"];
    $email = $_POST["email"];
    $url = $_POST["url"];
    $comment = $_POST["comment"];

    //建立資料連接
    $link = create_connection();

    //執行 UPDATE 陳述式來更新使用者資料
    $sql = "UPDATE members SET password = '$password', name = '$name',
            sex = '$sex', year = $year, month = $month, day = $day,
            telephone = '$telephone', cellphone = '$cellphone',
            address = '$address', email = '$email', url = '$url',
            comment = '$comment' WHERE id = $id";
    $result = execute_sql($link, "u0533001", $sql);

    //關閉資料連接
    mysqli_close($link);
  }
?>
<!doctype html>
<html>
  <head>
    <title>修改會員資料成功</title>
    <meta charset="utf-8">
  </head>
  <body>
    <center>
      <img src="revise.jpg"><br><br>
      <?php echo $name ?>，恭喜您已經修改資料成功了。
      <p><a href="main.php">回會員專屬網頁</a></p>
    </center>
  </body>
</html>

```

delete.php

```php+HTML
<?php
  //檢查 cookie 中的 passed 變數是否等於 TRUE
  $passed = $_COOKIE["passed"];

  /*  如果 cookie 中的 passed 變數不等於 TRUE，
      表示尚未登入網站，將使用者導向首頁 index.html */
  if ($passed != "TRUE")
  {
    header("location:index.html");
    exit();
  }

  /*  如果 cookie 中的 passed 變數等於 TRUE，
      表示已經登入網站，將使用者的帳號刪除 */
  else
  {
    require_once("dbtools.inc.php");

    $id = $_COOKIE["id"];

    //建立資料連接
    $link = create_connection();

    //刪除帳號
    $sql = "DELETE FROM members Where id = $id";
    $result = execute_sql($link, "u0533001", $sql);

    //關閉資料連接
    mysqli_close($link);
  }
?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>刪除會員資料成功</title>
  </head>
  <body bgcolor="#FFFFFF">
    <p align="center"><img src="erase.jpg"></p>
    <p align="center">
      您的資料已從本站中刪除，若要再次使用本站台服務，請重新申請，謝謝。
    </p>
    <p align="center"><a href="index0.html">回首頁</a></p>
  </body>
</html>
```

最後是登出的程式碼，這裡就是去將 cookie 的內容清除， logout.php

```php
<?php
  //清除 cookie 內容
  setcookie("id", "");
  setcookie("passed", "");

  //將使用者導回主網頁
  header("location:index0.html");
  exit();
?>
```

以會員管理為例，上面的程式碼提供了一個良好的介紹。為了熟悉寄信以及會員管理，作業 4 中我們要求同學將確認信置入，並且增加管理員的會員管理介面。

