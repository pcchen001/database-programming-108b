---
title: PHP 直譯器
date: 2020-02-21 21:38:23
tags:
- php
categories:
- 資料庫程式設計
---

## PHP 安裝及互動環境

wamp 安裝時，包含了 php 直譯器。目前比較穩定的是 7.3 版本。

php 安裝之後，原則上會順便設好　path, 我們可以在 console(cmd) 下直接執行。

雖然 PHP 主要在網站伺服器中用來回應　http 的 request ，但是作為一個程式語言，PHP 是可以直接執行的，我們可以

```
> php file.php 
```

執行一個 php 檔的程式，或是

```
php -a 
php > 
```

進入 interactive 模式。在這個模式，我們可以直接執行　php 的指令。

```

php > $x = 100; $y = 200; $z = $x + $y;
php > echo $z;
300
php >    
```

我們先在這個模式下，初步看一下 PHP 語法及程式。

### 語法慣例

有保留字 (reserved word) 

有特殊字元 (special character) 

- 單引號　' '  雙引號  " "

識別字 (identifier)

- 變數皆須冠上 $ 
- 英文字母大小寫有區分的 （除了保留字） 
- 空白字元， PHP會忽略多餘的空白字元

```
$x = 10;
$x  =  10;  // 跟上一行相同
```

- 分號 ，程式的每一行敘述結尾要加上分號。
- 註解 
  - 　雙斜線　//這是第一種單行註解符號
  - 　井字符號　# 是第二種單行註解符號
  -  斜線加星星　/* 是多行註解　*/

### ＰＨＰ　保留字

|           |                 |              |             |
| --------- | --------------- | ------------ | ----------- |
| and       | or              | xor          | \__FiLE__   |
| exception | php_user_filter | \__LINE__    | array()     |
| as        | break           | case         | cfunction   |
| class     | const           | continue     | declare     |
| default   | die()           | do           | echo()      |
| else      | elseif          | empty()      | enddeclare  |
| endfor    | endforeach      | endif        | endswitch   |
| endwhile  | eval()          | exit()       | extends     |
| for       | foreach         | function     | global      |
| include() | include_once()  | if           | isset()     |
| list()    | new             | old_function | print()     |
| require() | require_once()  | return()     | static      |
| switch    | unset()         | use          | var         |
| while     | \__FUNCTION__   | \__CLASS__   | \__METHOD__ |

範例檔案 sample05.php

```
php > for($c = 0; $c < 20; $c+=5)
php >   echo ($c."---> ".($c*9/5+32)."\n");
0---> 32
5---> 41
10---> 50
15---> 59
20---> 68
php >    
```

若要執行程式檔案，可先建立 php檔，例如 s5.php

```
<?php
$a = 100;
$b = 200;
for($c = 0; $c < 20; $c++){
    echo ($c."---> ".($c*9/5+32)."\n");;
}
?>
```

然後執行

```
E:\wamp64\www\dbP>php s5.php
0---> 32
1---> 33.8
2---> 35.6
3---> 37.4
4---> 39.2
5---> 41
6---> 42.8
7---> 44.6
8---> 46.4
9---> 48.2
10---> 50
11---> 51.8
12---> 53.6
13---> 55.4
14---> 57.2
15---> 59
16---> 60.8
17---> 62.6
18---> 64.4
19---> 66.2
```



## PHP型別

分為三大類：純量(Scalar type ), 複合(compound type), 特殊。

Scalar: integer, float, double, boolean, string

Compound: array, object

特殊: NULL 以及 resource 

