---
title: ＰＨＰ　基礎語法
date: 2020-02-22 10:16:28
tags:
- 語法
- 基礎
categories:
- 資料庫程式設計
mathjax: true
---

基礎語法：型別、控制、陣列與物件、函式、類別。

# 型別

1. 純量型別
   1. 整數　(integer)
   2. 浮點數 ( float, double, real 都是等義的！, 顯示為double)
   3. 布林 (boolean)
   4. 字串 (string)
2. 複合型別
   1. 陣列
   2. 物件
3. 特殊型別
   1. NULL
   2. resource

PHP變數不必宣告，首次指定的數值會決定該變數的型別。但是每一個變數在每一個時刻還是有它的型別，這個型別可以用 gettype($x) 來取得。

```php
php > $a = 1;
php > $b = 1.2;
php > $c = true;
php > $d = "Hello world";
php > echo (gettype($a)." ".gettype($b)." ".gettype($c)." ".gettype($d));
integer double boolean string
php >    
```

一個變數如果沒有被定義，isset($x)會返回 false, 否則返回 true。

```php
php > $a = NULL;
php > if( isset($a) ) echo "defined"; else echo "undefined";
undefined
php > if( isset($z) ) echo "defined"; else echo "undefined";
undefined
php > $b = 23;
php > if( isset($b) ) echo "defined"; else echo "undefined";
defined
php >     
```

整數的字面寫法有三個，　23, 023, 以及 0x23 ，分別表示 10, 8 以及 16進位。

浮點數的字面寫法可以使用科學記號， 123E3 或是 234e-3 都可以。

某個型別的變數轉型為布林時，只有下列情況被視為 false，其餘都是 true：

1. 整數 0
2. 浮點數 0.0
3. 空字串 "" 以及字串"0"
4. 沒有元素的陣列
5. 沒有成員的物件
6. 特殊型別 NULL 或是還沒有被定義的變數

字串用引號，單引號或雙引號都可以，但是單引號不進行逸脫字元的解析或是變數的解析，雙引號則會。

參考 ch02\single_q.php 以及 ch02\double_q.php。

```php
	echo('生日快樂！');	
	echo('C:\\Win');			
	echo('I am \'Jean\'.');	
	
  echo("Today is fine!<BR>");				
  echo("今天天氣很好！<BR>");			
  echo("Happy Birthday.\n<BR>");		
  echo("Happy Birthday.\\n<BR>");		
  echo("My name is \"Jean\".<BR>");		
  echo("My name is 'Jean'.<BR>"); 		
  $str = "Jean";						
  echo("My name is $str.<BR>");			
  //echo("My name is "Jean".<BR>"); 
```

變數的解析，必要時需要加上大括號，參考 ch02\nobraces.php, ch02\braces.php 。

```php
      $str1 = "Mon";								//將變數str1設定為字串 "Mon"
      $str2 = "Tues";								//將變數str2設定為字串 "Tues"
      echo("Today is $str1day.<BR>");		//將變數str1穿插於雙引號字串
      echo("Today is $str2day.<BR>");		//將變數str2穿插於雙引號字串
      
      $str1 = "Mon";
      $str2 = "Tues";
      echo("Today is {$str1}day.<br>");		//<br> 為HTML的強制換行標籤
      echo("Today is {$str2}day.<br>");		//<br> 為HTML的強制換行標籤
```

一個變數若沒有指定一個數值，除了 isset($x) 之外，出現在程式中都會是錯誤。若需要一個變數但又不希望有數值，則可以選擇給 NULL，e.g. 

```php
$x = NULL;  
```

給了 NULL 的變數，不會有 undefined 的錯誤，也可以用 ($x == NULL) 來判斷是否有給非 NULL 的數值。

resource 型別不容易講清楚，實際用到時再解釋。

## 型別轉換

跟 C++ 相同，相同型別的變數才能進行運算，為了方便 PHP 也定義了一些自動轉型。當然我們也可以強制轉型以避免不恰當的自動轉型。

一個 boolean 可以自動轉型為 integer, double 或是 string 。　0 或是 1 。

一個 integer 可以自動轉型為boolean, double或是 string 。

一個double 可以自動轉型為boolean, string。

一個字串可以自動轉型為 boolean, integer 或是 double。

```php
php > $a = 23 + "10";
php > echo $a;
33
```

判斷型別的函式，除了 gettype($x) 之外，也可以個別使用類似 is_integer($x) 的函式取得布林。例如

```php
is_integer($x)
is_real($x), is_float($x), is_real($x)  // 三個同義
is_bool($x)
is_string($x)
is_number($x)  // 相當於 is_integer($x)||is_real($x)
is_scalar($x)  //相當於 boolean, integer, double, string 其中一個
is_array($x)
is_object($x)
is_null($x)
is_resource($x)
```



### 強制轉型

對自動轉型有疑慮時，可以在變數或字面值(literal) 前加上括號型別，如 (int)$x 或 (double)23　進行強制轉型。為了方便 int 與 integer 同義。float, double, real 同義。bool, boolean 同義。 

settype($x, "integer"), intval($x), doubleval($x), strval($x) 也可以用，只是不好記住。

簡要範例：

```php
$x = 23.9;
$y = (double)$x;
$z = intval($y);
settype($x, "real");
```

使用(int)$x 轉型，取的值型別會改變，但 $x 變數的型別不會改變。若要直接改 $x 的型別，可以使用

```php
$x = 23.6;
$x = (int)$x; 
```

或是利用函式　進行型別轉換。

提醒同學，php 或 javascript 雖然都不必宣告型別，但骨子裡都有型別，還是要留意。

# 變數命名規則

- 變數的前面必須加上錢字符號 ($) 做為識別 。
- 第一個字元只能是英文字母或底線，之後其它字元可以是英文字母、底線或阿拉伯數字，而且英文字母有大小寫之分。
- PHP的保留字沒有大小寫之分，但變數、常數和類別的名稱則有大小寫之分。
- 不能使用PHP的保留字、內建變數的名稱、內建函式的名稱、內建物件的名稱…。
- 避免在內部範圍使用與外部範圍相同的名稱。 
- 除了一般指派外，也可使用參照指派 (assign by reference)，也就是別名！

```php
$var1 = "John"; 
$var2 = **&**$var1; // 加上 & $var2 是 $var1 的別名
$var2 = "Mary"; 
echo $var2; 
echo $var1;
```

# 常數的定義

```php
define("pi", 3.14159);
define("PI", 3.14159, true); // 大小寫都可以
```

# 運算子

常用運算子與 C++ 相同，我們就不重複介紹。php 另外有三個運算子

點　. 字串連結運算； 不同於 Javascript 使用 + ，PHP 使用 點 .

=== 強相同　等於且相同型別。　

!== 強不同　不相等或不同型別。

舉個例子：　12 == "12" 但是 12!=="12"。在 PHP 語言中，字串內的數值可以被視為數值來使用，因此在判斷時 "12" 與 12 都是可以互通。

在數值計算時， $c = 12 +"23"; 答案是 35。　

自字串相連時，$a = "result: "; $b = 23;　$c = $a." ".$b;  echo $c; 獲得 "result: 23" 。

另外請注意：　$a = "result: "; $c = $a." ".23; 是不被允許的。字面數值無法當作字串使用。  

# 控制流程 control flow

控制流程結構有三種（所有的程式語言都相同）

循序結構

判斷結構

迴圈結構

大部分都跟 C++ 相同，我們就不重複了。

 if ... else if ... else ... 可以簡寫成 if ... elseif ... else ...。

迴圈結構多了 foreach 可以用，主要針對陣列，我們到陣列單元再介紹。

# 陣列

### 陣列基本

php 的陣列設計為 associative array ，也就是每個元素都有一個 key 及 value 。

當我們不特別指定 key 時，php 會自動幫我們產生索引當作 key 。我們先看自動產生的情況：

```php
$arr = [];  // 空的陣列
$arr1 = [1, 2, 5, 7, 9]; //
$arr2 = [1, 3, 'apple', 'cherry']; //
echo (count($arr1));  // ５, 使用 sizeof($arr1) 是一樣的。
echo ($arr1[1]);　// 2
echo ($arr2[3]); // cherry
```

$arr1 的key 自動會是 0, 1, 2, 3, 4 。$arr2 的 key 會是 0, 1, 2, 3。

我們可以直接給　key-value：

```php
$arr1[10] = 60;
$arr1['seven'] = "聯大資管";
$arr2['one'=>123, 'two'=333, 3='cherry'];
$arr3 = array('one'=>'apple', 2=>'banana', 3=>100);
```

要針對陣列每個元素進行動作，比較方便的方式是使用 foreach，只針對 value 時:

```php
php > foreach( $arr3 as $e) echo ($e.' ');
apple banana 100
php >      
```

但是若要取得 key ，那麼就要使用 $k=>$v 的語法：

```php
php > foreach( $arr3 as $i=>$e)　echo ($i."=>".$e.' ');
one=>apple 2=>banana 3=>100
php >     
```

php 這樣的陣列設計有許多方便之處，我們可以當作 C++ 的陣列來用，也可以當作　key-value 的list 來看。

練習：

1. 令 $arr 存放  i * 7 / 5 + 32 ， i = 0 ~ 100 的 101 個值，用迴圈列印這些值。
2. 令$brr 存放亞洲國家及對應的首都，例如　'中國'=>'北京'。陣列包含 中、日、韓、臺、越、泰。用迴圈列印這些值。
3. 令$crr 存放亞洲各國的人口數，請用迴圈檢驗找出最多人口的國家名稱以及人口數。

請同學將程式碼寫成　.php 檔案，使用 php filename.php 來執行。

> 補充：在 console mode 下，我們可以使用 $value = readline("請輸入數值：　"); 來取得鍵盤輸入值。

### 陣列內建存取函式

對於一個php陣列，有一組指令可以方便我們讀取元素： current($arr), next(), prev(), end(), reset();

```php
php > echo current($arr3);　 // 也可以使用 pos($arr3)
apple
php > echo current($arr3);
apple
php > next($arr3);　// 下一個
php > echo current($arr3);
banana
php > prev($arr3); echo current($arr3);　// 前一個
apple
php > end($arr3); echo current($arr3);　// 最後一個
100
php > reset($arr3); echo current($arr3);　// 最前一個
apple
php >        
```

要刪除一個元素，可以使用 unset 直接以 key 刪掉元素：

```php
unset($arr3['one']); // 刪掉　key='one' 的元素
unset($arr3[2]); 　　// 刪掉　key=2 的元素
$arr[1] = 99;  　　// 新增一個元素
if( in_array(100, $arr3)  ) echo "100 is in array 3"; // 檢查是否陣列內有某值
```

### 陣列內建函式

　　比較常用的我作了翻譯註解。

- [array_change_key_case](https://www.php.net/manual/en/function.array-change-key-case.php) — Changes the case of all keys in an array

- [array_chunk](https://www.php.net/manual/en/function.array-chunk.php) — Split an array into chunks

- [array_column](https://www.php.net/manual/en/function.array-column.php) — Return the values from a single column in the input array

- [array_combine](https://www.php.net/manual/en/function.array-combine.php) — **由一個key的array 跟一個 value 的array合成一個新的陣列**

- [array_count_values](https://www.php.net/manual/en/function.array-count-values.php) — Counts all the values of an array

- [array_diff_assoc](https://www.php.net/manual/en/function.array-diff-assoc.php) — Computes the difference of arrays with additional index check

- [array_diff_key](https://www.php.net/manual/en/function.array-diff-key.php) — Computes the difference of arrays using keys for comparison

- [array_diff_uassoc](https://www.php.net/manual/en/function.array-diff-uassoc.php) — Computes the difference of arrays with additional index check which is performed by a user supplied callback function

- [array_diff_ukey](https://www.php.net/manual/en/function.array-diff-ukey.php) — Computes the difference of arrays using a callback function on the keys for comparison

- [array_diff](https://www.php.net/manual/en/function.array-diff.php) — **計算二個陣列的差**

- [array_fill_keys](https://www.php.net/manual/en/function.array-fill-keys.php) — Fill an array with values, specifying keys

- [array_fill](https://www.php.net/manual/en/function.array-fill.php) — Fill an array with values, 返回一個陣列： array_fill(0, 5, 'value');

- [array_filter](https://www.php.net/manual/en/function.array-filter.php) — Filters elements of an array using a callback function

- [array_flip](https://www.php.net/manual/en/function.array-flip.php) — **將每一個元素的 key 與 value 對調**

- [array_intersect_assoc](https://www.php.net/manual/en/function.array-intersect-assoc.php) — Computes the intersection of arrays with additional index check

- [array_intersect_key](https://www.php.net/manual/en/function.array-intersect-key.php) — Computes the intersection of arrays using keys for comparison

- [array_intersect_uassoc](https://www.php.net/manual/en/function.array-intersect-uassoc.php) — Computes the intersection of arrays with additional index check, compares indexes by a callback function

- [array_intersect_ukey](https://www.php.net/manual/en/function.array-intersect-ukey.php) — Computes the intersection of arrays using a callback function on the keys for comparison

- [array_intersect](https://www.php.net/manual/en/function.array-intersect.php) — **二個陣列的交集**

- [array_key_exists](https://www.php.net/manual/en/function.array-key-exists.php) — **陣列是否有這個 key** 

- [array_key_first](https://www.php.net/manual/en/function.array-key-first.php) — Gets the first key of an array

- [array_key_last](https://www.php.net/manual/en/function.array-key-last.php) — Gets the last key of an array

- [array_keys](https://www.php.net/manual/en/function.array-keys.php) — Return all the keys or a subset of the keys of an array

- [array_map](https://www.php.net/manual/en/function.array-map.php) — Applies the callback to the elements of the given arrays

- [array_merge_recursive](https://www.php.net/manual/en/function.array-merge-recursive.php) — Merge one or more arrays recursively

- [array_merge](https://www.php.net/manual/en/function.array-merge.php) — **合併二個陣列，key 相同的只留後一個的。沒有key的從0開始自動編排。**

- [array_multisort](https://www.php.net/manual/en/function.array-multisort.php) — Sort multiple or multi-dimensional arrays

- [array_pad](https://www.php.net/manual/en/function.array-pad.php) — Pad array to the specified length with a value

- [array_pop](https://www.php.net/manual/en/function.array-pop.php) — **從尾端取出一個，刪除**

- [array_product](https://www.php.net/manual/en/function.array-product.php) — Calculate the product of values in an array

- [array_push](https://www.php.net/manual/en/function.array-push.php) — **從尾端加入一個或多個，新增**

- [array_rand](https://www.php.net/manual/en/function.array-rand.php) — Pick one or more random keys out of an array

- [array_reduce](https://www.php.net/manual/en/function.array-reduce.php) — Iteratively reduce the array to a single value using a callback function

- [array_replace_recursive](https://www.php.net/manual/en/function.array-replace-recursive.php) — Replaces elements from passed arrays into the first array recursively

- [array_replace](https://www.php.net/manual/en/function.array-replace.php) — Replaces elements from passed arrays into the first array

- [array_reverse](https://www.php.net/manual/en/function.array-reverse.php) — **反轉陣列順序**

- [array_search](https://www.php.net/manual/en/function.array-search.php) — **搜尋**

- [array_shift](https://www.php.net/manual/en/function.array-shift.php) — **刪除最前面1個元素**

- [array_unshift](https://www.php.net/manual/en/function.array-unshift.php) — **在前面加入1個或多個元素**

- [array_slice](https://www.php.net/manual/en/function.array-slice.php) — **取出陣列片段**

- [array_splice](https://www.php.net/manual/en/function.array-splice.php) — **取代掉陣列片段，這個指令功能較強也好用。**

  ```php
  $input = array("red", "green", "blue", "yellow");
  array_splice($input, 2);
  // 從第 2 個之後被　空　取代。　結果　["red", "green"]
  
  $input = array("red", "green", "blue", "yellow");
  array_splice($input, 1, -1);
  // 從第1個到倒數第1個會被　空　取代，　結果 ["red", "yellow"]
  
  $input = array("red", "green", "blue", "yellow");
  array_splice($input, 1, count($input), "orange");
  // 從第1個開始 4 個，被　orange 取代，結果　["red", "orange"]
  
  $input = array("red", "green", "blue", "yellow");
  array_splice($input, -1, 1, array("black", "maroon"));
  // 從倒數第1 個，一個被陣列取代，結果 ["red", "green", "blue", "black", "maroon"]
  ```

- [array_sum](https://www.php.net/manual/en/function.array-sum.php) — Calculate the sum of values in an array

- [array_udiff_assoc](https://www.php.net/manual/en/function.array-udiff-assoc.php) — Computes the difference of arrays with additional index check, compares data by a callback function

- [array_udiff_uassoc](https://www.php.net/manual/en/function.array-udiff-uassoc.php) — Computes the difference of arrays with additional index check, compares data and indexes by a callback function

- [array_udiff](https://www.php.net/manual/en/function.array-udiff.php) — Computes the difference of arrays by using a callback function for data comparison

- [array_uintersect_assoc](https://www.php.net/manual/en/function.array-uintersect-assoc.php) — Computes the intersection of arrays with additional index check, compares data by a callback function

- [array_uintersect_uassoc](https://www.php.net/manual/en/function.array-uintersect-uassoc.php) — Computes the intersection of arrays with additional index check, compares data and indexes by separate callback functions

- [array_uintersect](https://www.php.net/manual/en/function.array-uintersect.php) — Computes the intersection of arrays, compares data by a callback function

- [array_unique](https://www.php.net/manual/en/function.array-unique.php) — Removes duplicate values from an array

- [array_values](https://www.php.net/manual/en/function.array-values.php) — Return all the values of an array

- [array_walk_recursive](https://www.php.net/manual/en/function.array-walk-recursive.php) — Apply a user function recursively to every member of an array

- [array_walk](https://www.php.net/manual/en/function.array-walk.php) — Apply a user supplied function to every member of an array

- [array](https://www.php.net/manual/en/function.array.php) — Create an array

- 

- [compact](https://www.php.net/manual/en/function.compact.php) — Create array containing variables and their values

- [count](https://www.php.net/manual/en/function.count.php) — Count all elements in an array, or something in an object

- ~~[each](https://www.php.net/manual/en/function.each.php) — Return the current key and value pair from an array and advance the array cursor~~

- [extract](https://www.php.net/manual/en/function.extract.php) — Import variables into the current symbol table from an array

- [in_array](https://www.php.net/manual/en/function.in-array.php) — **檢查陣列是否有這個 value** 

- [key_exists](https://www.php.net/manual/en/function.key-exists.php) — Alias of array_key_exists

- [key](https://www.php.net/manual/en/function.key.php) — Fetch a key from an array

- [list](https://www.php.net/manual/en/function.list.php) — Assign variables as if they were an array

- [range](https://www.php.net/manual/en/function.range.php) — Create an array containing a range of elements, python 有類似的指令

  range(0,10) 取得 [0,1,2,3,4,5,6,7,8,9]。range(0,30, 10) 取得 [0,10,20,30]。

- [shuffle](https://www.php.net/manual/en/function.shuffle.php) — Shuffle an array　**弄亂**。這個有時很好用。

  Sort  相關函式

- [sort](https://www.php.net/manual/en/function.sort.php) — Sort an array　**排序**

- [rsort](https://www.php.net/manual/en/function.rsort.php) — Sort an array in reverse order

- [uasort](https://www.php.net/manual/en/function.uasort.php) — Sort an array with a user-defined comparison function and maintain index association

- [uksort](https://www.php.net/manual/en/function.uksort.php) — Sort an array by keys using a user-defined comparison function

- [usort](https://www.php.net/manual/en/function.usort.php) — Sort an array by values using a user-defined comparison function

- [krsort](https://www.php.net/manual/en/function.krsort.php) — Sort an array by key in reverse order
- [ksort](https://www.php.net/manual/en/function.ksort.php) — Sort an array by key
- [natcasesort](https://www.php.net/manual/en/function.natcasesort.php) — Sort an array using a case insensitive "natural order" algorithm
- [natsort](https://www.php.net/manual/en/function.natsort.php) — Sort an array using a "natural order" algorithm
- [arsort](https://www.php.net/manual/en/function.arsort.php) — Sort an array in reverse order and maintain index association
- [asort](https://www.php.net/manual/en/function.asort.php) — Sort an array and maintain index association 索引不變

# 函式

所有的程式語言都會有函式，也都提供了函式的定義方式。我們依照幾個主題介紹函式：

1. 定義函式
2. 參數以及參數的傳遞
3. 靜態變數　static　

首先看如何定義一個函式：

```php
function funName( $x, $y){
    $local = 0;
    $local += $x; $local *= $y;
    return $local;
}
```

我們看到參數不需要指定型別。參數的傳遞基本上都是 call by value 。若要 call by reference 則須在參數列上的參數名前加上 & 符號。

```php
function swap( &$a, &$b){
   $tmp = $a; $a = $b; $b = $tmp;
}
```

參數可以有預設值，與C++類似寫法：

```php
function drink($kind = 'tea'){
    echo "Have a ".$kind;
}
```

陣列傳遞，傳遞的是陣列的 reference, 因此就算是 call by value，在函式內還是可以更動到陣列內容。

### 不定長度的參數列

PHP 可以有不定長度的參數列，宣告時不指定參數，執行時藉由內建的函式取得引數陣列。

- 被呼叫時引數有幾個

  func_num_args()

- 第 n 個引數

  func_get_arg(*n*) 

- 以陣列取得所有引數

  func_get_args() 

```php
04:      function tour()
05:      {
06:        if (func_num_args() == 0)
07:          echo '沒有指定地點！';
08:        else
09:          for($i = 0; $i < func_num_args(); $i++) echo func_get_arg($i).'<BR>';
10:      }
11:      tour('台北', '台中', '高雄');

```

可使用的函式庫有些需要在 php.ini 中打開。php的支援充沛，完整的程式庫參考說明可以參考[php Function Reference](https://www.php.net/manual/en/funcref.php)。

我們之前看過了 [Arrays](https://www.php.net/manual/en/book.array.php) 。接著先介紹常用的三個： [Math](https://www.php.net/manual/en/book.math.php), [Date/Time](https://www.php.net/manual/en/book.datetime.php), [Strings](https://www.php.net/manual/en/book.strings.php)。其餘之後我們還會用到關於 file system, mail, gd2, exif 等。

## Math

數學常用的常數，[predefined constants](https://www.php.net/manual/en/math.constants.php)，部分如下

| Constant       | Value                  | Description | Availability |
| :------------- | :--------------------- | :---------- | :----------- |
| **`M_PI`**     | 3.14159265358979323846 | Pi          |              |
| **`M_E`**      | 2.7182818284590452354  | e           |              |
| **`M_LOG2E`**  | 1.4426950408889634074  | log_2 e     |              |
| **`M_LOG10E`** | 0.43429448190325182765 | log_10 e    |              |
| **`M_LN2`**    | 0.69314718055994530942 | log_e 2     |              |
| **`M_LN10`**   | 2.30258509299404568402 | log_e 10    |              |
| **`M_PI_2`**   | 1.57079632679489661923 | pi/2        |              |
| **`M_PI_4`**   | 0.78539816339744830962 | pi/4        |              |

常用數學函式

- [abs](https://www.php.net/manual/en/function.abs.php) — Absolute value
- [acos](https://www.php.net/manual/en/function.acos.php) — Arc cosine
- [acosh](https://www.php.net/manual/en/function.acosh.php) — Inverse hyperbolic cosine
- [asin](https://www.php.net/manual/en/function.asin.php) — Arc sine
- [asinh](https://www.php.net/manual/en/function.asinh.php) — Inverse hyperbolic sine
- [atan2](https://www.php.net/manual/en/function.atan2.php) — Arc tangent of two variables
- [atan](https://www.php.net/manual/en/function.atan.php) — Arc tangent
- [atanh](https://www.php.net/manual/en/function.atanh.php) — Inverse hyperbolic tangent
- [base_convert](https://www.php.net/manual/en/function.base-convert.php) — Convert a number between arbitrary bases
- [bindec](https://www.php.net/manual/en/function.bindec.php) — Binary to decimal
- [ceil](https://www.php.net/manual/en/function.ceil.php) — Round fractions up
- [cos](https://www.php.net/manual/en/function.cos.php) — Cosine
- [cosh](https://www.php.net/manual/en/function.cosh.php) — Hyperbolic cosine
- [decbin](https://www.php.net/manual/en/function.decbin.php) — Decimal to binary
- [dechex](https://www.php.net/manual/en/function.dechex.php) — Decimal to hexadecimal
- [decoct](https://www.php.net/manual/en/function.decoct.php) — Decimal to octal
- [deg2rad](https://www.php.net/manual/en/function.deg2rad.php) — Converts the number in degrees to the radian equivalent
- [exp](https://www.php.net/manual/en/function.exp.php) — Calculates the exponent of e
- [expm1](https://www.php.net/manual/en/function.expm1.php) — Returns exp(number) - 1, computed in a way that is accurate even when the value of number is close to zero
- [floor](https://www.php.net/manual/en/function.floor.php) — Round fractions down
- [fmod](https://www.php.net/manual/en/function.fmod.php) — Returns the floating point remainder (modulo) of the division of the arguments
- [getrandmax](https://www.php.net/manual/en/function.getrandmax.php) — Show largest possible random value
- [hexdec](https://www.php.net/manual/en/function.hexdec.php) — Hexadecimal to decimal
- [hypot](https://www.php.net/manual/en/function.hypot.php) — Calculate the length of the hypotenuse of a right-angle triangle
- [intdiv](https://www.php.net/manual/en/function.intdiv.php) — Integer division
- [is_finite](https://www.php.net/manual/en/function.is-finite.php) — Finds whether a value is a legal finite number
- [is_infinite](https://www.php.net/manual/en/function.is-infinite.php) — Finds whether a value is infinite
- [is_nan](https://www.php.net/manual/en/function.is-nan.php) — Finds whether a value is not a number
- [lcg_value](https://www.php.net/manual/en/function.lcg-value.php) — Combined linear congruential generator
- [log10](https://www.php.net/manual/en/function.log10.php) — Base-10 logarithm
- [log1p](https://www.php.net/manual/en/function.log1p.php) — Returns log(1 + number), computed in a way that is accurate even when the value of number is close to zero
- [log](https://www.php.net/manual/en/function.log.php) — Natural logarithm
- [max](https://www.php.net/manual/en/function.max.php) — Find highest value
- [min](https://www.php.net/manual/en/function.min.php) — Find lowest value
- [mt_getrandmax](https://www.php.net/manual/en/function.mt-getrandmax.php) — Show largest possible random value
- [mt_rand](https://www.php.net/manual/en/function.mt-rand.php) — Generate a random value via the Mersenne Twister Random Number Generator
- [mt_srand](https://www.php.net/manual/en/function.mt-srand.php) — Seeds the Mersenne Twister Random Number Generator
- [octdec](https://www.php.net/manual/en/function.octdec.php) — Octal to decimal
- [pi](https://www.php.net/manual/en/function.pi.php) — Get value of pi
- [pow](https://www.php.net/manual/en/function.pow.php) — Exponential expression
- [rad2deg](https://www.php.net/manual/en/function.rad2deg.php) — Converts the radian number to the equivalent number in degrees
- [rand](https://www.php.net/manual/en/function.rand.php) — Generate a random integer
- [round](https://www.php.net/manual/en/function.round.php) — Rounds a float
- [sin](https://www.php.net/manual/en/function.sin.php) — Sine
- [sinh](https://www.php.net/manual/en/function.sinh.php) — Hyperbolic sine
- [sqrt](https://www.php.net/manual/en/function.sqrt.php) — Square root
- [srand](https://www.php.net/manual/en/function.srand.php) — Seed the random number generator
- [tan](https://www.php.net/manual/en/function.tan.php) — Tangent
- [tanh](https://www.php.net/manual/en/function.tanh.php) — Hyperbolic tangent

## 日期時間

[Date/Time](https://www.php.net/manual/en/ref.datetime.php)

大部分程式語言處理日期時間的函式都是以 timestamp 為基礎的，大約都是以 1900, 1,1 0:0:0 開始，以1/1000 秒為單位。我們可以用 getdate() 來取得目前的時間。至於這個時間是指哪一個時區？我們要的是日期還是時間？年，月，日，周次，格式？　這些都是透過函式來計算的。（當然你也可以自己寫函式來計算。)

getdate() 算是最好用的函式，返回一個陣列，我們可以依照需求，從陣列中取得所需的資料。

```php
php > $now = getdate();
php > print_r($now);
Array
(
    [seconds] => 58
    [minutes] => 26
    [hours] => 10
    [mday] => 22
    [wday] => 6
    [mon] => 2
    [year] => 2020
    [yday] => 52
    [weekday] => Saturday
    [month] => February
    [0] => 1582367218
)
php >   
```

time() 與 getdate()[0] 式相同的。

在用到時間函式的網頁中，通常會需要先設定 default time zone. 指令如下：

小註解：GMT　格林威治時間　UTC 協調世界時間，目前多以 UTC為國際標準。台灣的時間 UTC+8。

```php
date_default_timezone_set('UTC');　 // UTC 協調世界時　國際標準時間　台灣 +8
date_default_timezone_set('Asia/Taipei');　
```

#### mktime

這是一個由　年月日時分秒計算 Unix timestamp的函式。參數給 時分秒月日年，依序6個參數。e.g. mktime(19,51,0,2,22,2022);

timestamp 轉時分秒月日年，可利用 getdate(ts)取得。

#### date 

這是取得格式化日期字串的函式，給一個 timestamp, 獲得一個符合所需的日期字串。格式的設定式一個字串，利用規範的字元來進行，字元格式如下：

| `format` character  | Description                                                  | Example returned values                                      |
| :------------------ | :----------------------------------------------------------- | :----------------------------------------------------------- |
| *Day*               | ---                                                          | ---                                                          |
| *d*                 | Day of the month, 2 digits with leading zeros                | *01* to *31*                                                 |
| *D*                 | **A textual representation of a day, three letters**         | *Mon* through *Sun*                                          |
| *j*                 | **Day of the month without leading zeros**                   | *1* to *31*                                                  |
| *l* (lowercase 'L') | A full textual representation of the day of the week         | *Sunday* through *Saturday*                                  |
| *N*                 | ISO-8601 numeric representation of the day of the week (added in PHP 5.1.0) | *1* (for Monday) through *7* (for Sunday)                    |
| *S*                 | English ordinal suffix for the day of the month, 2 characters | *st*, *nd*, *rd* or *th*. Works well with *j*                |
| *w*                 | Numeric representation of the day of the week                | *0* (for Sunday) through *6* (for Saturday)                  |
| *z*                 | The day of the year (starting from 0)                        | *0* through *365*                                            |
| *Week*              | ---                                                          | ---                                                          |
| *W*                 | ISO-8601 week number of year, weeks starting on Monday       | Example: *42* (the 42nd week in the year)                    |
| *Month*             | ---                                                          | ---                                                          |
| *F*                 | A full textual representation of a month, such as January or March | *January* through *December*                                 |
| *m*                 | Numeric representation of a month, with leading zeros        | *01* through *12*                                            |
| *M*                 | A short textual representation of a month, three letters     | *Jan* through *Dec*                                          |
| *n*                 | Numeric representation of a month, without leading zeros     | *1* through *12*                                             |
| *t*                 | Number of days in the given month                            | *28* through *31*                                            |
| *Year*              | ---                                                          | ---                                                          |
| *L*                 | Whether it's a leap year                                     | *1* if it is a leap year, *0* otherwise.                     |
| *o*                 | ISO-8601 week-numbering year. This has the same value as *Y*, except that if the ISO week number (*W*) belongs to the previous or next year, that year is used instead. (added in PHP 5.1.0) | Examples: *1999* or *2003*                                   |
| *Y*                 | **A full numeric representation of a year, 4 digits**        | Examples: *1999* or *2003*                                   |
| *y*                 | A two digit representation of a year                         | Examples: *99* or *03*                                       |
| *Time*              | ---                                                          | ---                                                          |
| *a*                 | Lowercase Ante meridiem and Post meridiem                    | *am* or *pm*                                                 |
| *A*                 | **Uppercase Ante meridiem and Post meridiem**                | *AM* or *PM*                                                 |
| *B*                 | Swatch Internet time                                         | *000* through *999*                                          |
| *g*                 | 12-hour format of an hour without leading zeros              | *1* through *12*                                             |
| *G*                 | 24-hour format of an hour without leading zeros              | *0* through *23*                                             |
| *h*                 | **12-hour format of an hour with leading zeros**             | *01* through *12*                                            |
| *H*                 | **24-hour format of an hour with leading zeros**             | *00* through *23*                                            |
| *i*                 | **Minutes with leading zeros**                               | *00* to *59*                                                 |
| *s*                 | **Seconds with leading zeros**                               | *00* through *59*                                            |
| *u*                 | Microseconds (added in PHP 5.2.2). Note that **date()** will always generate *000000* since it takes an [integer](https://www.php.net/manual/en/language.types.integer.php) parameter, whereas [DateTime::format()](https://www.php.net/manual/en/datetime.format.php) does support microseconds if [DateTime](https://www.php.net/manual/en/class.datetime.php) was created with microseconds. | Example: *654321*                                            |
| *v*                 | Milliseconds (added in PHP 7.0.0). Same note applies as for *u*. | Example: *654*                                               |
| *Timezone*          | ---                                                          | ---                                                          |
| *e*                 | Timezone identifier (added in PHP 5.1.0)                     | Examples: *UTC*, *GMT*, *Atlantic/Azores*                    |
| *I* (capital i)     | Whether or not the date is in daylight saving time           | *1* if Daylight Saving Time, *0* otherwise.                  |
| *O*                 | Difference to Greenwich time (GMT) without colon between hours and minutes | Example: *+0200*                                             |
| *P*                 | Difference to Greenwich time (GMT) with colon between hours and minutes (added in PHP 5.1.3) | Example: *+02:00*                                            |
| *T*                 | Timezone abbreviation                                        | Examples: *EST*, *MDT* ...                                   |
| *Z*                 | Timezone offset in seconds. The offset for timezones west of UTC is always negative, and for those east of UTC is always positive. | *-43200* through *50400*                                     |
| *Full Date/Time*    | ---                                                          | ---                                                          |
| *c*                 | ISO 8601 date (added in PHP 5)                               | 2004-02-12T15:19:21+00:00                                    |
| *r*                 | [» RFC 2822](http://www.faqs.org/rfcs/rfc2822) formatted date | Example: *Thu, 21 Dec 2000 16:01:07 +0200*                   |
| *U*                 | Seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)   | See also [time()](https://www.php.net/manual/en/function.time.php) |

```
date('D, d m Y H:m:s', time());  //  Saturday, 22 2 2020 19:55:10
```

#### strtotime

給一個字串，算出對應時間的 timestamp。

```php
echo strtotime("now"), "\n";　　　　// 目前
echo strtotime("10 September 2000"), "\n";　　// 日月年
echo strtotime("+1 day"), "\n";　// 1天後
echo strtotime("+1 week"), "\n";　　// 一周後
echo strtotime("+1 week 2 days 4 hours 2 seconds"), "\n";　　// 一周2天4小時2秒之後
echo strtotime("next Thursday"), "\n";　　// 下周二
echo strtotime("last Monday"), "\n";　　// 上周一
```

實務應用時，會用 strtotime 將某個時間的 timestamp 抓出來，再丟給 date() 轉成所需的日期時間格式。

## String

字串函式不論哪一個程式語言都很豐富，也就是不好記得全部，比較需要的是

1. 字串比較，看看二個字串是否相同 
2. 尋找子字串，
3. 一個字串用逗點或Tab 斷開成字串陣列
4. 將一個字串陣列串成一個陣列
5. 子字串
6. 在HTML 應用時，將換行符號轉為 \<br>
7. 取得字串長度

```php
strcmp($str1, $str2);
strpos($strs, $patt);
stripos(), strrpos(), strripos() 不計大小寫　反向　反向不計大小
implode('-', $arr);  // 合併成一個字串
explode(',', $str); // 以 , 將 $str 分割成陣列。
strlen($str); // 字串長度
substr($str, 7, 5);  // 第 7 個字元開始，取 5 個字元形成字串
nl2br($str);  // 網頁最常用到
```

其餘的部分，需要時再查。[字串函式](https://www.php.net/manual/en/book.strings.php)



# 異常處理

與 C++ 類似。

```php
try{
  open_folder("c:\\wamp\\t1.txt");
  throw new Exception('拋出一個異常訊息');
}catch( $e ){
  echo $e->getMessage();
}
```

