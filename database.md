---
ptitle: database
date: 2020-03-19 17:20:44
tags:
- database
- mariaDB
- msqli
categories:
- database
---

# php Access MariaDB

原則上 Wamp 安裝時包含了 Apache, MariaDB 以及 PHP 。另外也有一個存取資料庫的介面程式 PhpMyadmin 這是 php 撰寫的，再網站開啟後，從首頁點擊進入。

第一次進入要記得刪掉匿名帳號(沒有名字的帳號)，再用 root 進入(預設沒有密碼，也請不要另設密碼！)　請建立一個自己學號的帳戶以及同名的資料庫，給這個資料庫所有權限。

登出後，用學號帳號進入，你的資料庫名稱會是 u0533001 (若帳戶是 U0533001)

有關 MariaDB 帳號的設定，請參考前面的軟體安裝說明。

下面的範例我們使用的帳號是 U0533001 密碼是 u0533001。你們自己的帳號密碼就請自己設定。

PHP 連接資料庫，首先你必須要有一個資料庫的帳號(U0533001)，要知道你的資料庫的名稱(u0533001)以及密碼(u0533001)。

建立連接的指令：

```
mysqli_connect([string host [, string username [, string password [,string dbname]]]]);
```

關閉連接指令：

```
mysqli_close([resource link_identifier]);
```

先看一個基本範例：

```php
$link = mysqli_connect("localhost", "U0533001", "u0533001") 
                       or die("無法建立連接");
echo "成功建立連接<br/>";
	  // 預設字集名稱
echo mysqli_character_set_name($link)."<br/>";
	  //用戶端函式庫的版本資訊
echo mysqli_get_client_info()."<br/>";
	  //主機的相關資訊
echo mysqli_get_host_info($link)."<br/>";
	//	資料庫伺服器的版本資訊
echo mysqli_get_server_info($link)."<br/>";
	//	資料庫伺服器的錯誤訊息
echo	mysqli_errno($link)."<br/>";
echo mysqli_error($link)."<br/>";

mysqli_close( $link );
```

正常的回覆如下：

```
成功建立連接
latin1
mysqlnd 5.0.12-dev - 20150407 - $Id: 7cc7cc96e675f6d72e5cf0f267f48e167c2abb23 $
localhost via TCP/IP
5.5.5-10.4.10-MariaDB
0
```

要存取資料庫就必須選用一個資料庫，然後進行各種查詢。

開始之前，我們先建立一個資料表　price，你可以用 phpmyadmin 進入你的資料庫，然後直接執行下列 sql 指令：

```
USE `u0533001`;
-- --------------------------------------------------------
--
-- 資料表格式： `price`
--

CREATE TABLE IF NOT EXISTS `price` (
  `no` int(10) unsigned NOT NULL DEFAULT '0',
  `category` varchar(20) NOT NULL DEFAULT '',
  `brand` varchar(20) NOT NULL DEFAULT '',
  `specification` varchar(100) NOT NULL DEFAULT '',
  `price` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `url` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 列出以下資料庫的數據： `price`
--

INSERT INTO `price` (`no`, `category`, `brand`, `specification`, `price`, `date`, `url`) VALUES
(301, '主機板', '華碩', 'P5Q Premium', 8500, '2009-02-01', 'tw.asus.com'),
(302, '主機板', '微星', 'P45 Platinum', 6900, '2009-02-02', 'www.msi.com.tw'),
(303, '主機板', '技嘉', 'EX48-DQ6 ', 9400, '2009-02-05', 'www.gigabyte.tw'),
(304, '主機板', '微星', 'P43 Neo', 2700, '2009-02-06', 'www.msi.com.tw'),
(305, '主機板', '華碩', 'M3N72-D', 4800, '2009-02-07', 'tw.asus.com'),
(306, '主機板', '微星', 'K9A2 Platinum', 6600, '2009-02-08', 'www.msi.com.tw'),
(307, '主機板', '技嘉', 'GigaByte GA-8KNXP', 7150, '2009-02-09', 'www.gigabyte.com.tw'),
(308, '主機板', '微星', 'X48C Platinum', 8750, '2009-02-04', 'www.msi.com.tw'),
(309, '主機板', '華碩', 'P5K-SE', 3050, '2009-02-03', 'tw.asus.com'),
(310, '主機板', '技嘉', 'EP45-DQ6', 9600, '2009-02-02', 'www.gigabyte.tw'),
(311, 'CPU', 'Intel', 'Core i7-965 3.2G', 34500, '2009-02-08', 'www.intel.com.tw'),
(312, 'CPU', 'Intel', 'Core2 Duo E2180 2.0G', 2000, '2009-02-02', 'www.intel.com.tw'),
(313, 'CPU', 'Intel', 'Core2 Duo E8400 3G', 5450, '2009-02-10', 'www.intel.com.tw'),
(314, 'CPU', 'AMD', 'AM2 Athlon64x2 5200', 1950, '2009-02-11', 'www.amdtaiwan.com.tw'),
(315, 'CPU', 'AMD', 'AM2 Phenom II X4 940', 8300, '2009-02-02', 'www.amdtaiwan.com.tw'),
(316, '記憶體', '創見', 'Transcend 512MB DDR-400', 3100, '2004-08-10', 'www.transcend.com.tw'),
(317, '記憶體', '勝創', 'Kingmax 512MB DDR-466', 3250, '2004-08-10', 'www.kingmax.com.tw'),
(318, '記憶體', '金士頓', 'Kingston 512MB DDR-400', 2950, '2004-08-15', 'www.kingston.com'),
(319, '記憶體', '創見', 'Transcend 512MB DDR-333', 3230, '2004-08-15', 'www.transcend.com.tw'),
(320, '記憶體', '勝創', 'Kingmax 512MB DDR-433', 2950, '2004-08-15', 'www.kingmax.com.tw'),
(321, '顯示卡', '華碩', 'A9800XT', 20275, '2004-08-01', 'www.asus.com.tw'),
(322, '顯示卡', '麗台', 'Quadro 4 NVS400/ 64M', 19445, '2004-08-01', 'www.leadtek.com.tw'),
(323, '顯示卡', '麗台', 'Leadtek WinFast A350 Ultra TDH', 17645, '2004-08-01', 'www.leadtek.com.tw'),
(324, '顯示卡', '華碩', 'ASUS AGP-V9520 Magic', 2030, '2004-08-01', 'www.asus.com.tw'),
(325, '硬碟', 'HITACHI', '500G/7200 轉/SATAII 300/16MB', 1700, '2009-02-13', 'www.hitachi.com.tw'),
(326, '硬碟', 'Seagate', '750G/7200 轉/SATAII 300/32MB', 3050, '2009-02-12', 'www.seagate.com'),
(327, '硬碟', 'Western Digital', '500G/7200 轉/SATAII 300/16MB', 1750, '2009-02-12', 'www.wdc.com'),
(328, '硬碟', 'Western Digital', '750G/7200 轉/SATAII 300/16MB', 2750, '2009-02-11', 'www.wdc.com'),
(329, '硬碟', 'Seagate', '1TB/7200 轉/SATAII 300/32MB', 3650, '2009-02-10', 'www.seagate.com'),
(330, '螢幕', 'ViewSonic', 'VG1921WM', 13300, '2009-02-17', 'www.viewsonic.com.tw'),
(331, '螢幕', 'EIZO', 'FlexScan SX3031', 92500, '2009-02-15', 'www.eizo.com.tw'),
(332, '螢幕', 'ViewSonic', 'VA1913wm', 3950, '2009-02-16', 'www.viewsonic.com.tw'),
(333, '螢幕', 'EIZO', 'FlexScan SX2761W', 57700, '2009-02-18', 'www.eizo.com.tw'),
(334, '印表機', '惠普', 'Photosmart C8180', 11100, '2009-02-12', 'www.hp.com.tw'),
(335, '印表機', 'Canon', 'IP4680', 3800, '2009-02-11', 'www.abico.com.tw'),
(336, '印表機', 'EPSON', 'C1100SE', 11500, '2009-02-10', 'www.epson.com.tw'),
(337, '掃描器', 'HP', 'Scanjet G3110', 3800, '2009-02-02', 'www.hp.com.tw'),
(338, '掃描器', 'Microtek', 'ScanMaker S430', 3900, '2009-02-01', 'www.microtek.com.tw'),
(339, '掃描器', 'EPSON', 'V500 PHOTO', 9300, '2009-02-03', 'www.epson.com.tw');
```

這是為了方便說明，你也可以自己建一個資料表，自己創建一些資料來練習。

匯入之後，你可以先瀏覽一番，對我們的範例資料表有一點概念。接著我們就來看如何存取這個資料表的內容：

```
<?php
$link = mysqli_connect("localhost", "U0533001", "u0533001") 
                       or die("無法建立連接");
echo "成功建立連接<br/>";
mysqli_query($link, "SET NAMES utf8");

$db_selected = mysqli_select_db($link, "u0533001")
       or die ("無法開啟 prodcut 資料庫<br>" . mysqli_error($link));

$sql = "SELECT * FROM price WHERE category = '主機板'";
$result = mysqli_query($link, $sql);
echo "category = 「主機板」 的記錄有 " . mysqli_num_rows($result) . " 筆";
echo "，包含 " . mysqli_num_fields($result) . " 個欄位。";
mysqli_close( $link );
?>
```

到目前，你應該好好檢視一下上面的程式碼，確認你都完全了解。

我們的基本框架上是：

1. 建立資料庫系統連結（帳密）
2. 設編碼　utf8
3. 選資料庫
4. 撰寫 sql 指令，存成一個字串。這個是執行關鍵！
5. 執行 sql 指令，結果指定給 $result, 我們可以將 $result 視為一個表！
6. 接下來就是從 $result 中取出我們需要的資訊，包括：
   1. 有幾筆
   2. 有幾欄
   3. ...



##### dbtools.inc.php

因為要連結資料庫總是需要相通的指令及帳號密碼資料，放在一個檔案中比較好管理，因此我們將剛才的程式中的連結部分複製出來，另外存成一個檔案，之後只要 require 進來，再改成執行create\_connect 以及 execute\_sql 就好了。

dbtools.inc.php

```
<?php
  function create_connection()
  {
    $link = mysqli_connect("localhost", "U0533001", "u0533001")
      or die("無法建立資料連接: " . mysqli_connect_error());
    mysqli_query($link, "SET NAMES utf8");
    return $link;
  }

  function execute_sql($link, $database, $sql)
  {
    mysqli_select_db($link, $database)
      or die("開啟資料庫失敗: " . mysqli_error($link));

    $result = mysqli_query($link, $sql);

    return $result;
  }
?>
```

有了上面的工具，剛才的程式碼可以改寫成：

```
<?php
	require_once("dbtools.inc.php");  // 含入工具檔案

	$link = create_connection();

    $sql = "SELECT * FROM price WHERE category = '主機板'";
    $result =execute_sql($link, "u0533001", $sql);

    echo "category = 「主機板」 的記錄有 " . mysqli_num_rows($result) . " 筆";
    echo "，包含 " . mysqli_num_fields($result) . " 個欄位。";

	mysqli_close( $link );
?>
```

在上面的程式碼， 第 2 行是含入其他 php 檔案的語法，若要用到別的程式庫，就需要這個指令。

介紹幾個指令：

```
mysqli_num_rows($result)     // 查詢所得筆數
mysqli_num_fields($result)   // 查詢所得欄位數

//查詢更動筆數 針對SELECT, INSERT, UPDATE, DELETE 
mysqli_affected_rows($result) 

// 查詢所得欄位資訊，欄位數從 0 起算，取得關聯陣列，包含 name 及 type
$meta = mysqli_fetch_field_direct($result, 1);
echo "欄位名稱：$meta->name <br>";
echo "資料型態：$meta->type <br>";
echo "資料長度：$meta->max-length <br>"

// 查詢所得欄位資訊，依序一個欄位一個欄位查詢
while( $meta = mysqli_fetch_field($result)){
    echo "欄位名稱：$meta->name <br>";
	echo "資料型態：$meta->type <br>";
	echo "資料長度：$meta->max-length <br>"
}
```

我們用下列程式碼來展示上述指令的使用範例：

```php
<?php
      require_once("dbtools.inc.php");
			
      $link = create_connection();
      $sql = "SELECT * FROM PRICE WHERE category = '主機板'";
      $result = execute_sql($link, "u0533001", $sql);
			
	  echo "<table width='400' border='1'><tr align='center'>";
      echo "<td>欄位名稱</td><td>資料型態</td><td>最大長度</td></tr>";

      $i = 0;
      while ($i < mysqli_num_fields($result))
      {
        $meta = mysqli_fetch_field($result);
        //$meta = mysqli_fetch_field_direct($result, $i);
        echo "<tr>";
        echo "<td>$meta->name</td>";
        echo "<td>$meta->type</td>";
        echo "<td>$meta->max_length</td>";
        echo "</tr>";
        $i++;
      }
      echo "</table>" ;			
      mysqli_close($link);
?> 
```

到目前為止，我們還沒有對資料表的內容進行存取，下面我們就要開始進行　新增　刪除　修改　查詢　這四個重要動作。

接下來的指令都是針對查詢回來的 result 進行的擷取動作：

```
直接讀取第 n 筆資料紀錄的第幾個欄位
mysqli_result(resource result, int row [, mixed field])
	result: 執行 query 之後的結果
	row: 	第幾筆資料紀錄
 	field: 第幾個欄位的值，有二種用法
		n: 第 n 個，e.g. mysql_result($result, 3, 2)
		fname: 欄位，e.g. mysql_result($result, 3, 'price')
		*省略時，表示第1個欄位

mysqli_db_name()
逐筆讀取整筆資料紀錄
mysqli_fetch_row()
mysqli_fetch_array()
mysqli_fetch_assoc()
mysqli_object()
```

我們分別用範例來說明。

範例1 使用 mysqli_fetch_row 進行擷取資料

```php
<!DOCTYPE html>
<head>
<meta charset="utf-8" />
</head>
<body>
<h1 align="CENTER">零組件報價表</h1>
    <?php
      require_once("dbtools.inc.php");
      $link = create_connection();
      $sql = "SELECT * FROM PRICE WHERE category = '主機板'";
      $result = execute_sql($link, "u0533001", $sql);

      echo "<table border='1' align='center'><tr align='center'>";
	for ($i = 0; $i < mysqli_num_fields($result); $i++)   // 顯示欄位名稱
        　echo "<td>" . mysqli_fetch_field_direct($result, $i)->name. "</td>";
	echo "</tr>";
	while( $row = mysqli_fetch_row($result) ){
    	echo "<tr>";
        for ($k = 0; $k < mysqli_num_fields($result); $k++)
          	echo "<td>" . $row[$k] . "</td>";
    	echo "</tr>";
    }      
	echo "</table>" ;
	mysqli_free_result($result);
	mysqli_close($link);
    ?>
</body>
```

請看到　mysqli\_fetch\_row 這一行，一次取一筆資料，變數　$row。　當最後一筆取出之後，$row 會是 null, 判斷上就會是 false。 while 迴圈就結束。

 while 迴圈中， for 迴圈用來依序取出每一個欄位資料 $row[$k] 。



範例 2 使用 mysql\_fetch\_array 進行擷取：

說明： array 取出的陣列，可以使用編號做索引( arr[0], arr[1], ...) 也可以使用欄位名稱做索引( arr['id'], arr['name'], ...) 方便寫程式的你熟悉甚麼用甚麼。

```php
    <?php
      require_once("dbtools.inc.php");
      $link = create_connection();
      $sql = "SELECT * FROM PRICE WHERE category = '主機板'";
      $result = execute_sql($link, "u0533001", $sql);
			
      echo "<table border='1' align='center'><tr align='center'>";
      for ($i = 0; $i < mysqli_num_fields($result); $i++)
        echo "<td>" . mysqli_fetch_field_direct($result, $i)->name. "</td>";
      echo "</tr>";
	
      while ($row = mysqli_fetch_array($result, MYSQLI_NUM))
			{
        echo "<tr>";			
        for($i = 0; $i < mysqli_num_fields($result); $i++)
          echo "<td>$row[$i]</td>";					

        echo "</tr>";				
      }
      echo "</table>" ;
      mysqli_free_result($result);
      mysqli_close($link);
    ?> 
```

範例 2.2 同樣是 mysqli_fetch_array

```php
	<?php
      require_once("dbtools.inc.php");
      $link = create_connection();
      $sql = "SELECT * FROM price WHERE category = '主機板'";
      $result = execute_sql($link, "u0533001", $sql);
			
      echo "<table border='1' align='center'><tr align='center'>";
      for ($i = 0; $i < mysqli_num_fields($result); $i++)
        echo "<td>" . mysqli_fetch_field_direct($result, $i)->name. "</td>";
      echo "</tr>";
	
      while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
      {
        echo "<tr>";			
        echo "<td>" . $row['no'] . "</td>";
        echo "<td>" . $row["category"] . "</td>";
        echo "<td>" . $row["brand"] . "</td>";
        echo "<td>" . $row["specification"] . "</td>";	
        echo "<td>" . $row["price"] . "</td>";
        echo "<td>" . $row["date"] . "</td>";	
        echo "<td>" . $row["url"] . "</td>";													
        echo "</tr>";				
      }
      echo "</table>" ;
      mysqli_free_result($result);
      mysqli_close($link);
?> 
```

範例 2.3 還是 mysqli_fetch_array 的使用。留意　$row[  ] 的使用。

```php
    <?php
      require_once("dbtools.inc.php");
      $link = create_connection();
      $sql = "SELECT * FROM price WHERE category = '主機板'";
      $result = execute_sql($link, "u0533001", $sql);
			
      echo "<table border='1' align='center'><tr align='center'>";
      for ($i = 0; $i < mysqli_num_fields($result); $i++)
        echo "<td>" . mysqli_fetch_field_direct($result, $i)->name. "</td>";
      echo "</tr>";
	
      while ($row = mysqli_fetch_array($result, MYSQLI_BOTH))
      {
        echo "<tr>";			
        echo "<td>" . $row["no"] . "</td>";
        echo "<td>" . $row[1] . "</td>";
        echo "<td>" . $row["brand"] . "</td>";
        echo "<td>" . $row["specification"] . "</td>";	
        echo "<td>" . $row[4] . "</td>";
        echo "<td>" . $row[5] . "</td>";	
        echo "<td>" . $row["url"] . "</td>";													
        echo "</tr>";				
      }
      echo "</table>" ;
      mysqli_free_result($result);
      mysqli_close($link);
    ?>
```



範例 3 使用 mysqli_fetch_assoc, 這個指令與 mysqli_fetch_array 的其中一個用法一模一樣，只是特別強調 associative array 的特性。請用 範例 2.2 更換指令試試看！



範例 4 使用 mysqli_fetch_object, 這個指令方便物件使用。

```php
    <?php
      require_once("dbtools.inc.php");
      $link = create_connection();
      $sql = "SELECT * FROM PRICE WHERE category = '主機板'";
      $result = execute_sql($link, "u0533001", $sql);
			
      echo "<table border='1' align='center'><tr align='center'>";
      for ($i = 0; $i < mysqli_num_fields($result); $i++)
        echo "<td>" . mysqli_fetch_field_direct($result, $i)->name. "</td>";
      echo "</tr>";
	
      while ($row = mysqli_fetch_object($result))
      {
        echo "<tr>";			
        echo "<td>$row->no</td>";
        echo "<td>$row->category</td>";
        echo "<td>$row->brand</td>";
        echo "<td>$row->specification</td>";	
        echo "<td>$row->price</td>";
        echo "<td>$row->date</td>";	
        echo "<td>$row->url</td>";	
        echo "</tr>";				
      }

      echo "</table>" ;
      mysqli_free_result($result);
      mysqli_close($link);
    ?>
```

範例 1 ~ 4 都是一次取一筆資料，只是對這一筆資料的擷取，提供使用陣列或是使用物件二種方式。



延伸說明：

<table ><td><span color="brown">php 的 物件與 assocative 陣列非常接近，在使用時雖然不可以任意互換，但大概都可以得到相同的功能。在轉換成 json 表示時， json_encode( objet|associative array ) 都可以。只是 json_decode( json ) 得到是會是一個 object 而不是一個 associative array 。</span></td></table>

$result 的游標概念：當我們讀取一筆一筆資料庫撈回來的 $result 時，有一個隱形的游標指在第一筆資料，取第一筆後，游標自動下移一筆，依此類推。

要跳著取資料，可以使用 mysqli_data_seek( $result, inx) 直接移到第 inx 筆。若接著讀取，就會是第 inx 筆資料。語法：

```
mysqli_data_seek(resource result, int row_number)
```



在介紹新增、刪除、修改之前，我們用一個範例來介紹如何對取得超過一個頁面的資料做分頁的呈現：

```php
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>分頁瀏覽</title>
  </head>
  <body>
    <?php
      require_once("dbtools.inc.php");

      //指定每頁顯示幾筆記錄
      $records_per_page = 5;

      //取得要顯示第幾頁的記錄
      if (isset($_GET["page"]))
        $page = $_GET["page"];
      else
        $page = 1;

      //建立資料連接
      $link = create_connection();

      //執行 SQL 命令
      $sql = "SELECT category AS '零組件種類', brand AS '品牌', specification AS
              '規格', price AS '價格', date AS '報價日期' FROM Price";
      $result = execute_sql($link, "u0533001", $sql);

      //取得欄位數
      $total_fields = mysqli_num_fields($result);

      //取得記錄數
      $total_records = mysqli_num_rows($result);

      //計算總頁數， ceil 天花板函式
      $total_pages = ceil($total_records / $records_per_page);

      //計算本頁第一筆記錄的序號
      $started_record = $records_per_page * ($page - 1);

      //將記錄指標移至本頁第一筆記錄的序號
      mysqli_data_seek($result, $started_record);

      //顯示欄位名稱
      echo "<table border='1' align='center' width='800'>";
      echo "<tr align='center'>";
      for ($i = 0; $i < $total_fields; $i++)
        echo "<td>" . mysqli_fetch_field_direct($result, $i)->name . "</td>";
      echo "</tr>";

      //顯示記錄
      $j = 1;
      while ($row = mysqli_fetch_row($result) and $j <= $records_per_page)
      {
        echo "<tr>";
        for($i = 0; $i < $total_fields; $i++)
          echo "<td>$row[$i]</td>";

        $j++;
        echo "</tr>";
      }
      echo "</table>" ;

      //產生導覽列
      echo "<p align='center'>";
      if ($page > 1)
        echo "<a href='show_record.php?page=". ($page - 1) . "'>上一頁</a> ";

      for ($i = 1; $i <= $total_pages; $i++)
      {
        if ($i == $page)
          echo "$i ";
        else
          echo "<a href='show_record.php?page=$i'>$i</a> ";
      }

      if ($page < $total_pages)
        echo "<a href='show_record.php?page=". ($page + 1) . "'>下一頁</a> ";

			echo "</p>";

      //釋放記憶體空間
      mysqli_free_result($result);
      mysqli_close($link);
    ?>
  </body>
</html>
```

請留意，本範例使用　GET 的方法來回應下一頁及上一頁。



現在，我們要繼續資料庫的另外三個主要指令：新增、刪除、修改。

SQL 語法：

```
新增
INSERT INTO tableName(fname1, fname2, …) VALUES('$v1', '$v2', …)
修改
UPDATE tableName SET fname1='$v1', fname2='$v2' … WHERE condition
刪除
DELETE FROME tabName WHERE condition
```

sql 指令透過 execute_sql($link, "u0533001", $sql); 來執行。這三種指令的 $result 可以用 mysqli_affected_rows($result) 來取得有多少筆資料更動了。

新增，我們用註冊帳號作為範例。

首先我們先建立一個 users 的資料表。這個資料表只需要二個欄位： account 以及 password 。型態可以設定為 varchar(20) 。

```
<!DOCTYPE html">
<html>
<head>
<meta charset="utf-8" />
<title>帳號註冊</title>
</head>
<body>
<form action="addUser.php" method="post">
帳號：<input name="account" type="text" value="輸入帳號" size="10" />　<br>
密碼：<input name="password" type="password" value="" size="10" /> <br>
<input type="submit" value="送出" size="8" />
</form>
</body>
</html>
```

addUser.php

```php
<?php
require_once("dbtools.inc.php");

  $a = $_POST["account"];
  $p = $_POST["password"];
  echo "after get the post".$a."p: ".$p."<br/>";

// 連結資料庫
  $link = create_connection() or die("connecting fails!");

	// 執行 query 並取得執行結果 
	$sql = "INSERT INTO users(account, passwd) VALUES ('$a', '$p');";  // 請使用雙引號
	$result = execute_sql($link, "u0533001", $sql);

  echo "insert ".$result." record(s) <br/>";
?>
```

範例　刪除一筆資料

```
<!DOCTYPE html">
<html>
<head>
<meta charset="utf-8" />
<title>帳號刪除</title>
</head>

<body>
<form action="deleteUser.php" method="post">
帳號：<input name="account" type="text" value="輸入帳號" size="10" />

密碼：<input name="password" type="password" value="" size="10" />

<input type="submit" value="送出" size="8" />
</form>

</body>
</html>

```

```php+HTML
<?php
require_once("dbtools.inc.php");

$a = $_POST["account"];
$p = $_POST["password"];
echo "after get the post".$a."p: ".$p."<br/>";

// 連結資料庫
 $link = create_connection() or die("connecting fails!");

// 執行 query 並取得執行結果 
$sql = "DELETE FROM users where account = '$a';";  // 請使用雙引號
$result = execute_sql($link, "u0533001", $sql);

echo "delete ".$result." record(s) <br/>";
?>

```

範例　修改一筆資料

```html
<!DOCTYPE html">
<html>
<head>
<meta charset="utf-8" />
<title>密碼更改</title>
</head>
<body>
<form action="updateUser.php" method="post">
帳號：<input name="account" type="text" value="更改密碼的帳號" size="10" />
密碼：<input name="password" type="password" value="" size="10" />
<input type="submit" value="送出" size="8" />
</form>
</body>
</html>
```

```php
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>php update</title>
</head>
<body>

<?php
require_once("dbtools.inc.php");

$a = $_POST["account"];
$p = $_POST["password"];
echo "after get the post".$a."p: ".$p."<br/>";

// 連結資料庫
$link = create_connection() or die("connecting fails!");

// 執行 query 並取得執行結果
$sql = "UPDATE users set passwd = '$p' where account = '$a';";  // 請使用雙引號
$result = execute_sql($link, "u0533000", $sql);

echo "update ".mysqli_affected_rows($link)." record(s) <br/>";
?>
</body>
</html>
```

在刪除資料時，比較合理的做法是列出目前的資料，為每一筆資料準備一個刪除的按鈕，點選之後就刪除這一筆資料。怎麼做呢？

首先，我們先實作可以顯示所有使用者名稱的程式：

```
require_once("dbtools.inc.php");

// 連結資料庫
 $link = create_connection() or die("connecting fails!");

// 執行 query 並取得執行結果
$sql = "SELECT * FROM users;";  // 請使用雙引號
$result = execute_sql($link, "u0533001", $sql);
while( $row = mysqli_fetch_array($result)){
    echo $row['account']."<br>";
}
```

接者改寫 while 迴圈內容：

```js
while( $row = mysqli_fetch_array($result)){
    $str = "<form action='deleteUser.php' method='post'>";
    $str .= "<input type='hidden' name='account' value='".$row['account']."'>";
    $str .= "<input type='hidden' name='password' value='".$row['password']."'>";
    $str .= $row['account']."---";
    $str .= "<input type='submit' value='x'> <br>";
    $str .="</form>";
    echo $str;
}
```

說明：我們為每一筆資料準備一個 form ，因為要使用 post 所以必須準備 input ，然而我們的資料是從資料庫來的，並不要使用者輸入，因此我們使用 hidden 。自己將資料填進去。顯示帳號，最後加上一個送出。

我們呼叫的是剛才已經寫好的 deleteUser.php 。

更新資料也應該是類似的：我們從資料表列中點選要更改的資料，點選後出現輸入欄，輸入後進行更改。

請設計一個管理帳號的網站，管理員根據介面可以新增、查看、刪除、更改帳號內容。



[mysqli說明及參考網站](https://www.php.net/manual/en/book.mysqli.php)　可以查到所有 mysqli 的指令。

本章介紹的指令有下列幾個：

mysqli_affected_rows()
mysqli_close()
mysqli_connect()
mysqli_data_seek()
mysqli_connect_errno()
mysqli_connect_error()
mysqli_errno()
mysqli_error()
mysqli_fetch_array()
mysqli_fetch_assoc()
mysqli_fetch_field()
mysqli_fetch_object()

mysqli_fetch_row()
mysqli_field_seek()
mysqli_free_result()
mysqli_get_client_info()
mysqli_get_host_info()
mysqli_get_proto_info()
mysqli_get_server_info()
mysqli_num_fields()
mysqli_num_rows()
mysqli_query()
mysqli_select_db()

附註：　php　的資料庫的主要指令沒有很多，最重要的當然就是執行 sql 。因此重點還是在 sql 指令要如何應用。雖然我們這門課不會強調　sql 語法，但在接下來的應用，我們仍需要多學習一些 sql 的實務應用。

