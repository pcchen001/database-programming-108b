---
title: 應用 投票與資料視覺化
date: 2020-05-12 11:22:28
tags:
- chart
- data visualization
categories:
- Application
---

# 資料視覺化

統計圖表用來將資料進行視覺化，好的視覺化需要設計互動及資訊重點的動畫設計。但我們從基礎的統計圖繪圖開始。

本應用包含了投票資料的資料庫處理。

我們設計二個資料表，一個用來處理選項，一個用來處理使用者。

選項管理由管理員進行新增、刪除、修改。使用者表格用來記錄使用者是否已經投過票了。課本的範例是以身分證字號作為認證，順便練習一下如何檢核一個有檢查碼的字號。( 身分認證的方式根據組織單位應用各有不同，本單元使用的範例僅為其中之一。

我們透過實作來介紹觀念、應用及邏輯。

首先建立二個資料表： candidate 及 id_number。

```
CREATE TABLE IF NOT EXISTS `candidate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `introduction` text NOT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;
```

```
CREATE TABLE IF NOT EXISTS `id_number` (
  `id` varchar(10) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

預設的資料可以如下：

```
INSERT INTO `candidate` (`id`, `name`, `introduction`, `score`) VALUES
(1, 'C++', '基礎泛用的基礎語言', 8051),
(2, 'Python', '人工智慧直譯語言', 7597),
(3, 'PHP', '後台程式語言主流之一', 6895),
(4, 'JavaScript', '網頁應用主要語言，後台也可用！', 4857),
(5, 'Java', '可攜性高的程式語言，開發App的主要語言之一', 10875);
```

接著我們從編輯候選項目的工作著手，需要一個表單填寫以及資料庫寫入：recommend.html, recommend.php

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>編輯候選項目</title>
  </head>
  <body>
    <p align="center"><img src="title_2.jpg"></p>
    <form action="recommend.php" method="post" >
      <table width="650" align="center" border="1">
        <tr bgcolor="#FFFFCC"> 
          <td align="right">候選項目</td>
          <td><input type="text" name="name" size="10"></td>
        </tr>
        <tr> 
          <td bgcolor="#CCFFCC" align="right">候選項目說明</td>
          <td bgcolor="#CCFFCC">
            <textarea name="introduction" cols="40" rows="3"></textarea>
          </td>
        </tr>
      </table>
      <br>
      <div align="center">
        <input type="submit" value="確定">  
        <input type="reset" value="重新輸入">
      </div>
    </form>
    <p align="center"><a href="javascript:history.back()">回首頁</a></p>	
  </body>
</html>
```

```php
<?php
  require_once("dbtools.inc.php");
  header("Content-type: text/html; charset=utf-8");

  //取得表單資料
  $name = $_POST["name"];
  $introduction = $_POST["introduction"];

  //建立資料連接
  $link = create_connection();

  //執行 SELECT 陳述式來選取候選項目資料
  $sql = "SELECT * FROM candidate WHERE name='$name'";
  $result = execute_sql($link, "u0533001", $sql);

  //檢查被推薦是否有在候選項目清單
  if (mysqli_num_rows($result) == 0)
  {
    //釋放資源
    mysqli_free_result($result);

    //將候選資料加入資料庫
    $sql = "INSERT INTO candidate (name , introduction , score)
            VALUES ('$name', '$introduction', 0)";
    $result = execute_sql($link, "u0533001", $sql);

    //將使用者導向線上投票首頁
    header("location:index0.php");
  }
  else
  {
    //顯示被推薦人已經是候選的訊息
    echo "<p align='center'>您推薦的已經在候選項目，不需要再推薦。</p>";
    echo "<p align='center'><a href ='javascript:history.back()'>回上一頁</a>";
    echo "　　<a href='index0.php'>回首頁</a></p>";
  }

  //關閉資料連接
  mysqli_close($link);
?>
```

接著我們寫登入頁面： index0.php

```php
<!doctype html>
<html>
  <head>
    <title>線上投票</title>
    <meta charset="utf-8">
    <script type="text/javascript">
      function check_data()
      {
        var id = document.myForm.id.value;
        var tab = "ABCDEFGHJKLMNPQRSTUVWXYZIO";
        var A1 = new Array (1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3 );
        var A2 = new Array (0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5 );
        var Mx = new Array (9,8,7,6,5,4,3,2,1,1);

        //將身分證字號的英文字母轉換為大寫
        id = id.toUpperCase();

        //驗證身分證字號是否為 10 碼
        if (id.length != 10)
        {
          alert("身分證字號共有 10 碼");
          return false;
        }

        //驗證身分證字號的第一碼是否為英文字母
        var i = tab.indexOf(id.charAt(0));
        if (i == -1)
        {
          alert("身分證字號第一碼為英文字母");
          return false;
        }

        //驗證身分證字號進階規則
        var sum = A1[i] + A2[i] * 9;
        var v;
        for (i = 1; i < 10; i++)
        {
          v = parseInt(id.charAt(i));
          if (isNaN(v))
          {
            alert("身分證字號末九碼必須為數字");
            return false;
          }

          sum = sum + v * Mx[i];
        }

        if (sum % 10 != 0)
        {
          alert("不合法的身分證字號");
          return false;
        }

        //此部分用來驗證瀏覽者是否有選擇候選項目
        for (var i = 0;i < document.myForm.elements.length; i++)
        {
          var e = document.myForm.elements[i];
          if (e.name == "name" && e.checked == true )
          {
            var found = true;
            break;
          }
        }

        if (found != true)
        {
          alert("您沒有選擇候選人");
          return false;
        }

        myForm.submit();
      }
    </script>
  </head>
  <body bgcolor="#FFE6CC">
    <p align="center"><img src="title_1.jpg"></p>
    <form name="myForm" action="vote.php" method="post" >
      <table width="75%" align="center" border="2" bordercolor="#999999">
        <tr bgcolor="#0033CC">
          <td align="center"><font color="#FFFFFF">候選項目</font></td>
          <td align="center"><font color="#FFFFFF">候選項目介紹</font></td>
        </tr>
          <?php
            require_once("dbtools.inc.php");

            //建立資料連接
            $link = create_connection();

            //執行 Select 陳述式選取候選資料
            $sql = "SELECT * FROM candidate";
            $result = execute_sql($link, "u0533000", $sql);

            while ($row = mysqli_fetch_object($result))
            {
              echo "<tr>";
              echo "<td bgcolor='#CCFFCC'> ";
              echo "<input type='radio' name='name'" .
                "value='$row->name'>$row->name</td>";
              echo "<td bgcolor='#FFCCCC'>$row->introduction</td>";
              echo "</tr>";
            }

            //關閉資料連接
            mysqli_close($link);
          ?>
        <tr bgcolor="#FFFF99">
          <td colspan="2" align="right">請輸入您的身分證字號：
          <input type="text" name="id" size="10"></td>
        </tr>
      </table>
      <p align="center">
        <input type="button" value="投票" onClick="check_data()">
        <input type="reset" value="重新設定">
        <input type="button" value="新增候選項目"
          onclick="javascript:window.open('recommend.html','_self')">
        <input type="button" value="觀看投票結果"
          onclick="javascript:window.open('result.php','_self')">
      </p>
    </form>
  </body>
</html>
```

投票的處理 vote.php

```php
<?php
  require_once("dbtools.inc.php");
  header("Content-type: text/html; charset=utf-8");

  //取得表單資料
  $id = strtoupper($_POST["id"]);
  $name = $_POST["name"];

  //建立資料連接
  $link = create_connection();

  //檢查身分證字號是否投過票
  $sql = "SELECT * FROM id_number Where id = '$id'";
  $result = execute_sql($link, "u0533001", $sql);

  //如果身分證字號已經投過票
  if (mysqli_num_rows($result) != 0)
  {
    //釋放 $result 佔用的記憶體
    mysqli_free_result($result);

    //顯示訊息要求使用者更換帳號名稱
    echo "<script type='text/javascript'>";
    echo "alert('您已經參加過本次活動了');";
    echo "history.back();";
    echo "</script>";
    exit();
  }

  //如果身分證字號沒有投過票
  else
  {
    //釋放 $result 佔用的記憶體
    mysqli_free_result($result);

    /* 執行 INSERT INTO 陳述式，將此瀏覽者的身分證字號加
       入 id_number 資料表，表示此身分證字號已投票 */
    $sql = "INSERT INTO id_number (id) VALUES ('$id')";
    $result = execute_sql($link, "u0533001", $sql);

    //使用 UPDATE 陳述式將票數 + 1
    $sql = "UPDATE candidate SET score = score + 1 WHERE name = '$name'";
    $result = execute_sql($link, "u0533001", $sql);
  }

  //關閉資料連接
  mysqli_close($link);

  //將使用者導向 result.php 網頁
  header("location:result.php");
?>
```

最後就是投票結果的設計：

```php
<!doctype html>
<html>
  <head>
    <title>投票結果</title>
    <meta charset="utf-8">
  </head>
  <body>
    <p align='center'><img src='title_3.jpg'></p>
    <table align='center' width='600' border='1' bordercolor='#990033'>
      <tr bgcolor='#CC66FF'>
        <td align='center'><b><font color='#FFFFFF'>候選人</font></b></td>
        <td align='center'><b><font color='#FFFFFF'>得票數</font></b></td>
        <td align='center'><b><font color='#FFFFFF'>得票百分比</font></b></td>
        <td align='center'><b><font color='#FFFFFF'>直方圖</font></b></td>
      </tr>
      <tr bgcolor='#FFCCFF'>
      <?php
        require_once("dbtools.inc.php");

        //建立資料連接
        $link = create_connection();

        //執行 SELECT 陳述式來選取候選人資料
        $sql = "SELECT * FROM candidate";
        $result = execute_sql($link, "u0533001", $sql);

        //計算總記錄數
        $total_records = mysqli_num_rows($result);

        //計算總票數
	      $total_score = 0;
        while ($row = mysqli_fetch_object($result))
          $total_score += $row->score;

        /* 目前記錄指錄已經在資料表尾端，我們使用
           mysql_data_seek() 函式將記錄指標移至第 1 筆記錄 */
        mysqli_data_seek($result, 0);

        //列出所有候選人得票資料
        for ($j = 0; $j < $total_records; $j++)
        {
          //取得候選人資料
          $row = mysqli_fetch_assoc($result);

          //計算候選人得票百分比
          $percent = round($row["score"] / $total_score, 4) * 100;

          //顯示候選人各欄位的資料
          echo "<tr>";
          echo "<td align='center'>" . $row["name"] . "</td>";
          echo "<td align='center'>" . $row['score'] . "</td>";
          echo "<td align='center'>" . $percent . "%</td>";
          echo "<td height='35'><img src='bar.jpg' width='" .
            $percent * 3 . "' height='20'></tr>";
          echo "</tr>";
        }

        //釋放資源及關閉資料連接
        mysqli_free_result($result);
        mysqli_close($link);
      ?>
      <tr bgcolor='#FFCCFF'>
        <td align='center'>總計</td>
        <td align='center'><?php echo $total_score ?></td>
        <td align='center'>100%</td>
        <td><img src='bar.jpg' width='300' height='20'></td>
      </tr>
    </table>
    <p align='center'><a href='index0.php'>回首頁</a></p>
  </body>
</html>
```

請各位仔細研讀每一行程式，你會發現長條圖的長度使用同一個小圖去縮放比例。算是一個聰明的設計。

由於統計圖的支援程式庫很多，我們其實可以利用來做出十分精巧的統計圖。最有名的二個網頁統計圖程式庫是 chart.js [前往chartjs官網](https://chartjs.org) 以及 d3.js [前往d3js官網](https://d3js.org/)。 

我們先來看一個官網的範例：

```js
<canvas id="myChart" width="400" height="400"></canvas>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
```

PHP 要使用 chart.js 的函式庫，只需要將我們的網頁輸出成類似上面的形式就可以了。

請注意，我們要先定義一個 canvas ，再將 Chart 套用在 canvas 上。

下面是我們將 result.php 改寫成 result_chart.php

```php+HTML
<!doctype html>
<html>
  <head>
    <title>投票結果</title>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
  </head>
  <body>
    <p align='center'><img src='title_3.jpg'></p>
      <?php
        require_once("dbtools.inc.php");
        $link = create_connection();
        $sql = "SELECT * FROM candidate";
        $result = execute_sql($link, "u0533001", $sql);

        $total_records = mysqli_num_rows($result);

        for ($j = 0; $j < $total_records; $j++)
        {
          $row = mysqli_fetch_assoc($result);

          $labels[$j] = $row["name"];
          $scores[$j] = $row["score"];
        }
        function randColor($a){  // 自訂函式：隨機色彩產生器，要返回字串
          $r = random_int(0,255);
          $g = random_int(0,255);
          $b = random_int(0,255);
          $s = "rgba($r,$g,$b,$a)";
          return $s;
        }
        ?>
        <canvas id="myChart" width="400" height="200" aria-label="投票結果統計圖" role="img"></canvas>
        <script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',    // polarArea, bar, line, radar, Pie
    data: {
        labels: [
              <?php 
                for($i=0;$i<$total_records-1;$i++) 
                  echo "'{$labels[$i]}',";
                echo "'{$labels[$total_records-1]}'";
              ?>],
        datasets: [{
            label: '# of Votes',
            data: [
              <?php 
                for($i=0; $i<$total_records-1; $i++) 
                  echo "{$scores[$i]},";
                echo "{$scores[$total_records-1]}";
              ?> ],
            backgroundColor: [ 
                <?php 
                  for($i=0; $i < $total_records-1; $i++) 
                    echo "'".randColor(0.2)."',";
                  echo "'".randColor(0.2)."'"; 
                ?> 
            ],
            borderColor: [
                <?php 
                  for($i=0; $i < $total_records-1; $i++)
                    echo "'".randColor(1)."',";
                    echo "'".randColor(1)."'"; 
                ?> 
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
<?php
        //釋放資源及關閉資料連接
        mysqli_free_result($result);
        mysqli_close($link);
      ?>
    <p align='center'><a href='index0.php'>回首頁</a></p>
  </body>
</html>
```

1. chart.js 的CDN 要引入
2. label, 資料及色彩要產生出來。使用 echo 將所需的字串輸出。
3. 隨機的色彩，我們用了一個函式 randColor($a), 產生出 chart 所需的色彩定義字串。
4. randColor(0.2) 不透明度 0.2, randColor(1) 不透明度 1。
5. 統計圖有不同模式，我們只要將type: 'bar' 換成 'pie' 就能得到圓餅圖。或是 polarArea 圖。

