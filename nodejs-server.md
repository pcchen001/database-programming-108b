---
title: nodejs-server
date: 2020-05-04 19:16:26
tags:
- nodejs
- server
categories:
- Node.js
- Server
---

安裝 node

https://nodejs.org/zh-tw/download/ Windows 安裝包，下載後執行安裝即可

要使用 node.js 來建立伺服器，通常使用 express

[Express/Node](https://developer.mozilla.org/zh-TW/docs/Learn/Server-side/Express_Nodejs/Introduction)

```js
var express = require('express');
var app = express();

app.get('/', function(req, res) {
  res.send('Hello World!');
});

app.listen(3000, function() {
  console.log('Example app listening on port 3000!');
});
```

