---
title: Http Request & Response
date: 2020-03-19 15:04:34
tags:
- http
- http request
- http response
categories:
- http
---

# 網頁間訊息的傳遞

WWW  依賴 Http 傳輸協定。

Http 傳輸協定分為 Request 及 Response 。

客戶端(瀏覽器) 對 伺服器( 網站 ) 送出 Resquest.

網站回傳 Response.

Request 有八種模式： GET, POST, PUT, HEAD, OPTIONS, TRACE, DELETE, CONNECT。

表單上傳時，利用 GET 或是 POST 將內容上傳。

GET 會把所有內容編成一個字串(QueryString)，接在伺服器URL後面。 (明碼)。有長度限制。

POT 會把所有內容編成一個資料(FormData)，在 URL 之外，另外傳送。沒有太大限制。

GET 預設會網路快取暫存(cache)，POST 預設不會cache。



Response 的狀態以三位數字來表示：

100 以及1xx 表示 informational 內容

200 以及 2xx 表示 Successful

300 以及 3XX 表示 Redirection 轉向別的網址

400 以及 4xx 表示錯誤，某種傳輸或客戶端錯誤

500 以及 5xx 表示伺服器端的錯誤



不論是 Request 或是 Response ，傳送的東西都分為二個部分， header, body 。

php 可以直接用指令傳送　header, 例如要轉向另一個網址：

```php
<?php
    header("Location: http://www.nuu.edu.tw");
?>
```

有時我們需要一點認證，可以使用要求認證的 header:

```php
<?php
  if (!isset($_SERVER['PHP_AUTH_USER']))
  {
	header('WWW-Authenticate: Basic realm="myrealm"');
	header('HTTP/1.0 401 Unauthorized');
    echo "抱歉！您沒有輸入密碼！";
    exit();
  }
  else
  {
    echo "{$_SERVER['PHP_AUTH_USER']}您好！<br>";
    echo "您輸入的密碼為{$_SERVER['PHP_AUTH_PW']}！";
  }
?>
```

header 內容蠻多的，我們不一次介紹，碰到時在逐一介紹。

[http-header](https://en.wikipedia.org/wiki/List_of_HTTP_header_fields) 參考網站。

練習：

```
帳號密碼正確
　　進入另一個網頁(wellcome.html)
密碼不正確
　　進入重新登入網頁(login.html)
帳號不存在
　　進入註冊網頁(register.html)
confirm.php
```

